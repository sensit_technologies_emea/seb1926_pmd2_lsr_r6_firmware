#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "stm32l4xx_hal.h"
#include "spi_adc_ltc1864l.h"
#include "dateEpoch.h"
#include "taskManager.h"
#include "version.h"
#include "eeprom_address.h"
#include "timer.h"
#include "time.h"
#include "main.h"
#include "ad.h"
#include "out.h"
#include "usart_pc.h"
#include "spi_address.h"
#include "vee.h"
#include "sensit.h" 
#include "math.h"
#include "i2c_24AA512.h"
#include "B2B_MASTER_Laser.h"
#include "i2c_digitalpot_AD5243.h"
#include "modbus.h"
#include "modbus_extension_register.h"
#include "spi_dds_AD9834.h"


#define 	FAST_RAMP								0
#define 	DIM_SHARED_MEMORY       64
#define 	SIZEBUFRXTX		4096
#define 	POSCRC		    SIZEBUFRXTX-1
uint8_t 	toCPU_TxData[SIZEBUFRXTX];
uint8_t 	fromCPU_RxData[SIZEBUFRXTX];
uint32_t 	uwCRCValue;
uint8_t  	sharedMemoryReceive[DIM_SHARED_MEMORY];
uint8_t  	sharedMemoryTransmit[DIM_SHARED_MEMORY];
uint32_t 	sharedMemoryCRC=0;
uint32_t 	sharedMemoryLastRX=0;
uint16_t 	flgSpiLaserCommunicationEnd=0;
extern 		SPI_HandleTypeDef hspi1;
extern 		RTC_HandleTypeDef hrtc;
extern 		CRC_HandleTypeDef hcrc;
extern 	  TIM_HandleTypeDef htim6;
extern	  DAC_HandleTypeDef hdac1;
extern 		uint8_t  USART1_RxAnalyzeBuffer[DIM_USART1_RxBuffer];
extern    uint16_t flgB2B_USART_RECORD;
float 		iLasHSDef;
float 		iLasLSDef;

uint8_t		timeMode;
void 			( *ptrTaskManager ) (void);
 
enum state { POWERUP_STATE=10, WARMING_STATE=20, SETPAR_STATE=30, MEAS_STATE=40, HOUSEKEEPING_STATE, SET_STATE=60, UPDATE_STATE, ERROR_STATE=110};

//-------------------------------------------------------------------
// le strutture vengono riempite con i valori di default
// viene accesa la stabilizzazione
void powerUp_state( void ){
	set_active_state(POWERUP_STATE);
	disable_controller_Modbus();
  TC_OFF();    VLD_OFF(); V5V_OFF();

	PMD2_SendToRemote.field.system.version=swVersion;
	PMD2_SendToRemote.field.system.release=swRelease;
	PMD2_SendToRemote.field.flag.autoRange=ON;
	PMD2_SendToRemote.field.flag.switchRange=OFF; 
	PMD2_SendToRemote.field.flag.centerFlag=ON;
	PMD2_SendToRemote.field.flag.sensorMode=MEAS;
	PMD2_SendToRemote.field.flag.print=OFF;	
	PMD2_SendToRemote.field.flag.autoBackground=ON;
	PMD2_SendToRemote.field.measurementReading.sensitivityLevel=HIGH;
	PMD2_SendToRemote.field.flag.uartBridge=OFF;
	PMD2_SendToRemote.field.flag.firmwareUpdate=OFF;
	//load from EEPROM must be called after flag initialization
	load_struct_from_eeprom();
	if (PMD2_SendToRemote.field.flag.sensorMode==SET){
		load_struct_from_eeprom_OLD();
		PMD2_SendToRemote.field.flag.autoRange=OFF;
		PMD2_SendToRemote.field.flag.centerFlag=OFF;
	}
			PMD2_SendToRemote.field.flag.autoRange=OFF;
		PMD2_SendToRemote.field.flag.centerFlag=OFF;
	check_eeprom_value();
	timeMode=0;
	if (timeMode==0){
		PMD2_SendToRemote.field.measurementSettings.startPoint=52; PMD2_SendToRemote.field.measurementSettings.endPoint=PMD2_SendToRemote.field.measurementSettings.startPoint+30;
		PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint=5;  PMD2_SendToRemote.field.measurementSettings.deltaX=45;
		PMD2_SendToRemote.field.measurementSettings.nAcqPoint=450;    PMD2_SendToRemote.field.measurementSettings.nAvg=100;
		PMD2_SendToRemote.field.measurementSettings.waitBefore=500;PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle=280;	
		PMD2_SendToRemote.field.workingSettings.laserRampFrequency=260;
//		PMD2_SendToRemote.field.measurementSettings.waitBefore=200;PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle=5;	
//		PMD2_SendToRemote.field.workingSettings.laserRampFrequency=325;
//		PMD2_SendToRemote.field.flag.autoRange=OFF;
//		PMD2_SendToRemote.field.flag.centerFlag=OFF;
//		PMD2_SendToRemote.field.flag.autoBackground=OFF;
		if (FAST_RAMP){
			PMD2_SendToRemote.field.measurementSettings.startPoint=45; PMD2_SendToRemote.field.measurementSettings.endPoint=PMD2_SendToRemote.field.measurementSettings.startPoint+30;
			PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint=5;  PMD2_SendToRemote.field.measurementSettings.deltaX=40;
			PMD2_SendToRemote.field.measurementSettings.nAcqPoint=400;    PMD2_SendToRemote.field.measurementSettings.nAvg=100;
			PMD2_SendToRemote.field.measurementSettings.waitBefore=250;PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle=50;	
			PMD2_SendToRemote.field.workingSettings.laserRampFrequency=345; //260Hz=3.84ms 300*1024/(80MHz) (0.5+0.28+0.5)*0.9 +450*6us=1.15+2.7 
		}

	}else if (timeMode==1){
		PMD2_SendToRemote.field.measurementSettings.startPoint=26; PMD2_SendToRemote.field.measurementSettings.endPoint=PMD2_SendToRemote.field.measurementSettings.startPoint+15;
		PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint=2;  PMD2_SendToRemote.field.measurementSettings.deltaX=22;
		PMD2_SendToRemote.field.measurementSettings.nAcqPoint=225;    PMD2_SendToRemote.field.measurementSettings.nAvg=150;
		PMD2_SendToRemote.field.measurementSettings.waitBefore=250;PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle=180;	
		PMD2_SendToRemote.field.workingSettings.laserCurrentHS=5.65f;
		PMD2_SendToRemote.field.workingSettings.calibHS=0.35;
		PMD2_SendToRemote.field.workingSettings.laserRampFrequency=520;//520Hz=1.92ms 0.68*0.9=0.6 + 1.35
	}else if (timeMode==2){
		PMD2_SendToRemote.field.measurementSettings.startPoint=13; PMD2_SendToRemote.field.measurementSettings.endPoint=PMD2_SendToRemote.field.measurementSettings.startPoint+7;
		PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint=2;  PMD2_SendToRemote.field.measurementSettings.deltaX=11;
		PMD2_SendToRemote.field.measurementSettings.nAcqPoint=110;    PMD2_SendToRemote.field.measurementSettings.nAvg=100;
		PMD2_SendToRemote.field.measurementSettings.waitBefore=185;PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle=185;	
		PMD2_SendToRemote.field.workingSettings.laserCurrentHS=5.65f;
		PMD2_SendToRemote.field.workingSettings.calibHS=0.35;
		PMD2_SendToRemote.field.workingSettings.laserRampFrequency=919;//920 Hz=1.1ms
	}
	PMD2_SendToRemote.field.measurementSettings.PHDGain=32;
	PMD2_SendToRemote.field.measurementSettings.PHDBackground=32768;
	PMD2_SendToRemote.field.measurementReading.concentration=0.0f;
	PMD2_SendToRemote.field.measurementReading.PHDZero=0;
  PMD2_SendToRemote.field.measurementReading.concentration=0.0;  PMD2_SendToRemote.field.measurementReading.concentrationB=0.0;
  PMD2_SendToRemote.field.measurementReading.errorID=OFF;
	PMD2_SendToRemote.field.measurementReading.counterM=0;
	iLasHSDef=PMD2_SendToRemote.field.workingSettings.laserCurrentHS;
	iLasLSDef=PMD2_SendToRemote.field.workingSettings.laserCurrentLS;
  service.nStab=0;  service.nV=1; 
  service.ready=0; 	service.modbusCH1=0;
	flgB2B_USART_RECORD=0;

//  //********************************************************************************************************  
//  iLasHSDef=PMD2_SendToRemote.field.workingSettings.laserCurrentHS;    tLasDef=PMD2_SendToRemote.field.workingSettings.laserTemperature;
//  nSamplesDef=PMD2_SendToRemote.field.measurementSettings.nAcqPoint;    nMeanDef=PMD2_SendToRemote.field.measurementSettings.nAvg;
//  iAMPHSDef=PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS;    iScanFreqDef=PMD2_SendToRemote.field.workingSettings.laserRampFrequency;
//	lDCDef=PMD2_SendToRemote.field.measurementSettings.PHDBackground;  
//  iLasLSDef=PMD2_SendToRemote.field.workingSettings.laserCurrentLS;  iAMPLSDef=PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS;
//  amplResistorValueDef=PMD2_SendToRemote.field.measurementSettings.PHDGain;
//  //********************************************************************************************************  
	
	//////

	copySrcToTwiceDest(&PMD2_SendToRemote,&PMD2_ReceiveFromRemote,&PMD2_PreviousReceiveFromRemote);
	if (PMD2_SendToRemote.field.flag.sensorMode==MEAS){
		timCiclo1ms = Tc_housekeeping_state;
		ptrTaskManager = warming_housekeeping_state;
	}else{
		timCiclo1ms =0;
		ptrTaskManager = setting_state;
	}
}
//--------------------------------------------------------------------------------------------------------------
// attesa della stabilizzazione e controllo dei parametri del laser in funzione
void warming_housekeeping_state(void){
	set_active_state(WARMING_STATE);
	if (PMD2_SendToRemote.field.flag.tecStatus==OFF){
		set_ad5693_laser_temperature(PMD2_SendToRemote.field.workingSettings.laserTemperature);                                    
		TC_ON();    
		set_ad5693_laser_temperature(PMD2_SendToRemote.field.workingSettings.laserTemperature); 
	}
  if ( timCiclo1ms )         
        return;
  timCiclo1ms = Tc_warming_state;
  read_laser_temperature();  read_laser_voltage();  read_laser_current();
  read_peltier_current();  read_cell_temperature();
	if (PMD2_SendToRemote.field.flag.laserStatus==OFF){
		if(PMD2_SendToRemote.field.flag.errorFlag==ON && ((PMD2_SendToRemote.field.houseKeeping.laserTemperature>PMD2_SendToRemote.field.workingSettings.laserTemperature+200) ||
																	 (PMD2_SendToRemote.field.houseKeeping.laserTemperature<PMD2_SendToRemote.field.workingSettings.laserTemperature-200))){
			ptrTaskManager = warming_housekeeping_state;
			service.nStab++;
			if (service.nStab>250){
				ptrTaskManager = error_state;
				PMD2_SendToRemote.field.measurementReading.errorID=1;
			}
		}
		else{
			ptrTaskManager = set_parameters_state;
			service.nStab=0;
		}
	}else{
		if(PMD2_SendToRemote.field.flag.errorFlag==ON && ((PMD2_SendToRemote.field.houseKeeping.laserTemperature>PMD2_SendToRemote.field.workingSettings.laserTemperature+200) ||
																	 (PMD2_SendToRemote.field.houseKeeping.laserTemperature<PMD2_SendToRemote.field.workingSettings.laserTemperature-200))){
			ptrTaskManager = warming_housekeeping_state;
			service.nStab++;
			if (service.nStab>250){
				ptrTaskManager = error_state;
				PMD2_SendToRemote.field.measurementReading.errorID=2;
			}
		}
		else{
			service.nStab=0;
			timCiclo1ms = Tc_measurement_state;
			ptrTaskManager = measurement_state;
		}
		if (PMD2_SendToRemote.field.flag.errorFlag && (PMD2_SendToRemote.field.houseKeeping.laserVoltage<0.5f || PMD2_SendToRemote.field.houseKeeping.laserVoltage>2.8f)){
			ptrTaskManager = error_state;
			PMD2_SendToRemote.field.measurementReading.errorID=3;
		}
		if (PMD2_SendToRemote.field.flag.errorFlag && (PMD2_SendToRemote.field.houseKeeping.laserCurrent<0.001f)){
			ptrTaskManager = error_state;
			PMD2_SendToRemote.field.measurementReading.errorID=4;
		}      
	}
}

//--------------------------------------------------------------------------------------------------------------
// si accende il laser e vengono impostati i valori di default per amplificazione e sottrazione del background
void set_parameters_state(void){
	set_active_state(SETPAR_STATE);
	ad5696_set_channel(WRITE_TO_AND_UPDT_DAC_N | AD5696_DAC_A | AD5696_DAC_B | AD5696_DAC_C | AD5696_DAC_D, 65535);  
															
	set_ad5693_laser_bias_current(PMD2_SendToRemote.field.workingSettings.laserCurrentHS);
	//set_laser_scan_amplitude(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS);  
	VLD_ON();  
	set_ad5693_laser_bias_current(PMD2_SendToRemote.field.workingSettings.laserCurrentHS);
	set_ad5693_laser_temperature(PMD2_SendToRemote.field.workingSettings.laserTemperature); 
	set_ad5693_laser_temperature(PMD2_SendToRemote.field.workingSettings.laserTemperature); 
	V5V_ON();
	initSpi_Dds_AD9834();
	set_ad5693_VGA_GAIN(65535);
	reset_DDS_rf();
	RF_in_phase_same_freq_init();
	set_DDS_rf();
	//	set_ad5693_laser_bias_current(0);
	//	set_ad5693_laser_bias_current(11);
	//set_laser_scan_amplitude(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS);
	init_ramp(PMD2_SendToRemote.field.workingSettings.laserRampFrequency,PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS);
	start_ramp();
	set_ad5693_DC_level(PMD2_SendToRemote.field.measurementSettings.PHDBackground);
	AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_A, PMD2_SendToRemote.field.measurementSettings.PHDGain, 0);
	AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_B, PMD2_SendToRemote.field.measurementSettings.PHDGain, 0);
	set_phase_f(PMD2_SendToRemote.field.workingSettings.calibLS);
	timCiclo1ms = Tc_housekeeping_state;
  ptrTaskManager = warming_housekeeping_state;
}

//--------------------------------------------------------------------------------------------------------------
//acquisizione, calcolo della concentrazione, centraggio e celta del range
void measurement_state(void){
	uint8_t fase;
	set_active_state(MEAS_STATE);
	timCiclo1ms = Tc_test_state;
//	B2B_USART_DisableCPU_TX_Laser_RX();
	start_clock_TIM5();
	//read_rampa_DC(&service.adcValueOriginal[0],PMD2_SendToRemote.field.measurementSettings.nAcqPoint,PMD2_SendToRemote.field.measurementSettings.nAvg);
	read_rampa_SD(&service.adcValueOriginal[0],PMD2_SendToRemote.field.measurementSettings.nAcqPoint,PMD2_SendToRemote.field.measurementSettings.nAvg);
	PMD2_SendToRemote.field.measurementReading.lastTimeAcq=read_stop_clock_TIM5()/1000;

	PMD2_SendToRemote.field.measurementReading.counterM++;
	if (PMD2_SendToRemote.field.measurementReading.counterM>76)
		service.ready=1;
//	B2B_USART_EnableCPU_TX_Laser_RX();
	calculate_concentration_diff();
	change_sensitivity_level();

	if (PMD2_SendToRemote.field.flag.centerFlag==ON && PMD2_SendToRemote.field.measurementReading.counterM>20)
		line_center();
	timCiclo1ms = Tc_measurement;
	ptrTaskManager = measurement_housekeeping_state;

	if (PMD2_SendToRemote.field.flag.errorFlag==ON && (PMD2_SendToRemote.field.houseKeeping.laserVoltage<0.5f || PMD2_SendToRemote.field.houseKeeping.laserVoltage>2.5f)){
		ptrTaskManager = error_state;
		PMD2_SendToRemote.field.measurementReading.errorID=5;
	}
	if (PMD2_SendToRemote.field.flag.errorFlag==ON && (PMD2_SendToRemote.field.houseKeeping.laserCurrent<0.001f)){
		ptrTaskManager = error_state;
		PMD2_SendToRemote.field.measurementReading.errorID=6;
	}      
}

void measurement_housekeeping_state(void){
	set_active_state(HOUSEKEEPING_STATE);
	read_laser_temperature();	read_laser_voltage();	read_laser_current();
	read_peltier_current();	read_cell_temperature();
	timCiclo1ms = Tc_measurement_state;
	if 	(PMD2_SendToRemote.field.flag.print==ON)
		ptrTaskManager = measurement_housekeeping_state;		
	else
		ptrTaskManager = communication_state;
}
//--------------------------------------------------------------------------------------------------------------
//stampa su seriale
void communication_state(void){
	static uint8_t indUart=0;
	static uint8_t counterPrint=0;

	uint32_t uwCRCValueCheck;
	volatile uint8_t ptrTxBufData[256];
	static uint8_t txBufData[256];

//	if (PMD2_SendToRemote.field.flag.print==ON){
//			if (counterPrint<10)
//				counterPrint++;
//			else{
//			if (service.nV==0)
//				print_n_values_first_protocol(&service.adcValueOriginal[0], PMD2_SendToRemote.field.measurementSettings.nAcqPoint);
//			else if (service.nV==1)
//				print_n_values_second_protocol(&service.adcValueOriginal[0], PMD2_SendToRemote.field.measurementSettings.nAcqPoint);  
//			}
//	}
//	else
//		counterPrint=0;
	if (service.modbusCH1==OFF && PMD2_SendToRemote.field.flag.uartBridge==OFF){
//		sprintf((char *)ptrTxBufData, "%2d;%3d;%3.1f;%3.1f;%d;%d;%d;%2.1f;%2.1f;%2.1f;%2.1f;%d ",indUart, PMD2_SendToRemote.field.measurementReading.activeState, 
//														PMD2_SendToRemote.field.measurementReading.concentration, PMD2_SendToRemote.field.measurementReading.concentrationRaw,
//														PMD2_SendToRemote.field.measurementReading.sensitivityLevel, PMD2_SendToRemote.field.measurementReading.errorID,
//														PMD2_SendToRemote.field.measurementReading.quality,PMD2_SendToRemote.field.houseKeeping.laserTemperature, 
//														PMD2_SendToRemote.field.houseKeeping.laserVoltage, PMD2_SendToRemote.field.houseKeeping.laserCurrent,
//														PMD2_SendToRemote.field.houseKeeping.laserTECCurrent, PMD2_SendToRemote.field.measurementReading.bit);	
		
//		sprintf((char *)ptrTxBufData, "%3.1f;%3.1f;%3.1f;%3.1f;%d;%d;%d;%d;%d;%d;%d ",PMD2_SendToRemote.field.measurementReading.concentrationRaw,
//														PMD2_SendToRemote.field.measurementReading.concentration, PMD2_SendToRemote.field.measurementReading.concentrationB,
//														PMD2_SendToRemote.field.measurementReading.PHDPower,PMD2_SendToRemote.field.measurementReading.bit, 
//														PMD2_SendToRemote.field.measurementReading.lastTimeAcq,PMD2_SendToRemote.field.measurementReading.counterM, 
//														PMD2_SendToRemote.field.measurementReading.sensitivityLevel, PMD2_SendToRemote.field.measurementReading.errorID,
//														PMD2_SendToRemote.field.measurementReading.activeState, PMD2_SendToRemote.field.measurementReading.quality);		
//		uwCRCValueCheck = HAL_CRC_Calculate(&hcrc,(uint32_t *)  ptrTxBufData , strlen((char*)ptrTxBufData));	
//		sprintf((char *)ptrTxBufData, "%s%04X\n",ptrTxBufData,uwCRCValueCheck&0xFFFF);
//		HAL_UART_Transmit (&huart1, (uint8_t *)ptrTxBufData, strlen((char *)ptrTxBufData), 100);
		print_UART2();
	}
  if (PMD2_SendToRemote.field.flag.sensorMode==MEAS && PMD2_SendToRemote.field.measurementReading.activeState==HOUSEKEEPING_STATE){
		timCiclo1ms = Tc_dummy_state;
		ptrTaskManager=measurement_state;
	}else{
		timCiclo1ms = 0;
		ptrTaskManager=setting_state;
	}
}

void setting_state(void){
	set_active_state(SET_STATE);
	if (PMD2_SendToRemote.field.flag.tecStatus==ON)
		TC_OFF();    VLD_OFF();		
	if (timCiclo1ms)
		return;
	ptrTaskManager=communication_state;
	PMD2_SendToRemote.field.measurementReading.concentration=111;
	timCiclo1ms = Tc_setting_state;
  if (PMD2_SendToRemote.field.flag.sensorMode==MEAS){
		timCiclo1ms = Tc_housekeeping_state;
		ptrTaskManager = warming_housekeeping_state;
	}
}

void update_state(void){
	set_active_state(UPDATE_STATE);
	if (PMD2_SendToRemote.field.flag.tecStatus==ON)
		TC_OFF();    VLD_OFF();	
	ptrTaskManager=update_state;
}



void error_state(){
	set_active_state(ERROR_STATE);
  VLD_OFF();
  TC_OFF();
  ptrTaskManager=taskSPI_communication;
  ptrTaskManager = communication_state;
}

void change_sensitivity_level(void){
	if(PMD2_SendToRemote.field.measurementReading.sensitivityLevel==HIGH && 
		PMD2_SendToRemote.field.flag.autoRange==1 && (PMD2_SendToRemote.field.measurementReading.rampStatus==3 || 
		PMD2_SendToRemote.field.measurementReading.concentration>3300 || PMD2_SendToRemote.field.measurementReading.bit<8)){
		set_ad5693_laser_bias_current(PMD2_SendToRemote.field.workingSettings.laserCurrentLS);
		set_laser_scan_amplitude(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS);
		set_sensitivity(LOW);
	}	else if(PMD2_SendToRemote.field.measurementReading.sensitivityLevel==LOW  && PMD2_SendToRemote.field.flag.autoRange==1 && PMD2_SendToRemote.field.measurementReading.concentration<3000 ){
		set_ad5693_laser_bias_current(PMD2_SendToRemote.field.workingSettings.laserCurrentHS);
		set_laser_scan_amplitude(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS);
		set_sensitivity(HIGH);
	}
	if(PMD2_SendToRemote.field.measurementReading.sensitivityLevel==HIGH && PMD2_SendToRemote.field.flag.switchRange==1){
		set_ad5693_laser_bias_current(PMD2_SendToRemote.field.workingSettings.laserCurrentLS);
		set_laser_scan_amplitude(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS);	
		set_sensitivity(LOW);reset_switchRange();
	}
	if(PMD2_SendToRemote.field.measurementReading.sensitivityLevel==LOW  && PMD2_SendToRemote.field.flag.switchRange==1){
		set_ad5693_laser_bias_current(PMD2_SendToRemote.field.workingSettings.laserCurrentHS);
		set_laser_scan_amplitude(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS);
		set_sensitivity(HIGH);reset_switchRange();
	}
}

void print_conc(){
	sprintf( (char *)service.tmpBuf, "{\r\n %3.6f \r\n %3.6f \r\n %4.1f \r\n %d\r\n}\r\n",PMD2_SendToRemote.field.measurementReading.concentration, 
																																																	PMD2_SendToRemote.field.measurementReading.concentrationB,
																																																	PMD2_SendToRemote.field.measurementReading.PHDPower,
																																																	PMD2_SendToRemote.field.measurementReading.bit);
	sendUsart2( (uint8_t *)service.tmpBuf );
}

void print_n_values_first_protocol(uint32_t *pvalues, uint16_t n_values){
	sprintf( (char *)service.tmpBuf, "{\r\n");
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "%3.6f\r\n",PMD2_SendToRemote.field.measurementReading.concentration );
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "%3.6f\r\n",PMD2_SendToRemote.field.measurementReading.concentrationB );
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "%d\r\n",PMD2_SendToRemote.field.measurementReading.sensitivityLevel);		
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "%4.1f\r\n",PMD2_SendToRemote.field.measurementReading.PHDPower);			
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "%df\r\n",PMD2_SendToRemote.field.measurementReading.bit);			
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "%2.2f\r\n",PMD2_SendToRemote.field.houseKeeping.laserVoltage );			
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "%d\r\n",service.nV);			
	sendUsart2( (uint8_t *)service.tmpBuf );    
	for (uint16_t i=0; i<n_values; i++){
		sprintf( (char *)service.tmpBuf, "%d\r\n", *(pvalues+i));			
		sendUsart2( (uint8_t *)service.tmpBuf );
	}
	sprintf( (char *)service.tmpBuf, "}\r\n");			
	sendUsart2( (uint8_t *)service.tmpBuf );  
}

void print_UART1(void){
	sprintf((char *)service.tmpBuf, "{%3.1f;%3.1f;%3.1f;%3.1f;%d;%d;%d;%d;%d;%d;%d||XX}\r\n",PMD2_SendToRemote.field.measurementReading.concentrationRaw,
														PMD2_SendToRemote.field.measurementReading.concentration, PMD2_SendToRemote.field.measurementReading.concentrationB,
														PMD2_SendToRemote.field.measurementReading.PHDPower,PMD2_SendToRemote.field.measurementReading.bit, 
														PMD2_SendToRemote.field.measurementReading.lastTimeAcq,PMD2_SendToRemote.field.measurementReading.counterM, 
														PMD2_SendToRemote.field.measurementReading.sensitivityLevel, PMD2_SendToRemote.field.measurementReading.errorID,
														PMD2_SendToRemote.field.measurementReading.activeState, PMD2_SendToRemote.field.measurementReading.quality);	
		
//		for (uint16_t i=0; i<PMD2_SendToRemote.field.measurementSettings.nAcqPoint-1; i++){
//		sprintf( (char *)(service.tmpBuf+strlen((char*)service.tmpBuf)), "%d;", service.adcValueOriginal[i]);			
//	}
//	sprintf( (char *)(service.tmpBuf+strlen((char*)service.tmpBuf)), "%d|", service.adcValueOriginal[PMD2_SendToRemote.field.measurementSettings.nAcqPoint-1]);			
		//uwCRCValueCheck = HAL_CRC_Calculate(&hcrc,(uint32_t *)  ptrTxBufData , strlen((char*)ptrTxBufData));	
		//sprintf((char *)ptrTxBufData, "%s%04X}\n",ptrTxBufData,uwCRCValueCheck&0xFFFF);
		HAL_UART_Transmit (&huart1, (uint8_t *)service.tmpBuf, strlen((char *)service.tmpBuf), 100);
}
void print_UART2(void){
	uint32_t uwCRCValueCheck;
	sprintf((char *)service.tmpBuf, "{%3.1f;%3.1f;%3.1f;%3.1f;%d;%d;%d;%d;%d;%d;%d||",PMD2_SendToRemote.field.measurementReading.concentrationRaw,
														PMD2_SendToRemote.field.measurementReading.concentration, PMD2_SendToRemote.field.measurementReading.concentrationB,
														PMD2_SendToRemote.field.measurementReading.PHDPower,PMD2_SendToRemote.field.measurementReading.bit, 
														PMD2_SendToRemote.field.measurementReading.lastTimeAcq,PMD2_SendToRemote.field.measurementReading.counterM, 
														PMD2_SendToRemote.field.measurementReading.sensitivityLevel, PMD2_SendToRemote.field.measurementReading.errorID,
														PMD2_SendToRemote.field.measurementReading.activeState, PMD2_SendToRemote.field.measurementReading.quality);	
		
//		for (uint16_t i=0; i<PMD2_SendToRemote.field.measurementSettings.nAcqPoint-1; i++){
//		sprintf( (char *)(service.tmpBuf+strlen((char*)service.tmpBuf)), "%d;", service.adcValueOriginal[i]);			
//	}
//	sprintf( (char *)(service.tmpBuf+strlen((char*)service.tmpBuf)), "%d|", service.adcValueOriginal[PMD2_SendToRemote.field.measurementSettings.nAcqPoint-1]);			
		uwCRCValueCheck = HAL_CRC_Calculate(&hcrc,(uint32_t *)  service.tmpBuf , strlen((char*)(service.tmpBuf)));	
		sprintf((char *)service.tmpBuf, "%s%04X\n",(char *)service.tmpBuf,uwCRCValueCheck&0xFFFF);
		HAL_UART_Transmit (&huart1, (uint8_t *)service.tmpBuf, strlen((char *)service.tmpBuf), 100);
//		HAL_UART_Transmit (&huart2, (uint8_t *)service.tmpBuf, strlen((char *)service.tmpBuf), 100);
}
void print_n_values_second_protocol(uint32_t *pvalues, uint16_t n_values){
	uint8_t sep=PMD2_SendToRemote.field.measurementSettings.deltaX;
	uint16_t middle_point=PMD2_SendToRemote.field.measurementSettings.nAcqPoint/2;
	sprintf( (char *)service.tmpBuf, "{");
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "%d;",PMD2_SendToRemote.field.system.Serial_Nr);
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "%d;",PMD2_SendToRemote.field.measurementReading.counterM);
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "%3.6f;",PMD2_SendToRemote.field.measurementReading.concentration );
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "%3.6f;",PMD2_SendToRemote.field.measurementReading.concentrationB );
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "%d;",PMD2_SendToRemote.field.measurementReading.sensitivityLevel);		
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "%d;",PMD2_SendToRemote.field.measurementReading.lastTimeAcq);			
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "%d",PMD2_SendToRemote.field.measurementReading.bit*10);					
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, " | ");			
	sendUsart2( (uint8_t *)service.tmpBuf );  
	for (uint16_t i=0; i<n_values-1; i++){
		sprintf( (char *)service.tmpBuf, "%d;", *(pvalues+i));			
		sendUsart2( (uint8_t *)service.tmpBuf );
	}
	sprintf( (char *)service.tmpBuf, "%d", *(pvalues+n_values-1));			
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, " | ");			
	sendUsart2( (uint8_t *)service.tmpBuf );
	for( uint16_t i=0; i<middle_point-2*sep; i++){
		sprintf( (char *)service.tmpBuf, "%d;", service.adcValueElaborated[i]);			
		sendUsart2( (uint8_t *)service.tmpBuf );
	}
	for( uint16_t i=0; i<middle_point-2*sep; i++){
		sprintf( (char *)service.tmpBuf, "%d;", service.adcValueElaborated[i+middle_point]);			
		sendUsart2( (uint8_t *)service.tmpBuf );
	}

	sprintf( (char *)service.tmpBuf, "XX");			
	sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "}\r\n");			
	sendUsart2( (uint8_t *)service.tmpBuf );  
}

void print_n_values_third_protocol(uint32_t *pvalues, uint16_t n_values){
	char printBuff[3700];
	sprintf( printBuff, "{%3.6f;%3.6f;%d;%4.1f;%df|",PMD2_SendToRemote.field.measurementReading.concentration,PMD2_SendToRemote.field.measurementReading.concentrationB,
					PMD2_SendToRemote.field.measurementReading.sensitivityLevel,PMD2_SendToRemote.field.measurementReading.PHDPower,PMD2_SendToRemote.field.measurementReading.bit);
	for (uint16_t i=0; i<n_values-1; i++){
		sprintf( printBuff+strlen(printBuff), "%d;", *(pvalues+i));			
	}
	sprintf( printBuff+strlen(printBuff), "%d;XX}\r\n", *(pvalues+n_values-1));				
	sendUsart2((uint8_t *)printBuff);  
}


void dummy_state(void){
  if ( timCiclo1ms )         
        return;
  timCiclo1ms = Tc_measurement_state;
  ptrTaskManager=measurement_state;
}


void calculate_concentration_diff(){
  uint16_t xmin, xmax;
  uint32_t ymin1, ymin2, ymax;
	uint8_t sep=PMD2_SendToRemote.field.measurementSettings.deltaX;
	float y_mean1, y_mean2;

	uint16_t startInd, rngInd;
	uint16_t middle_point=PMD2_SendToRemote.field.measurementSettings.nAcqPoint/2;
	float concMean;
	startInd=PMD2_SendToRemote.field.measurementSettings.startPoint;rngInd=PMD2_SendToRemote.field.measurementSettings.endPoint-PMD2_SendToRemote.field.measurementSettings.startPoint;
	float normF=1000000.0f;
	//smooth a 10 punti

	for( uint16_t i=0; i<PMD2_SendToRemote.field.measurementSettings.nAcqPoint; i++){
		service.adcValueElaborated[i]=service.adcValueOriginal[i];
	}
	minmaxmeanV(&(service.adcValueElaborated[0]),middle_point, &y_mean1, &xmin, &ymin1,&xmax, &ymax);
	PMD2_SendToRemote.field.measurementReading.PHDPower=y_mean1/((float)PMD2_SendToRemote.field.measurementSettings.nAvg);
	PMD2_SendToRemote.field.measurementReading.bit=(int)(log2f((ymax-ymin1)/((float)PMD2_SendToRemote.field.measurementSettings.nAvg))*10);
	PMD2_ReceiveFromRemote.field.measurementReading.PHDPower=PMD2_SendToRemote.field.measurementReading.PHDPower;
	PMD2_PreviousReceiveFromRemote.field.measurementReading.PHDPower=PMD2_ReceiveFromRemote.field.measurementReading.PHDPower;
	PMD2_ReceiveFromRemote.field.measurementReading.bit=PMD2_SendToRemote.field.measurementReading.bit;
	PMD2_PreviousReceiveFromRemote.field.measurementReading.bit=PMD2_ReceiveFromRemote.field.measurementReading.bit;
	service.adcValueElaborated[0]=service.adcValueElaborated[1];
	smooth(PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint, service.adcValueOriginal, middle_point);
	smooth(PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint, &(service.adcValueOriginal[middle_point]), middle_point);
	//meanv(&(service.adcValueOriginal[0]), &y_mean1, middle_point);
	//meanv(&(service.adcValueOriginal[0]), &y_mean2, middle_point);
	PMD2_SendToRemote.field.data.acquiredLength=PMD2_SendToRemote.field.measurementSettings.nAcqPoint;//450
	PMD2_SendToRemote.field.data.elaboratedLength=2*middle_point-4*sep;//270
	for( uint16_t i=0; i<middle_point; i++){
		PMD2_SendToRemote.field.data.acquiredVector[i]=(service.adcValueOriginal[i]);
		PMD2_SendToRemote.field.data.acquiredVector[i+middle_point]=(service.adcValueOriginal[i+middle_point]);
	}

	minmaxmeanV(&(service.adcValueOriginal[0]),middle_point, &y_mean1, &xmin, &ymin1,&xmax, &ymax);
//	for( uint16_t i=0; i<middle_point; i++){
//		service.adcValueElaborated[i]=(service.adcValueElaborated[i]*(normF/y_mean1)+0.5f);
//		service.adcValueElaborated[i+middle_point]=(service.adcValueElaborated[i+middle_point]*(normF/y_mean2)+0.5f);
//		PMD2_SendToRemote.field.data.acquiredVector[i]=(service.adcValueOriginal[i]);
//		PMD2_SendToRemote.field.data.acquiredVector[i+middle_point]=(service.adcValueOriginal[i+middle_point]);
//	}

	PMD2_SendToRemote.field.measurementReading.concentrationRaw=(ymax-ymin1)/(PMD2_SendToRemote.field.measurementSettings.nAvg*100.0);
	minmaxmeanV(&(service.adcValueOriginal[middle_point]),middle_point, &y_mean1, &xmin, &ymin1,&xmax, &ymax);
	PMD2_SendToRemote.field.measurementReading.concentrationB=(ymax-ymin1)/(PMD2_SendToRemote.field.measurementSettings.nAvg*100.0);
	if (PMD2_SendToRemote.field.measurementReading.sensitivityLevel==LOW){
		PMD2_SendToRemote.field.measurementReading.concentration=PMD2_SendToRemote.field.measurementReading.concentrationRaw*PMD2_SendToRemote.field.workingSettings.calibLS;
		concMean*=PMD2_SendToRemote.field.workingSettings.calibLS;
		PMD2_SendToRemote.field.measurementReading.concentrationB*=PMD2_SendToRemote.field.workingSettings.calibLS;
	}else{
	 	PMD2_SendToRemote.field.measurementReading.concentration=PMD2_SendToRemote.field.measurementReading.concentrationRaw*PMD2_SendToRemote.field.workingSettings.calibHS;
	 	concMean*=PMD2_SendToRemote.field.workingSettings.calibHS;
		PMD2_SendToRemote.field.measurementReading.concentrationB*=PMD2_SendToRemote.field.workingSettings.calibHS;;
  }
	PMD2_SendToRemote.field.measurementReading.concentration-=PMD2_SendToRemote.field.workingSettings.testCellConcentration;
	PMD2_SendToRemote.field.measurementReading.concentrationB-=PMD2_SendToRemote.field.workingSettings.testCellConcentration;
	if(PMD2_SendToRemote.field.measurementReading.concentration<-20.0f){
		//PMD2_SendToRemote.field.workingSettings.testCellConcentration+=PMD2_SendToRemote.field.measurementReading.concentration;
		PMD2_ReceiveFromRemote.field.workingSettings.testCellConcentration=PMD2_SendToRemote.field.workingSettings.testCellConcentration;
		PMD2_PreviousReceiveFromRemote.field.workingSettings.testCellConcentration=PMD2_ReceiveFromRemote.field.workingSettings.testCellConcentration;
		PMD2_SendToRemote.field.measurementReading.concentration=0.0;
	}
	PMD2_ReceiveFromRemote.field.measurementReading.concentration=PMD2_SendToRemote.field.measurementReading.concentration;
	PMD2_PreviousReceiveFromRemote.field.measurementReading.concentration=PMD2_ReceiveFromRemote.field.measurementReading.concentration;
	PMD2_ReceiveFromRemote.field.measurementReading.concentrationB=PMD2_SendToRemote.field.measurementReading.concentrationB;
	PMD2_PreviousReceiveFromRemote.field.measurementReading.concentrationB=PMD2_ReceiveFromRemote.field.measurementReading.concentrationB;
	PMD2_ReceiveFromRemote.field.measurementReading.concentrationRaw=PMD2_SendToRemote.field.measurementReading.concentrationRaw;
	PMD2_PreviousReceiveFromRemote.field.measurementReading.concentrationRaw=PMD2_ReceiveFromRemote.field.measurementReading.concentrationRaw;
}


void change_settings_state(void){
//	set_active_state(CHANGE_STATE);
//	if (PMD2_SendToRemote.field.measurementSettings.nAcqPoint!=nSamplesDef){
//			PMD2_SendToRemote.field.measurementSettings.nAcqPoint=nSamplesDef;
//	}
//	if (PMD2_SendToRemote.field.workingSettings.laserTemperature!=tLasDef){
//		set_ad5693_laser_temperature(PMD2_SendToRemote.field.workingSettings.laserTemperature);
//		tLasDef=PMD2_SendToRemote.field.workingSettings.laserTemperature;
//	}
//	if (PMD2_SendToRemote.field.workingSettings.laserCurrentHS!=iLasHSDef){
//		set_ad5693_laser_bias_current(PMD2_SendToRemote.field.workingSettings.laserCurrentHS);
//		iLasHSDef=PMD2_SendToRemote.field.workingSettings.laserCurrentHS;
//	}
//	if (PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS!=iAMPHSDef){
//		set_laser_scan_amplitude(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS);
//		for (uint16_t it=0;it<1024;it++){
//			service.waveform[it] = 1.225/3.0*4096.0-(triangleWavePointTable[it])*(1672.0/4096.0)*PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS/2.6;
//		}
//		iAMPHSDef=PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS;
////			htim6.Init.Period = 690;
//		//MX_TIM6_Init();
//		//HAL_TIM_Base_Start(&htim6); 
//	}                   
//	 if (PMD2_SendToRemote.field.measurementSettings.nAvg!=nMeanDef){
//			nMeanDef=PMD2_SendToRemote.field.measurementSettings.nAvg;
//	}
//	 if (PMD2_SendToRemote.field.workingSettings.laserRampFrequency!=iScanFreqDef){
//			//start_laser_scan(PMD2_SendToRemote.field.workingSettings.laserRampFrequency);
//			iScanFreqDef=PMD2_SendToRemote.field.workingSettings.laserRampFrequency;
//	}
//	if (PMD2_SendToRemote.field.measurementSettings.PHDBackground!=lDCDef){
//			set_ad5693_DC_level(PMD2_SendToRemote.field.measurementSettings.PHDBackground);
//			lDCDef=PMD2_SendToRemote.field.measurementSettings.PHDBackground;
//	}
//	if (PMD2_SendToRemote.field.measurementSettings.PHDGain!=amplResistorValueDef){
//		AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_A, PMD2_SendToRemote.field.measurementSettings.PHDGain, 0);
////			HAL_Delay(100);
//		AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_B, PMD2_SendToRemote.field.measurementSettings.PHDGain, 0);
//		amplResistorValueDef=PMD2_SendToRemote.field.measurementSettings.PHDGain;
//	}
//	ptrTaskManager = measurement_housekeeping_state;
}


void minV(uint32_t *v,uint16_t dim, uint16_t *xmin, uint32_t *ymin){
  *xmin=0;*ymin=*v;
  for (uint16_t i=1; i<dim; i++)
  {
    if (*ymin>*(v+i)){
        *ymin=*(v+i);
        *xmin=i;
    }
  }
}

void meanv(uint32_t *v,	float *y_mean, uint16_t dim){
	*y_mean=0.0;
	for (uint16_t i=0; i<dim; i++){
		*y_mean+=*(v+i);	
	}	
	*y_mean/=dim;
}


void minmaxmeanV(uint32_t *v,uint16_t dim,float *y_mean, uint16_t *xmin, uint32_t *ymin, uint16_t *xmax, uint32_t *ymax){
  *xmin=0;*ymin=*v;
	*xmax=0;*ymax=*v;
	*y_mean=0.0;
  for (uint16_t i=1; i<dim; i++)
  {
    if (*ymin>*(v+i)){
        *ymin=*(v+i);
        *xmin=i;
    }
		if (*ymax<*(v+i)){
        *ymax=*(v+i);
        *xmax=i;
    }
		*y_mean+=*(v+i);	
  }
	*y_mean/=dim;
}

void minmaxV(uint32_t *v,uint16_t dim, uint16_t *xmin, uint32_t *ymin, uint16_t *xmax, uint32_t *ymax){
  *xmin=0;*ymin=*v;
	*xmax=0;*ymax=*v;
  for (uint16_t i=1; i<dim; i++)
  {
    if (*ymin>*(v+i)){
        *ymin=*(v+i);
        *xmin=i;
    }
		if (*ymax<*(v+i)){
        *ymax=*(v+i);
        *xmax=i;
    }
  }
}



void smooth(uint8_t nsm, uint32_t *x, uint16_t dim){
  float v[1000];
	uint8_t nsmtmp;

  for (uint16_t i=0; i<dim;i++){
		*(v+i)=*(x+i);
		nsmtmp=nsm;
		if (i<nsm)
			nsmtmp=i;
		if (dim-1-i<nsm)
				nsmtmp=dim-1-i;
		for (uint8_t j=0; j<nsmtmp; j++){
			*(v+i)+=*(x+i+j+1)+*(x+i-j-1);
		}
    *(v+i)/=(2.0f*nsmtmp+1.0f);
  }
  for (uint16_t i=0; i<dim;i++)
    *(x+i)=(uint32_t)(*(v+i)+0.5f);
  
}


void line_center(){
  int16_t diff;
  uint16_t xmin, xmax, center;
  uint32_t ymin1, ymax;
  float currStep,limp;
  center= (PMD2_SendToRemote.field.measurementSettings.nAcqPoint/2-2*PMD2_SendToRemote.field.measurementSettings.deltaX)/2; 
	if (timeMode==0){
		minmaxV(&(service.adcValueElaborated[center-15]),30, &xmin, &ymin1,&xmax, &ymax);
		xmin+=center-15;
		currStep=0.001f;
		limp=3;
	}else if (timeMode==1){
		minmaxV(&(service.adcValueElaborated[center-7]),15, &xmin, &ymin1,&xmax, &ymax);
		xmin+=center-7;
		currStep=0.0015f;
		limp=2;
	}else if(timeMode==2){
		minmaxV(&(service.adcValueElaborated[center-4]),8, &xmin, &ymin1,&xmax, &ymax);
		xmin+=center-4;
		currStep=0.003f;
		limp=1.5;
	}
  diff=xmin-center;
  if (abs(diff)>limp && PMD2_SendToRemote.field.measurementReading.sensitivityLevel==HIGH && PMD2_SendToRemote.field.flag.centerFlag==ON && 
		((PMD2_SendToRemote.field.workingSettings.laserCurrentHS+diff*currStep)<iLasHSDef+0.5f && (PMD2_SendToRemote.field.workingSettings.laserCurrentHS+diff*currStep)>iLasHSDef-0.5f)){
    PMD2_SendToRemote.field.workingSettings.laserCurrentHS=PMD2_SendToRemote.field.workingSettings.laserCurrentHS-diff*currStep;
		PMD2_ReceiveFromRemote.field.workingSettings.laserCurrentHS=PMD2_SendToRemote.field.workingSettings.laserCurrentHS;
		PMD2_PreviousReceiveFromRemote.field.workingSettings.laserCurrentHS=PMD2_SendToRemote.field.workingSettings.laserCurrentHS;
    set_ad5693_laser_bias_current(PMD2_SendToRemote.field.workingSettings.laserCurrentHS);
  }
  if (abs(diff) && PMD2_SendToRemote.field.measurementReading.sensitivityLevel==LOW && PMD2_SendToRemote.field.measurementReading.concentration<500000 && PMD2_SendToRemote.field.flag.centerFlag==ON &&
			((PMD2_SendToRemote.field.workingSettings.laserCurrentLS+diff*currStep)<iLasLSDef+0.5f && (PMD2_SendToRemote.field.workingSettings.laserCurrentLS+diff*currStep)>iLasLSDef-0.5f)){
    PMD2_SendToRemote.field.workingSettings.laserCurrentLS=PMD2_SendToRemote.field.workingSettings.laserCurrentLS-diff*currStep;
		PMD2_ReceiveFromRemote.field.workingSettings.laserCurrentLS=PMD2_SendToRemote.field.workingSettings.laserCurrentLS;
		PMD2_PreviousReceiveFromRemote.field.workingSettings.laserCurrentLS=PMD2_SendToRemote.field.workingSettings.laserCurrentLS;
		set_ad5693_laser_bias_current(PMD2_SendToRemote.field.workingSettings.laserCurrentLS);
  }
}


void taskSPI_communication(void)
{
	/*
	realTime.timeStamp=epochTimeStamp_now(&hrtc);
	if (service.ready==1)
		set_active_state(41);
	sprintf (service.tmpBuf ,"*%uld %f %f %u\r\n",realTime.timeStamp, PMD2_SendToRemote.field.measurementReading.concentration, PMD2_SendToRemote.field.measurementReading.concentrationB, PMD2_SendToRemote.field.measurementReading.activeState); 
	HAL_UART_Transmit ( &huart1, ( uint8_t * ) service.tmpBuf ,strlen ( service.tmpBuf ),1000 );
	if (PMD2_SendToRemote.field.measurementReading.activeState>100)
		ptrTaskManager = error_state;
	else
		ptrTaskManager = measurement_housekeeping_state;
	*/
}



//load from EEPROM must be called after flag initialization
void load_struct_from_eeprom(void){
	uint8_t stop=0;
	uint32_t checkInit1;
	uint32_t checkInit2;
	
	PMD2_SendToRemote.field.system.Serial_Nr=readLong(EEP_SERIAL_NUMBER);
	if(PMD2_SendToRemote.field.system.Serial_Nr !=PMD2_SendToRemote.field.system.Serial_Nr){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.system.calYear=readWord(EEP_CAL_YEAR);
	if(PMD2_SendToRemote.field.system.calYear !=PMD2_SendToRemote.field.system.calYear){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.system.calMonth=readWord(EEP_CAL_MONTH);
	if(PMD2_SendToRemote.field.system.calMonth !=PMD2_SendToRemote.field.system.calMonth){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.system.calDay=readWord(EEP_CAL_DAY);
	if(PMD2_SendToRemote.field.system.calDay !=PMD2_SendToRemote.field.system.calDay){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.system.modbusID = readLong(EEP_MODBUS_ID);
	if(PMD2_SendToRemote.field.system.modbusID !=PMD2_SendToRemote.field.system.modbusID ){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	if (PMD2_SendToRemote.field.system.modbusID>255)
		PMD2_SendToRemote.field.system.modbusID=49;
	PMD2_SendToRemote.field.workingSettings.laserID = readLong(EEP_LASER_ID);
	if(PMD2_SendToRemote.field.workingSettings.laserID !=PMD2_SendToRemote.field.workingSettings.laserID ){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.workingSettings.laserTemperature = readFloat(EEP_LASER_TEMPERATURE);
	if(PMD2_SendToRemote.field.workingSettings.laserTemperature!=PMD2_SendToRemote.field.workingSettings.laserTemperature){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
		stop=1;
	}	
	PMD2_SendToRemote.field.workingSettings.laserCurrentHS = readFloat(EEP_LASER_CURRENT_HS);
	if(PMD2_SendToRemote.field.workingSettings.laserCurrentHS!=PMD2_SendToRemote.field.workingSettings.laserCurrentHS){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
		stop=1;
	}
	PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS = readFloat(EEP_LASER_RAMP_AMPLITUDE_HS);
	if(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS!=PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
		stop=1;
	}
	PMD2_SendToRemote.field.workingSettings.laserRampFrequency = readFloat(EEP_LASER_RAMP_FREQUENCY);
	if(PMD2_SendToRemote.field.workingSettings.laserRampFrequency!=PMD2_SendToRemote.field.workingSettings.laserRampFrequency){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
		stop=1;
	}
	PMD2_SendToRemote.field.workingSettings.laserCurrentLS = readFloat(EEP_LASER_CURRENT_LS);
	if(PMD2_SendToRemote.field.workingSettings.laserCurrentLS !=PMD2_SendToRemote.field.workingSettings.laserCurrentLS ){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
		stop=1;
	}
	PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS = readFloat(EEP_LASER_RAMP_AMPLITUDE_LS);
	if(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS!=PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
		stop=1;
	}
	PMD2_SendToRemote.field.workingSettings.calibHS = readFloat(EEP_CALIB_HIGH);
	if(PMD2_SendToRemote.field.workingSettings.calibHS !=PMD2_SendToRemote.field.workingSettings.calibHS){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
		stop=1;
	}
	PMD2_SendToRemote.field.workingSettings.calibLS = readFloat(EEP_CALIB_LOW);
	if(PMD2_SendToRemote.field.workingSettings.calibLS !=PMD2_SendToRemote.field.workingSettings.calibLS){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
		stop=1;
	}
	PMD2_SendToRemote.field.workingSettings.testCellConcentration= readFloat(EEP_TEST_CELL_CONCENTRATION);
	if(PMD2_SendToRemote.field.workingSettings.testCellConcentration !=PMD2_SendToRemote.field.workingSettings.testCellConcentration){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}  
	PMD2_SendToRemote.field.measurementSettings.nAvg= readWord(EEP_N_ACQUISITION);
	if(PMD2_SendToRemote.field.measurementSettings.nAvg !=PMD2_SendToRemote.field.measurementSettings.nAvg){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
 	PMD2_SendToRemote.field.measurementSettings.nAcqPoint= readWord(EEP_N_ACQ_POINT);
	if(PMD2_SendToRemote.field.measurementSettings.nAcqPoint !=PMD2_SendToRemote.field.measurementSettings.nAcqPoint){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}

	PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint= readWord(EEP_N_SMOOTH_POINT);
	if(PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint !=PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.measurementSettings.deltaX= readWord(EEP_DELTA_X);
	if(PMD2_SendToRemote.field.measurementSettings.deltaX !=PMD2_SendToRemote.field.measurementSettings.deltaX){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	} 
	PMD2_SendToRemote.field.measurementSettings.startPoint= readWord(EEP_START_P);
	if(PMD2_SendToRemote.field.measurementSettings.startPoint !=PMD2_SendToRemote.field.measurementSettings.startPoint){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	} 	
	PMD2_SendToRemote.field.measurementSettings.endPoint= readWord(EEP_END_P);
	if(PMD2_SendToRemote.field.measurementSettings.endPoint !=PMD2_SendToRemote.field.measurementSettings.endPoint){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	} 	
	PMD2_SendToRemote.field.measurementSettings.waitBefore= readWord(EEP_WAIT_BEFORE);
	if(PMD2_SendToRemote.field.measurementSettings.waitBefore !=PMD2_SendToRemote.field.measurementSettings.waitBefore){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}  
	PMD2_SendToRemote.field.measurementSettings.waitBefore= readWord(EEP_WAIT_BEFORE);
	if(PMD2_SendToRemote.field.measurementSettings.waitBefore !=PMD2_SendToRemote.field.measurementSettings.waitBefore){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}  
	PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle= readWord(EEP_WAIT_INTHEMIDDLE);
	if(PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle !=PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}  
	checkInit1=readLong(EEP_CHECK_INIT1);
	checkInit2=readLong(EEP_CHECK_INIT2);
	if (checkInit1!=1234567890 || checkInit2!=1234567890)
		PMD2_SendToRemote.field.flag.sensorMode=SET;
	
// read calibration
	
}
void load_struct_from_eeprom_OLD(void){
	PMD2_SendToRemote.field.system.Serial_Nr=readLong(OLD_EEP_SERIAL_NUMBER);
	if(PMD2_SendToRemote.field.system.Serial_Nr !=PMD2_SendToRemote.field.system.Serial_Nr){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.system.calYear=readWord(OLD_EEP_CAL_YEAR);
	if(PMD2_SendToRemote.field.system.calYear !=PMD2_SendToRemote.field.system.calYear){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.system.calMonth=readWord(OLD_EEP_CAL_MONTH);
	if(PMD2_SendToRemote.field.system.calMonth !=PMD2_SendToRemote.field.system.calMonth){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.system.calDay=readWord(OLD_EEP_CAL_DAY);
	if(PMD2_SendToRemote.field.system.calDay !=PMD2_SendToRemote.field.system.calDay){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.workingSettings.laserID = readLong(OLD_EEP_LASER_ID);
	if(PMD2_SendToRemote.field.workingSettings.laserID !=PMD2_SendToRemote.field.workingSettings.laserID ){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.workingSettings.laserTemperature = readFloat(OLD_EEP_LASER_TEMPERATURE);
	if(PMD2_SendToRemote.field.workingSettings.laserTemperature!=PMD2_SendToRemote.field.workingSettings.laserTemperature){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}	
	PMD2_SendToRemote.field.workingSettings.laserCurrentHS = readFloat(OLD_EEP_LASER_CURRENT_HS);
	if(PMD2_SendToRemote.field.workingSettings.laserCurrentHS!=PMD2_SendToRemote.field.workingSettings.laserCurrentHS){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS = readFloat(OLD_EEP_LASER_RAMP_AMPLITUDE_HS);
	if(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS!=PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.workingSettings.laserRampFrequency = readFloat(OLD_EEP_LASER_RAMP_FREQUENCY);
	if(PMD2_SendToRemote.field.workingSettings.laserRampFrequency!=PMD2_SendToRemote.field.workingSettings.laserRampFrequency){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.workingSettings.laserCurrentLS = readFloat(OLD_EEP_LASER_CURRENT_LS);
	if(PMD2_SendToRemote.field.workingSettings.laserCurrentLS !=PMD2_SendToRemote.field.workingSettings.laserCurrentLS ){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS = readFloat(OLD_EEP_LASER_RAMP_AMPLITUDE_LS);
	if(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS!=PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.workingSettings.calibHS = readFloat(OLD_EEP_CALIB_HIGH);
	if(PMD2_SendToRemote.field.workingSettings.calibHS !=PMD2_SendToRemote.field.workingSettings.calibHS){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.workingSettings.calibLS = readFloat(OLD_EEP_CALIB_LOW);
	if(PMD2_SendToRemote.field.workingSettings.calibLS !=PMD2_SendToRemote.field.workingSettings.calibLS){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}
	PMD2_SendToRemote.field.workingSettings.testCellConcentration= readFloat(OLD_EEP_TEST_CELL_CONCENTRATION);
	if(PMD2_SendToRemote.field.workingSettings.testCellConcentration !=PMD2_SendToRemote.field.workingSettings.testCellConcentration){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}  
	PMD2_SendToRemote.field.measurementSettings.nAvg= readFloat(OLD_EEP_NUMBER_OF_ACQUISITION);
	if(PMD2_SendToRemote.field.measurementSettings.nAvg !=PMD2_SendToRemote.field.measurementSettings.nAvg){
		PMD2_SendToRemote.field.measurementReading.errorID =EEP_ERROR;
	}  
	PMD2_SendToRemote.field.measurementSettings.startPoint=52; PMD2_SendToRemote.field.measurementSettings.endPoint=PMD2_SendToRemote.field.measurementSettings.startPoint+30;
	PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint=5;  PMD2_SendToRemote.field.measurementSettings.deltaX=45;
	PMD2_SendToRemote.field.measurementSettings.nAcqPoint=450;    PMD2_SendToRemote.field.measurementSettings.nAvg=100;
	PMD2_SendToRemote.field.measurementSettings.waitBefore=500;PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle=280;	
	PMD2_SendToRemote.field.workingSettings.laserRampFrequency=260; //260Hz=3.84ms 300*1024/(80MHz) (0.5+0.28+0.5)*0.9 +450*6us=1.15+2.7 
	//write_struct_2_eeprom();
}

void write_struct_2_eeprom(void){
		writeLong(EEP_SERIAL_NUMBER,PMD2_SendToRemote.field.system.Serial_Nr);
		writeWord(EEP_CAL_YEAR,PMD2_SendToRemote.field.system.calYear);
		writeWord(EEP_CAL_MONTH,PMD2_SendToRemote.field.system.calMonth);
		writeWord(EEP_CAL_DAY,PMD2_SendToRemote.field.system.calDay);
		writeWord(EEP_MODBUS_ID,PMD2_SendToRemote.field.system.modbusID);
		writeLong(EEP_LASER_ID, PMD2_SendToRemote.field.workingSettings.laserID);
		writeFloat(EEP_LASER_TEMPERATURE, PMD2_SendToRemote.field.workingSettings.laserTemperature);
		writeFloat(EEP_LASER_CURRENT_HS, PMD2_SendToRemote.field.workingSettings.laserCurrentHS);
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_HS, PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS);
		writeFloat(EEP_LASER_RAMP_FREQUENCY, PMD2_SendToRemote.field.workingSettings.laserRampFrequency);
		writeFloat(EEP_LASER_CURRENT_LS, PMD2_SendToRemote.field.workingSettings.laserCurrentLS);
		writeFloat(EEP_LASER_RAMP_AMPLITUDE_LS, PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS);
		writeFloat(EEP_TEST_CELL_CONCENTRATION, PMD2_SendToRemote.field.workingSettings.testCellConcentration);
		writeFloat(EEP_CALIB_HIGH, PMD2_SendToRemote.field.workingSettings.calibHS);
		writeFloat(EEP_CALIB_LOW, PMD2_SendToRemote.field.workingSettings.calibLS);
		writeWord(EEP_N_ACQUISITION, PMD2_SendToRemote.field.measurementSettings.nAvg);
		writeWord(EEP_N_ACQ_POINT, PMD2_SendToRemote.field.measurementSettings.nAcqPoint);
		writeWord(EEP_N_SMOOTH_POINT, PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint);
		writeWord(EEP_DELTA_X, PMD2_SendToRemote.field.measurementSettings.deltaX);
		writeWord(EEP_START_P, PMD2_SendToRemote.field.measurementSettings.startPoint);
		writeWord(EEP_END_P, PMD2_SendToRemote.field.measurementSettings.endPoint);
		writeWord(EEP_WAIT_BEFORE, PMD2_SendToRemote.field.measurementSettings.waitBefore);
		writeWord(EEP_WAIT_INTHEMIDDLE, PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle);
		writeLong(EEP_CHECK_INIT1, 1234567890);
		writeLong(EEP_CHECK_INIT2, 1234567890);
}
void init_BLE_UART(void){
	sprintf( (char *)service.tmpBuf, "ATZ\r");
  sendUsart2( (uint8_t *)service.tmpBuf );
	HAL_Delay(1000);
	sprintf( (char *)service.tmpBuf, "AT+FACTORYRESET\r");
  sendUsart2( (uint8_t *)service.tmpBuf );
	HAL_Delay(1000);
	sprintf( (char *)service.tmpBuf, "AT+GAPDEVNAME=SENSIT PMD2\r");
  sendUsart2( (uint8_t *)service.tmpBuf );
	HAL_Delay(1000);
	sprintf( (char *)service.tmpBuf, "AT+GATTADDSERVICE=UUID=0x180D\r");
  sendUsart2( (uint8_t *)service.tmpBuf );
	HAL_Delay(1000);
	sprintf( (char *)service.tmpBuf, "AT+GATTADDCHAR=UUID=0x2A37, PROPERTIES=0x10, MIN_LEN=2, MAX_LEN=3, VALUE=00-40\r");
  sendUsart2( (uint8_t *)service.tmpBuf );
	HAL_Delay(1000);
	sprintf( (char *)service.tmpBuf, "AT+GATTADDCHAR=UUID=0x2A38, PROPERTIES=0x02, MIN_LEN=1, VALUE=3\r");
  sendUsart2( (uint8_t *)service.tmpBuf );
	HAL_Delay(1000);
	sprintf( (char *)service.tmpBuf, "AT+GAPSETADVDATA=02-01-06-05-02-0d-18-0a-18\r");
	HAL_Delay(1000);
  sendUsart2( (uint8_t *)service.tmpBuf );
	sprintf( (char *)service.tmpBuf, "ATZ\r");
  sendUsart2( (uint8_t *)service.tmpBuf );
	HAL_Delay(1000);
}

void print_BLE_UART(void){
	sprintf( (char *)service.tmpBuf, "AT+GATTCHAR=1,00-%x\r",(uint32_t)PMD2_SendToRemote.field.measurementReading.concentration);
  sendUsart2( (uint8_t *)service.tmpBuf );
}

void set_sensitivity(uint8_t range){
	PMD2_SendToRemote.field.measurementReading.sensitivityLevel=range;
	PMD2_ReceiveFromRemote.field.measurementReading.sensitivityLevel=range;
	PMD2_PreviousReceiveFromRemote.field.measurementReading.sensitivityLevel=range;
	
}
void reset_switchRange(void){
	PMD2_SendToRemote.field.flag.switchRange=0;PMD2_ReceiveFromRemote.field.flag.switchRange=0;PMD2_PreviousReceiveFromRemote.field.flag.switchRange=0;
}
void set_active_state(uint16_t number){
	PMD2_SendToRemote.field.measurementReading.activeState=number;PMD2_ReceiveFromRemote.field.measurementReading.activeState=number;
	PMD2_PreviousReceiveFromRemote.field.measurementReading.activeState=number;
}

void activate_print(void){
	PMD2_SendToRemote.field.flag.print=ON;	PMD2_ReceiveFromRemote.field.flag.print=ON;	PMD2_PreviousReceiveFromRemote.field.flag.print=ON;
}
void deactivate_print(void){
	PMD2_SendToRemote.field.flag.print=OFF;	PMD2_ReceiveFromRemote.field.flag.print=OFF;	PMD2_PreviousReceiveFromRemote.field.flag.print=OFF;
}
void	init_ramp(uint16_t freqRamp, float amp){
	uint16_t timerRampa=(uint16_t)((80000000.0/1024.0)/freqRamp);
  htim6.Init.Period = timerRampa;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
	set_laser_scan_amplitude(amp);
}

void 	start_ramp(void){
	HAL_TIM_Base_Start(&htim6);  
	if (BOARD_VERSION==4)
		HAL_DAC_Start_DMA(&hdac1, DAC_CHANNEL_2, (uint32_t*)service.waveform, 1024, DAC_ALIGN_12B_R); 
	if (BOARD_VERSION==3)
		HAL_DAC_Start_DMA(&hdac1, DAC_CHANNEL_1, (uint32_t*)service.waveform, 1024, DAC_ALIGN_12B_R); 
}

void stop_ramp(void){
	HAL_TIM_Base_Stop(&htim6);  
	HAL_DAC_Stop_DMA(&hdac1, DAC_CHANNEL_2); 
}


void check_eeprom_value(void){
	if(PMD2_SendToRemote.field.system.calYear <2020 || PMD2_SendToRemote.field.system.calYear >2100)
		PMD2_SendToRemote.field.system.calYear =2020;

	if(PMD2_SendToRemote.field.system.calMonth <1 || PMD2_SendToRemote.field.system.calMonth>12)
		PMD2_SendToRemote.field.system.calMonth =1;

	if(PMD2_SendToRemote.field.system.calDay <1 || PMD2_SendToRemote.field.system.calDay>31)
		PMD2_SendToRemote.field.system.calDay =1;

	if (PMD2_SendToRemote.field.system.modbusID>255 || PMD2_SendToRemote.field.system.modbusID<1)
		PMD2_SendToRemote.field.system.modbusID=50;

	if(PMD2_SendToRemote.field.workingSettings.laserTemperature<5 || PMD2_SendToRemote.field.workingSettings.laserTemperature>30)
		PMD2_SendToRemote.field.workingSettings.laserTemperature=20;

	if(PMD2_SendToRemote.field.workingSettings.laserCurrentHS<3 || PMD2_SendToRemote.field.workingSettings.laserCurrentHS>9)
		PMD2_SendToRemote.field.workingSettings.laserCurrentHS = 7;
	
	if(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS<0.1 || PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS>1.5)
		PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS=1;

	if(PMD2_SendToRemote.field.workingSettings.laserRampFrequency<200 || PMD2_SendToRemote.field.workingSettings.laserRampFrequency>10000)
		PMD2_SendToRemote.field.workingSettings.laserRampFrequency = 260;

	if(PMD2_SendToRemote.field.workingSettings.laserCurrentLS<3 || PMD2_SendToRemote.field.workingSettings.laserCurrentLS>9 )
		PMD2_SendToRemote.field.workingSettings.laserCurrentLS=5;
 
	if(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS<0.1 || PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS>1.5)
		PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS=1;

	if(PMD2_SendToRemote.field.workingSettings.calibHS<0.01 || PMD2_SendToRemote.field.workingSettings.calibHS>10)
		PMD2_SendToRemote.field.workingSettings.calibHS=0.5;

	if(PMD2_SendToRemote.field.workingSettings.calibLS<1 || PMD2_SendToRemote.field.workingSettings.calibLS>1000)
		PMD2_SendToRemote.field.workingSettings.calibLS=50;

	if(PMD2_SendToRemote.field.workingSettings.testCellConcentration<1 || PMD2_SendToRemote.field.workingSettings.testCellConcentration>100)
		PMD2_SendToRemote.field.workingSettings.testCellConcentration=20;

	if(PMD2_SendToRemote.field.measurementSettings.nAvg<1 || PMD2_SendToRemote.field.measurementSettings.nAvg>1000)
		PMD2_SendToRemote.field.measurementSettings.nAvg =100;
	
	if(PMD2_SendToRemote.field.measurementSettings.nAcqPoint<100 || PMD2_SendToRemote.field.measurementSettings.nAcqPoint>500)
		PMD2_SendToRemote.field.measurementSettings.nAcqPoint =450;
	
	if(PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint <1 || PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint>10)
		PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint=5;
		
	if(PMD2_SendToRemote.field.measurementSettings.deltaX<10 || PMD2_SendToRemote.field.measurementSettings.deltaX>50)
		PMD2_SendToRemote.field.measurementSettings.deltaX =45;
	 
	if(PMD2_SendToRemote.field.measurementSettings.startPoint<1 || PMD2_SendToRemote.field.measurementSettings.startPoint>100)
		PMD2_SendToRemote.field.measurementSettings.startPoint =52;

	if(PMD2_SendToRemote.field.measurementSettings.endPoint<1 || PMD2_SendToRemote.field.measurementSettings.endPoint>100)
		PMD2_SendToRemote.field.measurementSettings.endPoint=PMD2_SendToRemote.field.measurementSettings.startPoint+30;

	if(PMD2_SendToRemote.field.measurementSettings.waitBefore<100 || PMD2_SendToRemote.field.measurementSettings.waitBefore>1000)
		PMD2_SendToRemote.field.measurementSettings.waitBefore= 500;

	if(PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle<100 || PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle>500)
		PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle= 250;
}