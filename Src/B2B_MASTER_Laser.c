#include "main.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "B2B_MASTER_Laser.h"
#include "sensit.h"


extern UART_HandleTypeDef huart1;

#define B2B_USART_END_RECORD	'\n'
#define KT_1MS_USART1_TIMEOUT		1000

uint16_t idxUSART1_RxBuffer =0;
uint16_t flgB2B_USART_RECORD;
uint32_t usartRX_Error=0L;
uint32_t B2B_flgB2B_USART_RECORD_numPkt=0L;
uint32_t B2B_flgB2B_USART_RECORD_numPktError=0L;
uint16_t lenUSART1_RxBuffer =0;

uint8_t  USART1_RxBuffer[DIM_USART1_RxBuffer];
uint8_t  USART1_RxAnalyzeBuffer[DIM_USART1_RxBuffer];
uint8_t  USART1_singleCharRx;

/*
 *
 * USART SECTION
 *
 */

void B2B_USART_MAIN_Laser_CPU(void)
{
		HAL_UART_Receive_IT(&huart1, (uint8_t *) (&USART1_singleCharRx), 1);

}


void B2B_USART_LaserReceive_completeTXRX (void) 
{
	/*
	if (huart1.ErrorCode == HAL_UART_ERROR_ORE){
        // remove the error condition
        huart1.ErrorCode = HAL_UART_ERROR_NONE;
        // set the correct state, so that the UART_RX_IT works correctly
        huart1->State = HAL_UART_STATE_BUSY_RX;
    }
	*/

	USART1_RxBuffer[idxUSART1_RxBuffer]=USART1_singleCharRx;	
	HAL_UART_Receive_IT(&huart1, (uint8_t *) (&USART1_singleCharRx), 1);  
	if(USART1_RxBuffer[idxUSART1_RxBuffer] == B2B_USART_END_RECORD) {
		lenUSART1_RxBuffer=idxUSART1_RxBuffer;
		idxUSART1_RxBuffer = 0;  
		flgB2B_USART_RECORD = 1;
		memcpy(USART1_RxAnalyzeBuffer, USART1_RxBuffer, lenUSART1_RxBuffer);
	} else {
		++idxUSART1_RxBuffer;
		if ( idxUSART1_RxBuffer>=DIM_USART1_RxBuffer)
			idxUSART1_RxBuffer = 0;   
	}
}

void B2B_USART_EnableCPU_TX_Laser_RX(void)
{
	HAL_GPIO_WritePin(USART1_RTS_GPIO_Port, USART1_RTS_Pin, GPIO_PIN_RESET);
}

void B2B_USART_DisableCPU_TX_Laser_RX(void)
{
	HAL_GPIO_WritePin(USART1_RTS_GPIO_Port, USART1_RTS_Pin, GPIO_PIN_SET);
}


