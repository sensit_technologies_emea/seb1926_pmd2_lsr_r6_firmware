/****************************************
* File Name          : S_I2C.c
* Author             : Aivar Saar
* Version            : V1.0
* Date               : 10/08/2008
* Description        : Software I2C
********************************************************************************
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "i2c_24AA512.h"
#include "stdio.h"
#include "main.h"


#define EEP_WRITE_ENABLE  HAL_GPIO_WritePin(E2PROM_WP_GPIO_Port,E2PROM_WP_Pin,GPIO_PIN_RESET)
#define EEP_WRITE_DISABLE HAL_GPIO_WritePin(E2PROM_WP_GPIO_Port,E2PROM_WP_Pin,GPIO_PIN_SET)

extern I2C_HandleTypeDef hi2c2;

/**
  * @brief               : This function handles Writing Array of Bytes on the specific Address .
  * 					   This program have this feature that don't force you to use absolute 16 bytes
  * 					   you can use more than 16 bytes buffer.
  * @param  hi2c         : Pointer to a I2C_HandleTypeDef structure that contains
  *                        the configuration information for the specified I2C.
  * @param	DevAddress   : specifies the slave address to be programmed(EEPROM ADDRESS).
  * @param	MemAddress   : Internal memory address (WHERE YOU WANNA WRITE TO)
  * @param	pData	     : Pointer to data buffer
  * @param  TxBufferSize : Amount of data you wanna Write
  * @retval
  */
int16_t AT24AA512_WriteBytes(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress, uint8_t *pData, uint16_t TxBufferSize)
{
	int TimeOut;
  EEP_WRITE_ENABLE;
  uint8_t *pMemoryData =pData;
	
  for(int i=0; i<TxBufferSize; ++i) {
    if(HAL_I2C_Mem_Write(hi2c,(uint16_t)DevAddress,(uint16_t)MemAddress,I2C_MEMADD_SIZE_16BIT,pMemoryData,1,1000)!= HAL_OK) {
			return 0;
		}
		HAL_Delay(10);
		++pMemoryData;
		++MemAddress;
  }
  
  EEP_WRITE_DISABLE;
  return 1;
}


int16_t AT24AA512_ReadBytes(I2C_HandleTypeDef *hi2c, uint16_t DevAddress, uint16_t MemAddress, uint8_t *pData, uint16_t RxBufferSize)
{
  int TimeOut;
  uint8_t *pMemoryData =pData;

  for( int i=0; i<RxBufferSize; ++i )
  {
		TimeOut = 0;
		while(HAL_I2C_Mem_Read(hi2c,(uint16_t)DevAddress,(uint16_t)MemAddress,I2C_MEMADD_SIZE_16BIT,pMemoryData,(uint16_t)1,1000)!= HAL_OK && TimeOut < 10)
		{
			TimeOut++;
		}
		++pMemoryData;
		++MemAddress;		 
  }

  return 1;
}



uint16_t readWord ( uint32_t addr )
{
    uint16_t uiTmp;

		AT24AA512_ReadBytes (&hi2c2, EEP_AA512_ADDR_U8, addr,  ( uint8_t * ) &uiTmp, 2);
	
    return ( uiTmp );
}


void readBuffer (  uint8_t *  buf, uint32_t addr,uint16_t nr )
{
    AT24AA512_ReadBytes (&hi2c2, EEP_AA512_ADDR_U8, addr,  buf, nr);
}

void writeBuffer (  uint8_t *  buf, uint32_t addr,uint16_t nr )
{
		AT24AA512_WriteBytes(&hi2c2, EEP_AA512_ADDR_U8, addr, buf, nr);
}

void writeWord ( uint32_t addr, uint16_t dato )
{
	AT24AA512_WriteBytes(&hi2c2, EEP_AA512_ADDR_U8, addr, ( uint8_t * ) &dato, 2);
}

uint8_t readByte ( uint32_t addr )
{
    uint8_t ucTmp;

    AT24AA512_ReadBytes (&hi2c2, EEP_AA512_ADDR_U8, addr,  ( uint8_t * ) &ucTmp, 1);

    return ( ucTmp );
}

void writeByte ( uint32_t addr ,uint8_t dato )
{
    AT24AA512_WriteBytes(&hi2c2, EEP_AA512_ADDR_U8, addr, ( uint8_t * ) &dato, 1);
}

uint32_t readLong ( uint32_t addr )
{
    uint32_t ulTmp;

		AT24AA512_ReadBytes (&hi2c2, EEP_AA512_ADDR_U8, addr,  ( uint8_t * ) &ulTmp, 4);

    return ( ulTmp );
}

float  readFloat ( uint32_t addr )
{
    float ulTmp;

		AT24AA512_ReadBytes (&hi2c2, EEP_AA512_ADDR_U8, addr,  ( uint8_t * ) &ulTmp, 4);

    return ( ulTmp );
}

double  readDouble ( uint32_t addr )
{
    double ulTmp;

		AT24AA512_ReadBytes (&hi2c2, EEP_AA512_ADDR_U8, addr,  ( uint8_t * ) &ulTmp, 8);	

    return ( ulTmp );
}

void writeLong ( uint32_t addr, uint32_t dato )
{
    AT24AA512_WriteBytes(&hi2c2, EEP_AA512_ADDR_U8, addr, ( uint8_t * ) &dato, 4);
}


void incLong ( uint32_t addr )
{
    uint32_t dato;

    dato = readLong ( addr );
    ++dato;
    writeLong ( addr, dato );
}
void incFloat ( uint32_t addr )
{
    float dato;

    dato = readFloat( addr );
    dato=dato+1.5f;
    writeFloat ( addr, dato );
}

void writeFloat ( uint32_t addr, float dato )
{
    AT24AA512_WriteBytes(&hi2c2, EEP_AA512_ADDR_U8, addr, ( uint8_t * ) &dato, 4);
}

void writeDouble ( uint32_t addr, double dato )
{
    AT24AA512_WriteBytes(&hi2c2, EEP_AA512_ADDR_U8, addr, ( uint8_t * ) &dato, 8);
}
