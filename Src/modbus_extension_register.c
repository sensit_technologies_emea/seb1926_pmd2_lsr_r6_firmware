/****************************************
* File Name          : modbus_extension_register.c
* Author             : Luca Herzog
* Version            : V1.0
* Date               : 06/09/20108
* Description        : Modbus register
********************************************************************************
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "sensit.h"
#include "main.h"
#include "modbus_extension_register.h"
#include "i2c_24AA512.h"
#include "i2c_digitalpot_AD5243.h"
#include "vee.h"
#include "eeprom_address.h"
#include "dateEpoch.h"
#include "functions.h"
#include "taskmanager.h"
#include "out.h"

typeRegisterExtensions PMD2_SendToRemote;           		// Data for remote system
typeRegisterExtensions PMD2_ReceiveFromRemote;         	// Data from remote system
typeRegisterExtensions PMD2_PreviousReceiveFromRemote; 	// Compare PMD2_ReceiveFromRemote with this structure to verify new data

extern RTC_HandleTypeDef hrtc;
extern void acceptOnlySecondaryModbusChannel(void);

void clearRegisterExtensionReceiveStructure ( void )
{
    for ( int16_t i=0; i<DIM_UINT16_REGISTER_EXTERNSION; ++i ) {
        PMD2_ReceiveFromRemote.global[i]=0;
    }
}

void clearRegisterExtensionPreviousReceiveStructure ( void )
{
    for ( int16_t i=0; i<DIM_UINT16_REGISTER_EXTERNSION; ++i ) {
        PMD2_PreviousReceiveFromRemote.global[i]=0;
    }
}

void clearRegisterExtensionPreviousSendToRemote ( void )
{
    for ( int16_t i=0; i<DIM_UINT16_REGISTER_EXTERNSION; ++i ) {
        PMD2_SendToRemote.global[i]=0;
    }
}

void initAllRegisterExtensionStructure ( void )
{
    clearRegisterExtensionReceiveStructure();
    clearRegisterExtensionPreviousReceiveStructure();
    clearRegisterExtensionPreviousSendToRemote();
}

void copySrcToTwiceDest ( typeRegisterExtensions *srcStruct, typeRegisterExtensions *dest1_struct, typeRegisterExtensions *dest2_struct )
{
    uint16_t uitmp;
    for ( int16_t i=0; i<DIM_UINT16_REGISTER_EXTERNSION; ++i ) {
        uitmp = srcStruct->global[i];
        dest1_struct->global[i] = uitmp;
        dest2_struct->global[i] = uitmp;
    }
}

void checkAndReactToRemoteChanges ( void )
{
	int16_t flgChanged=FALSE;

	// Check for a generic change into structure
	for ( int16_t i=0; i<DIM_UINT16_REGISTER_EXTERNSION; ++i ) {
			if ( PMD2_ReceiveFromRemote.global[i] != PMD2_PreviousReceiveFromRemote.global[i] ) {
					flgChanged=TRUE;
					break;
			}
	}

    // if change a generic value, verifify in detail which data..
	if ( flgChanged==TRUE ) {
		//********************* SYSTEM ***************************//

		if (PMD2_ReceiveFromRemote.field.system.Serial_Nr != PMD2_PreviousReceiveFromRemote.field.system.Serial_Nr ) {
			PMD2_PreviousReceiveFromRemote.field.system.Serial_Nr = PMD2_ReceiveFromRemote.field.system.Serial_Nr;
			PMD2_SendToRemote.field.system.Serial_Nr = PMD2_ReceiveFromRemote.field.system.Serial_Nr;		
			writeWord(EEP_SERIAL_NUMBER, PMD2_SendToRemote.field.system.Serial_Nr);
		}			
		if (PMD2_ReceiveFromRemote.field.system.calYear != PMD2_PreviousReceiveFromRemote.field.system.calYear ) {
			PMD2_PreviousReceiveFromRemote.field.system.calYear = PMD2_ReceiveFromRemote.field.system.calYear;
			PMD2_SendToRemote.field.system.calYear = PMD2_ReceiveFromRemote.field.system.calYear;		
			writeWord(EEP_CAL_YEAR, PMD2_SendToRemote.field.system.calYear);
		}	        
		if (PMD2_ReceiveFromRemote.field.system.calMonth != PMD2_PreviousReceiveFromRemote.field.system.calMonth ) {
			PMD2_PreviousReceiveFromRemote.field.system.calMonth = PMD2_ReceiveFromRemote.field.system.calMonth;
			PMD2_SendToRemote.field.system.calMonth = PMD2_ReceiveFromRemote.field.system.calMonth;		
			writeWord(EEP_CAL_MONTH, PMD2_SendToRemote.field.system.calMonth);
		}
		if (PMD2_ReceiveFromRemote.field.system.calDay != PMD2_PreviousReceiveFromRemote.field.system.calDay ) {
			PMD2_PreviousReceiveFromRemote.field.system.calDay = PMD2_ReceiveFromRemote.field.system.calDay;
			PMD2_SendToRemote.field.system.calDay = PMD2_ReceiveFromRemote.field.system.calDay;		
			writeWord(EEP_CAL_DAY, PMD2_SendToRemote.field.system.calDay);
		}	
		 //********************* WORKING SETTINGS ***************************//
			if (PMD2_ReceiveFromRemote.field.workingSettings.laserID != PMD2_PreviousReceiveFromRemote.field.workingSettings.laserID ) {
			PMD2_PreviousReceiveFromRemote.field.workingSettings.laserID = PMD2_ReceiveFromRemote.field.workingSettings.laserID;
			PMD2_SendToRemote.field.workingSettings.laserID = PMD2_ReceiveFromRemote.field.workingSettings.laserID;						
		}	
		if (PMD2_ReceiveFromRemote.field.workingSettings.laserCurrentHS != PMD2_PreviousReceiveFromRemote.field.workingSettings.laserCurrentHS ) {
			if (PMD2_ReceiveFromRemote.field.workingSettings.laserCurrentHS>0 && PMD2_ReceiveFromRemote.field.workingSettings.laserCurrentHS<10){
				PMD2_PreviousReceiveFromRemote.field.workingSettings.laserCurrentHS = PMD2_ReceiveFromRemote.field.workingSettings.laserCurrentHS;
				PMD2_SendToRemote.field.workingSettings.laserCurrentHS = PMD2_ReceiveFromRemote.field.workingSettings.laserCurrentHS;		
				PMD2_SendToRemote.field.flag.centerFlag=OFF;PMD2_ReceiveFromRemote.field.flag.centerFlag=OFF;PMD2_ReceiveFromRemote.field.flag.centerFlag=OFF;
				if(PMD2_SendToRemote.field.measurementReading.sensitivityLevel==HIGH){
					set_ad5693_laser_bias_current(PMD2_SendToRemote.field.workingSettings.laserCurrentHS);
				}
			}else{
				PMD2_ReceiveFromRemote.field.workingSettings.laserCurrentHS = PMD2_PreviousReceiveFromRemote.field.workingSettings.laserCurrentHS;		
			}
		}
		if (PMD2_ReceiveFromRemote.field.workingSettings.laserCurrentLS != PMD2_PreviousReceiveFromRemote.field.workingSettings.laserCurrentLS ) {
			if (PMD2_ReceiveFromRemote.field.workingSettings.laserCurrentLS>0 && PMD2_ReceiveFromRemote.field.workingSettings.laserCurrentLS<10){
				PMD2_PreviousReceiveFromRemote.field.workingSettings.laserCurrentLS = PMD2_ReceiveFromRemote.field.workingSettings.laserCurrentLS;
				PMD2_SendToRemote.field.workingSettings.laserCurrentLS = PMD2_ReceiveFromRemote.field.workingSettings.laserCurrentLS;		
				PMD2_SendToRemote.field.flag.centerFlag=OFF;PMD2_ReceiveFromRemote.field.flag.centerFlag=OFF;PMD2_ReceiveFromRemote.field.flag.centerFlag=OFF;
				if(PMD2_SendToRemote.field.measurementReading.sensitivityLevel==LOW){
					set_ad5693_laser_bias_current(PMD2_SendToRemote.field.workingSettings.laserCurrentLS);
				}
			}else{
				PMD2_ReceiveFromRemote.field.workingSettings.laserCurrentLS = PMD2_PreviousReceiveFromRemote.field.workingSettings.laserCurrentLS;		
			}
		}
		if (PMD2_ReceiveFromRemote.field.workingSettings.laserTemperature != PMD2_PreviousReceiveFromRemote.field.workingSettings.laserTemperature ) {
			if (PMD2_ReceiveFromRemote.field.workingSettings.laserTemperature>5 && PMD2_ReceiveFromRemote.field.workingSettings.laserTemperature<35){
				PMD2_PreviousReceiveFromRemote.field.workingSettings.laserTemperature = PMD2_ReceiveFromRemote.field.workingSettings.laserTemperature;
				PMD2_SendToRemote.field.workingSettings.laserTemperature = PMD2_ReceiveFromRemote.field.workingSettings.laserTemperature;		
				PMD2_SendToRemote.field.flag.centerFlag=OFF;PMD2_ReceiveFromRemote.field.flag.centerFlag=OFF;PMD2_ReceiveFromRemote.field.flag.centerFlag=OFF;
				set_ad5693_laser_temperature(PMD2_SendToRemote.field.workingSettings.laserTemperature);
			}else{
				PMD2_ReceiveFromRemote.field.workingSettings.laserTemperature = PMD2_PreviousReceiveFromRemote.field.workingSettings.laserTemperature;		
			}
		}	
		if (PMD2_ReceiveFromRemote.field.workingSettings.laserRampAmplitudeHS != PMD2_PreviousReceiveFromRemote.field.workingSettings.laserRampAmplitudeHS ) {
			if (PMD2_ReceiveFromRemote.field.workingSettings.laserRampAmplitudeHS>0 && PMD2_ReceiveFromRemote.field.workingSettings.laserRampAmplitudeHS<10){
				PMD2_PreviousReceiveFromRemote.field.workingSettings.laserRampAmplitudeHS = PMD2_ReceiveFromRemote.field.workingSettings.laserRampAmplitudeHS;
				PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS = PMD2_ReceiveFromRemote.field.workingSettings.laserRampAmplitudeHS;		
				PMD2_SendToRemote.field.flag.centerFlag=OFF;PMD2_ReceiveFromRemote.field.flag.centerFlag=OFF;PMD2_ReceiveFromRemote.field.flag.centerFlag=OFF;
				if(PMD2_SendToRemote.field.measurementReading.sensitivityLevel==HIGH)
					set_laser_scan_amplitude(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeHS);
			}else{
				PMD2_ReceiveFromRemote.field.workingSettings.laserRampAmplitudeHS = PMD2_PreviousReceiveFromRemote.field.workingSettings.laserRampAmplitudeHS;		
			}
		}
		if (PMD2_ReceiveFromRemote.field.workingSettings.laserRampAmplitudeLS != PMD2_PreviousReceiveFromRemote.field.workingSettings.laserRampAmplitudeLS ) {
			if (PMD2_ReceiveFromRemote.field.workingSettings.laserRampAmplitudeLS>0 && PMD2_ReceiveFromRemote.field.workingSettings.laserRampAmplitudeLS<10){
				PMD2_PreviousReceiveFromRemote.field.workingSettings.laserRampAmplitudeLS = PMD2_ReceiveFromRemote.field.workingSettings.laserRampAmplitudeLS;
				PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS = PMD2_ReceiveFromRemote.field.workingSettings.laserRampAmplitudeLS;		
				PMD2_SendToRemote.field.flag.centerFlag=OFF;PMD2_ReceiveFromRemote.field.flag.centerFlag=OFF;PMD2_ReceiveFromRemote.field.flag.centerFlag=OFF;
				if(PMD2_SendToRemote.field.measurementReading.sensitivityLevel==LOW)
					set_laser_scan_amplitude(PMD2_SendToRemote.field.workingSettings.laserRampAmplitudeLS);
			}else{
				PMD2_ReceiveFromRemote.field.workingSettings.laserRampAmplitudeLS = PMD2_PreviousReceiveFromRemote.field.workingSettings.laserRampAmplitudeLS;		
			}
		}	
		if (PMD2_ReceiveFromRemote.field.workingSettings.testCellConcentration != PMD2_PreviousReceiveFromRemote.field.workingSettings.testCellConcentration ) {
			if (PMD2_ReceiveFromRemote.field.workingSettings.testCellConcentration>0 && PMD2_ReceiveFromRemote.field.workingSettings.testCellConcentration<50){
				PMD2_PreviousReceiveFromRemote.field.workingSettings.testCellConcentration = PMD2_ReceiveFromRemote.field.workingSettings.testCellConcentration;
				PMD2_SendToRemote.field.workingSettings.testCellConcentration = PMD2_ReceiveFromRemote.field.workingSettings.testCellConcentration;		
			}else{
				PMD2_ReceiveFromRemote.field.workingSettings.testCellConcentration = PMD2_PreviousReceiveFromRemote.field.workingSettings.testCellConcentration;	
			}					
		}	
		if (PMD2_ReceiveFromRemote.field.workingSettings.laserRampFrequency != PMD2_PreviousReceiveFromRemote.field.workingSettings.laserRampFrequency ) {
			if (PMD2_ReceiveFromRemote.field.workingSettings.laserRampFrequency>50 && PMD2_ReceiveFromRemote.field.workingSettings.laserRampFrequency<500){
				PMD2_PreviousReceiveFromRemote.field.workingSettings.laserRampFrequency = PMD2_ReceiveFromRemote.field.workingSettings.laserRampFrequency;
				PMD2_SendToRemote.field.workingSettings.laserRampFrequency = PMD2_ReceiveFromRemote.field.workingSettings.laserRampFrequency;	
			}else{
				PMD2_ReceiveFromRemote.field.workingSettings.laserRampFrequency = PMD2_PreviousReceiveFromRemote.field.workingSettings.laserRampFrequency;	
			}		
		}	
		if (PMD2_ReceiveFromRemote.field.workingSettings.calibHS != PMD2_PreviousReceiveFromRemote.field.workingSettings.calibHS ) {
			if (PMD2_ReceiveFromRemote.field.workingSettings.calibHS>0){
				PMD2_PreviousReceiveFromRemote.field.workingSettings.calibHS = PMD2_ReceiveFromRemote.field.workingSettings.calibHS;
				PMD2_SendToRemote.field.workingSettings.calibHS = PMD2_ReceiveFromRemote.field.workingSettings.calibHS;	
			}else{
				PMD2_ReceiveFromRemote.field.workingSettings.calibHS = PMD2_PreviousReceiveFromRemote.field.workingSettings.calibHS;	
			}		
		}	
		if (PMD2_ReceiveFromRemote.field.workingSettings.calibLS != PMD2_PreviousReceiveFromRemote.field.workingSettings.calibLS ) {
			if (PMD2_ReceiveFromRemote.field.workingSettings.calibLS>0){
				PMD2_PreviousReceiveFromRemote.field.workingSettings.calibHS = PMD2_ReceiveFromRemote.field.workingSettings.calibLS;
				PMD2_SendToRemote.field.workingSettings.calibLS = PMD2_ReceiveFromRemote.field.workingSettings.calibLS;
				uint8_t fase;
				fase=(uint8_t)PMD2_SendToRemote.field.workingSettings.calibLS;
				set_phase_f(fase);				
			}else{
				PMD2_ReceiveFromRemote.field.workingSettings.calibLS = PMD2_PreviousReceiveFromRemote.field.workingSettings.calibLS;	
			}		
		}				
			
		 //********************* MEASUREMENTS SETTINGS ***************************//

		if (PMD2_ReceiveFromRemote.field.measurementSettings.nAvg != PMD2_PreviousReceiveFromRemote.field.measurementSettings.nAvg ) {
			if (PMD2_ReceiveFromRemote.field.measurementSettings.nAvg>0 && PMD2_ReceiveFromRemote.field.measurementSettings.nAvg<1001){
				PMD2_PreviousReceiveFromRemote.field.measurementSettings.nAvg = PMD2_ReceiveFromRemote.field.measurementSettings.nAvg;
				PMD2_SendToRemote.field.measurementSettings.nAvg = PMD2_ReceiveFromRemote.field.measurementSettings.nAvg;	
			}else{
				PMD2_ReceiveFromRemote.field.measurementSettings.nAvg = PMD2_PreviousReceiveFromRemote.field.measurementSettings.nAvg;	
			}		
		}	

		if (PMD2_ReceiveFromRemote.field.measurementSettings.nSmoothingPoint != PMD2_PreviousReceiveFromRemote.field.measurementSettings.nSmoothingPoint ) {
			if (PMD2_ReceiveFromRemote.field.measurementSettings.nSmoothingPoint<30){
				PMD2_PreviousReceiveFromRemote.field.measurementSettings.nSmoothingPoint = PMD2_ReceiveFromRemote.field.measurementSettings.nSmoothingPoint;
				PMD2_SendToRemote.field.measurementSettings.nSmoothingPoint = PMD2_ReceiveFromRemote.field.measurementSettings.nSmoothingPoint;	
			}else{
				PMD2_ReceiveFromRemote.field.measurementSettings.nSmoothingPoint = PMD2_PreviousReceiveFromRemote.field.measurementSettings.nSmoothingPoint;	
			}		
		}	
		if (PMD2_ReceiveFromRemote.field.measurementSettings.startPoint != PMD2_PreviousReceiveFromRemote.field.measurementSettings.startPoint ) {
			if (PMD2_ReceiveFromRemote.field.measurementSettings.startPoint<PMD2_ReceiveFromRemote.field.measurementSettings.nAcqPoint/4-5){
				PMD2_PreviousReceiveFromRemote.field.measurementSettings.startPoint = PMD2_ReceiveFromRemote.field.measurementSettings.startPoint;
				PMD2_SendToRemote.field.measurementSettings.startPoint = PMD2_ReceiveFromRemote.field.measurementSettings.startPoint;	
			}else{
				PMD2_ReceiveFromRemote.field.measurementSettings.startPoint = PMD2_PreviousReceiveFromRemote.field.measurementSettings.startPoint;	
			}		
		}	
		if (PMD2_ReceiveFromRemote.field.measurementSettings.endPoint != PMD2_PreviousReceiveFromRemote.field.measurementSettings.endPoint ) {
			if (PMD2_ReceiveFromRemote.field.measurementSettings.endPoint>PMD2_ReceiveFromRemote.field.measurementSettings.nAcqPoint/4+5){
				PMD2_PreviousReceiveFromRemote.field.measurementSettings.endPoint = PMD2_ReceiveFromRemote.field.measurementSettings.endPoint;
				PMD2_SendToRemote.field.measurementSettings.endPoint = PMD2_ReceiveFromRemote.field.measurementSettings.endPoint;	
			}else{
				PMD2_ReceiveFromRemote.field.measurementSettings.endPoint = PMD2_PreviousReceiveFromRemote.field.measurementSettings.endPoint;	
			}		
		}	
		if (PMD2_ReceiveFromRemote.field.measurementSettings.deltaX != PMD2_PreviousReceiveFromRemote.field.measurementSettings.deltaX ) {
			if (PMD2_ReceiveFromRemote.field.measurementSettings.deltaX>PMD2_ReceiveFromRemote.field.measurementSettings.nAcqPoint/13 &&
					PMD2_ReceiveFromRemote.field.measurementSettings.deltaX<PMD2_ReceiveFromRemote.field.measurementSettings.nAcqPoint/7){
				PMD2_PreviousReceiveFromRemote.field.measurementSettings.deltaX = PMD2_ReceiveFromRemote.field.measurementSettings.deltaX;
				PMD2_SendToRemote.field.measurementSettings.deltaX = PMD2_ReceiveFromRemote.field.measurementSettings.deltaX;	
			}else{
				PMD2_ReceiveFromRemote.field.measurementSettings.deltaX = PMD2_PreviousReceiveFromRemote.field.measurementSettings.deltaX;	
			}		
		}
		if (PMD2_ReceiveFromRemote.field.measurementSettings.PHDGain != PMD2_PreviousReceiveFromRemote.field.measurementSettings.PHDGain ) {
			if (PMD2_SendToRemote.field.flag.autoBackground==OFF){
				PMD2_PreviousReceiveFromRemote.field.measurementSettings.PHDGain = PMD2_ReceiveFromRemote.field.measurementSettings.PHDGain;
				PMD2_SendToRemote.field.measurementSettings.PHDGain = PMD2_ReceiveFromRemote.field.measurementSettings.PHDGain;	
				if (BOARD_VERSION==4){
					AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_A, PMD2_SendToRemote.field.measurementSettings.PHDGain, 0);
					AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_B, PMD2_SendToRemote.field.measurementSettings.PHDGain, 0);					
				}
			}else{
				PMD2_ReceiveFromRemote.field.measurementSettings.PHDGain = PMD2_PreviousReceiveFromRemote.field.measurementSettings.PHDGain;	
			}		
		}	
		if (PMD2_ReceiveFromRemote.field.measurementSettings.PHDBackground != PMD2_PreviousReceiveFromRemote.field.measurementSettings.PHDBackground ) {
			if (PMD2_SendToRemote.field.flag.autoBackground==OFF){
				PMD2_PreviousReceiveFromRemote.field.measurementSettings.PHDBackground = PMD2_ReceiveFromRemote.field.measurementSettings.PHDBackground;
				PMD2_SendToRemote.field.measurementSettings.PHDBackground = PMD2_ReceiveFromRemote.field.measurementSettings.PHDBackground;					
				set_ad5693_DC_level(PMD2_SendToRemote.field.measurementSettings.PHDBackground);
			}else{
				PMD2_ReceiveFromRemote.field.measurementSettings.PHDBackground = PMD2_PreviousReceiveFromRemote.field.measurementSettings.PHDBackground;	
			}		
		}	
		if (PMD2_ReceiveFromRemote.field.measurementSettings.nAcqPoint!= PMD2_PreviousReceiveFromRemote.field.measurementSettings.nAcqPoint ) {
			if (PMD2_SendToRemote.field.flag.sensorMode==SET){
				PMD2_PreviousReceiveFromRemote.field.measurementSettings.nAcqPoint = PMD2_ReceiveFromRemote.field.measurementSettings.nAcqPoint;
				PMD2_SendToRemote.field.measurementSettings.nAcqPoint = PMD2_ReceiveFromRemote.field.measurementSettings.nAcqPoint;					
			}else{
				PMD2_ReceiveFromRemote.field.measurementSettings.nAcqPoint = PMD2_PreviousReceiveFromRemote.field.measurementSettings.nAcqPoint;	
			}		
		}	
		if (PMD2_ReceiveFromRemote.field.measurementSettings.waitBefore != PMD2_PreviousReceiveFromRemote.field.measurementSettings.waitBefore ) {
			if (PMD2_ReceiveFromRemote.field.measurementSettings.waitBefore>0){
				PMD2_PreviousReceiveFromRemote.field.measurementSettings.waitBefore = PMD2_ReceiveFromRemote.field.measurementSettings.waitBefore;
				PMD2_SendToRemote.field.measurementSettings.waitBefore = PMD2_ReceiveFromRemote.field.measurementSettings.waitBefore;	
			}else{
				PMD2_ReceiveFromRemote.field.measurementSettings.waitBefore = PMD2_PreviousReceiveFromRemote.field.measurementSettings.waitBefore;	
			}		
		}		
		if (PMD2_ReceiveFromRemote.field.measurementSettings.waitInTheMiddle != PMD2_PreviousReceiveFromRemote.field.measurementSettings.waitInTheMiddle ) {
			if (PMD2_ReceiveFromRemote.field.measurementSettings.waitInTheMiddle>0){
				PMD2_PreviousReceiveFromRemote.field.measurementSettings.waitInTheMiddle = PMD2_ReceiveFromRemote.field.measurementSettings.waitInTheMiddle;
				PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle = PMD2_ReceiveFromRemote.field.measurementSettings.waitInTheMiddle;	
			}else{
				PMD2_ReceiveFromRemote.field.measurementSettings.waitInTheMiddle = PMD2_PreviousReceiveFromRemote.field.measurementSettings.waitInTheMiddle;	
			}		
		}				

		//********************* FLAG    ***************************//
		

		if (PMD2_ReceiveFromRemote.field.flag.laserStatus != PMD2_PreviousReceiveFromRemote.field.flag.laserStatus ) {
			PMD2_PreviousReceiveFromRemote.field.flag.laserStatus = PMD2_ReceiveFromRemote.field.flag.laserStatus;
			PMD2_SendToRemote.field.flag.laserStatus = PMD2_ReceiveFromRemote.field.flag.laserStatus;	
			if (PMD2_ReceiveFromRemote.field.flag.laserStatus==ON )
				VLD_ON();
			else
				VLD_OFF();
		}	
		if (PMD2_ReceiveFromRemote.field.flag.tecStatus != PMD2_PreviousReceiveFromRemote.field.flag.tecStatus ) {
			PMD2_PreviousReceiveFromRemote.field.flag.tecStatus = PMD2_ReceiveFromRemote.field.flag.tecStatus;
			PMD2_SendToRemote.field.flag.tecStatus = PMD2_ReceiveFromRemote.field.flag.tecStatus;	
			if (PMD2_ReceiveFromRemote.field.flag.tecStatus ==ON )
				TC_ON();
			else
				TC_OFF();
		}	
		if (PMD2_ReceiveFromRemote.field.flag.centerFlag != PMD2_PreviousReceiveFromRemote.field.flag.centerFlag ) {
			PMD2_PreviousReceiveFromRemote.field.flag.centerFlag = PMD2_ReceiveFromRemote.field.flag.centerFlag;
			PMD2_SendToRemote.field.flag.centerFlag = PMD2_ReceiveFromRemote.field.flag.centerFlag;	
		}	
		if (PMD2_ReceiveFromRemote.field.flag.switchRange != PMD2_PreviousReceiveFromRemote.field.flag.switchRange ) {
			PMD2_PreviousReceiveFromRemote.field.flag.switchRange = PMD2_ReceiveFromRemote.field.flag.switchRange;
			PMD2_SendToRemote.field.flag.switchRange = PMD2_ReceiveFromRemote.field.flag.switchRange;	
		}	
		if (PMD2_ReceiveFromRemote.field.flag.autoRange != PMD2_PreviousReceiveFromRemote.field.flag.autoRange ) {
			PMD2_PreviousReceiveFromRemote.field.flag.autoRange = PMD2_ReceiveFromRemote.field.flag.autoRange;
			PMD2_SendToRemote.field.flag.autoRange = PMD2_ReceiveFromRemote.field.flag.autoRange;	
		}	
		if (PMD2_ReceiveFromRemote.field.flag.autoBackground != PMD2_PreviousReceiveFromRemote.field.flag.autoBackground ) {
			PMD2_PreviousReceiveFromRemote.field.flag.autoBackground = PMD2_ReceiveFromRemote.field.flag.autoBackground;
			PMD2_SendToRemote.field.flag.autoBackground = PMD2_ReceiveFromRemote.field.flag.autoBackground;	
		}	
		if (PMD2_ReceiveFromRemote.field.flag.print != PMD2_PreviousReceiveFromRemote.field.flag.print ) {
			PMD2_PreviousReceiveFromRemote.field.flag.print = PMD2_ReceiveFromRemote.field.flag.print;
			PMD2_SendToRemote.field.flag.print = PMD2_ReceiveFromRemote.field.flag.print;	
		}	
		if (PMD2_ReceiveFromRemote.field.flag.sensorMode != PMD2_PreviousReceiveFromRemote.field.flag.sensorMode ) {
			PMD2_PreviousReceiveFromRemote.field.flag.sensorMode = PMD2_ReceiveFromRemote.field.flag.sensorMode;
			PMD2_SendToRemote.field.flag.sensorMode = PMD2_ReceiveFromRemote.field.flag.sensorMode;	
		}	
		if (PMD2_ReceiveFromRemote.field.flag.e2proomSave != PMD2_PreviousReceiveFromRemote.field.flag.e2proomSave ) {
			PMD2_PreviousReceiveFromRemote.field.flag.e2proomSave = OFF; 
			PMD2_ReceiveFromRemote.field.flag.e2proomSave=OFF;
			PMD2_SendToRemote.field.flag.e2proomSave = OFF ;	
			write_struct_2_eeprom();
		}	
		if (PMD2_ReceiveFromRemote.field.flag.e2proomLoad != PMD2_PreviousReceiveFromRemote.field.flag.e2proomLoad ) {
			PMD2_PreviousReceiveFromRemote.field.flag.e2proomLoad = OFF;
			PMD2_ReceiveFromRemote.field.flag.e2proomLoad = OFF;
			PMD2_SendToRemote.field.flag.e2proomLoad = OFF;;	
			load_struct_from_eeprom();
		}	
		if (PMD2_ReceiveFromRemote.field.flag.firmwareUpdate != PMD2_PreviousReceiveFromRemote.field.flag.firmwareUpdate ) {
			PMD2_PreviousReceiveFromRemote.field.flag.firmwareUpdate = PMD2_ReceiveFromRemote.field.flag.firmwareUpdate;
			PMD2_SendToRemote.field.flag.firmwareUpdate = PMD2_ReceiveFromRemote.field.flag.firmwareUpdate;	
		}
		if (PMD2_ReceiveFromRemote.field.flag.uartBridge != PMD2_PreviousReceiveFromRemote.field.flag.uartBridge ) {
			PMD2_PreviousReceiveFromRemote.field.flag.uartBridge = PMD2_ReceiveFromRemote.field.flag.uartBridge;
			PMD2_SendToRemote.field.flag.uartBridge = PMD2_ReceiveFromRemote.field.flag.uartBridge;	
		}	
			//Reset CPU
//		if ( ( ODR_ReceiveFromRemote.field.FLAG.Reset_Restart  != ODR_PreviousReceiveFromRemote.field.FLAG.Reset_Restart ) ) {
//				HAL_NVIC_SystemReset();
//		}	
	}
}


