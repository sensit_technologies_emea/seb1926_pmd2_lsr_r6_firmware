/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "i2c_digitalpot_AD5243.h"

/* Private variables ---------------------------------------------------------*/
extern I2C_HandleTypeDef hi2c2;

HAL_StatusTypeDef AD5243_is_ready(I2C_HandleTypeDef *phi2c, uint16_t DevAddress)
{
    return HAL_I2C_IsDeviceReady(phi2c, DevAddress, TRIAL_COUNT, COMM_TIMEOUT);
}


/**
  * @brief  The application entry point.
  *
  * @retval None
  */
HAL_StatusTypeDef AD5243_set_potentiometer_value(I2C_HandleTypeDef *phi2c, uint16_t DevAddress, AD5243_SelectWiper wiper, uint16_t value, uint16_t shutDown)
{
  uint8_t buffer[2];
  HAL_StatusTypeDef tmpRetVal;

  if ( (tmpRetVal=AD5243_is_ready(phi2c, DevAddress)) == HAL_OK ) {

    buffer[0]= (shutDown !=0)?0x40:0x00;
    buffer[0]= (wiper !=0)?0x80:0x00;		
    buffer[1]=(value) & 0xFF;        //MSB byte   
    tmpRetVal=HAL_I2C_Master_Transmit(phi2c, DevAddress,buffer,2,100);  

 }
  
  return tmpRetVal;
}


HAL_StatusTypeDef ad5693_get_potentiometer(I2C_HandleTypeDef *phi2c, uint16_t DevAddress, AD5243_SelectWiper wiper, uint8_t *buf_read_back, uint16_t shutDown)
{
  uint8_t buffer[2];
  HAL_StatusTypeDef tmpRetVal;

  if ( (tmpRetVal=AD5243_is_ready(phi2c, DevAddress)) == HAL_OK ) {
    buffer[0]= (shutDown !=0)?0x40:0x00;
    buffer[0]= (wiper !=AD5243_WIPER_A)?0x80:0x00;		
    tmpRetVal=HAL_I2C_Master_Transmit(phi2c, DevAddress,buffer,1,100);  

    HAL_I2C_Master_Receive(phi2c, DevAddress, buf_read_back, 1, 100);
 }
  
  return tmpRetVal;
}



