#ifndef OUT_H
#define OUT_H

void LED1_ON(void);
void LED1_OFF(void);
void LED2_ON(void);
void LED2_OFF(void);
void LED3_ON(void);
void LED3_OFF(void);
void LED4_ON(void);
void LED4_OFF(void);
void LED5_ON(void);
void LED5_OFF(void);

void VLD_ON(void);
void VLD_OFF(void);

void V5V_ON(void);
void V5V_OFF(void);



void TC_ON(void);
void TC_OFF(void);

void WDI_TOGGLE(void);

//#define E2PROM_WP(x) 	        (x?(I2C_WP_GPIO_Port->BSRR=I2C_WP_Pin):(I2C_WP_GPIO_Port->BSRR=(uint32_t)I2C_WP_Pin<< 16U));
#define EEP_WRITE_DISABLE       (I2C_WP_GPIO_Port->BSRR=I2C_WP_Pin)
#define EEP_WRITE_ENABLE        (I2C_WP_GPIO_Port->BSRR=(uint32_t)I2C_WP_Pin << 16U)


#define DFLASH_DISABLE          (SPI2_NSS_GPIO_Port->BSRR=SPI2_NSS_Pin)
#define DFLASH_ENABLE           (SPI2_NSS_GPIO_Port->BSRR=(uint32_t)SPI2_NSS_Pin << 16U)

#define LASER_STATE_OUT(x) 	(x?(LASER_SYNC_OUT_GPIO_Port->BSRR=LASER_SYNC_OUT_Pin):(LASER_SYNC_OUT_GPIO_Port->BSRR= (uint32_t)LASER_SYNC_OUT_Pin<< 16U));

#define CS2n_Com(x) 	        (x?(ADDR_DATA_FLASH_GPIO_Port->BSRR=ADDR_DATA_FLASH_Pin):(ADDR_DATA_FLASH_GPIO_Port->BSRR=(uint32_t)ADDR_DATA_FLASH_Pin<< 16U));
#define RSTn_RFU(x) 	        (x?(RESET_DATA_FLASH_GPIO_Port->BSRR=RESET_DATA_FLASH_Pin):(RESET_DATA_FLASH_GPIO_Port->BSRR=(uint32_t)RESET_DATA_FLASH_Pin << 16U));
#define WPn_IO2(x) 	        (x?(WP_DATA_FLASH_GPIO_Port->BSRR=WP_DATA_FLASH_Pin):(WP_DATA_FLASH_GPIO_Port->BSRR=(uint32_t)WP_DATA_FLASH_Pin<< 16U));
#define HOLDn_IO3(x) 	        (x?(HOLD_DATA_FLASH_GPIO_Port->BSRR=HOLD_DATA_FLASH_Pin):(HOLD_DATA_FLASH_GPIO_Port->BSRR=(uint32_t)HOLD_DATA_FLASH_Pin<< 16U));
//#define WP(x) 		        (x?(AT45D_WP_GPIO_Port->BSRR=AT45D_WP_Pin):(AT45D_WP_GPIO_Port->BRR=AT45D_WP_Pin));


#endif
