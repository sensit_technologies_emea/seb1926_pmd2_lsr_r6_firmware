#ifndef EEPROM_ADDRESS_H
#define EEPROM_ADDRESS_H

#define     EEP_ADDR_EEP_TEST           		0x0004        // 4 byte 

#define     EEP_LANGUAGE                		0x0008        // 1 byte 
//#define     EEP_SERIAL_NUMBER           		0x0009        // 4 byte 
//#define     EEP_MODBUS_ID           				0x000D        // 1 byte 

//////////////////////////////////////////////////////////////////////////
//        TYPE LASER EEP_LASER_TEMPERATURE
//////////////////////////////////////////////////////////////////////////

#define     EEP_SERIAL_NUMBER           		0x0010        											// 4 byte 
#define     EEP_CAL_YEAR                		EEP_SERIAL_NUMBER + 4            		// 2 byte 		uint16_t		calibration year;				
#define     EEP_CAL_MONTH               		EEP_CAL_YEAR + 2                 		// 2 byte 		uint16_t		calibration month;				
#define     EEP_CAL_DAY                 		EEP_CAL_MONTH + 2	            			// 2 byte 		uint16_t		calibration day;				
#define     EEP_CAL_HOUR                		EEP_CAL_DAY + 2    									// 2 byte     uint16_t		calibration hour;				
#define     EEP_CAL_MINUTE              		EEP_CAL_HOUR + 2                		// 2 byte 		uint16_t		calibration minute;			
#define     EEP_MODBUS_ID           				EEP_CAL_MINUTE+2										// 2 byte			uint16_t		calibration MODBUS_ID

#define     EEP_LASER_ID                 		EEP_MODBUS_ID + 2										// 4 byte     uint32_t    id;             
#define     EEP_LASER_TEMPERATURE       		EEP_LASER_ID + 4    								// 4 byte     float		    laser temperature;		 
#define     EEP_LASER_CURRENT_HS						EEP_LASER_TEMPERATURE + 4      			// 4 byte     float		    laser currenthigh sensitivity;		 
#define     EEP_LASER_RAMP_AMPLITUDE_HS   	EEP_LASER_CURRENT_HS + 4     				// 4 byte     float		    laser ramp amplitude high sensitivity;		 
#define     EEP_LASER_RAMP_FREQUENCY    		EEP_LASER_RAMP_AMPLITUDE_HS + 4 		// 4 byte     float		    laser rampa frequency;		 
#define     EEP_LASER_CURRENT_LS					 	EEP_LASER_RAMP_FREQUENCY + 4     		// 4 byte     float		    laser current low sensitivity;		 
#define     EEP_LASER_RAMP_AMPLITUDE_LS   	EEP_LASER_CURRENT_LS + 4     		  	// 4 byte     float		    laser ramp amplitude low sensitivity;		 
#define     EEP_TEST_CELL_CONCENTRATION			EEP_LASER_RAMP_AMPLITUDE_LS + 4			// 4 byte     float				Test cell concentration in ppm;
#define     EEP_CALIB_HIGH						   		EEP_TEST_CELL_CONCENTRATION + 4	  	// 4 byte     float				calibration factor high sensitivity;		
#define     EEP_CALIB_LOW							   		EEP_CALIB_HIGH + 4	  							// 4 byte     float				calibration factor low sensitivity;

#define     EEP_N_ACQUISITION					   		EEP_CALIB_LOW + 4		 								// 2 byte     uint16_t		number of acquisitions for the average;
#define     EEP_N_ACQ_POINT		   						EEP_N_ACQUISITION + 2								// 2 byte     uint16_t		number of acquisition points for a single ramp;
#define     EEP_N_SMOOTH_POINT   						EEP_N_ACQ_POINT + 2	 								// 2 byte     uint16_t		number of smooth points for the analysis;
#define     EEP_DELTA_X								   		EEP_N_SMOOTH_POINT + 2							// 2 byte     uint16_t		about half width of the absorption line in points;
#define     EEP_START_P								   		EEP_DELTA_X + 2											// 2 byte     uint16_t		start point to search the peak of absorption;
#define     EEP_END_P									   		EEP_START_P + 2											// 2 byte     uint16_t		end point to search the peak of absorption;
#define     EEP_WAIT_BEFORE						   		EEP_DELTA_X + 2											// 2 byte     uint16_t		delay in usec before to start the acquisition of the ramp;
#define     EEP_WAIT_INTHEMIDDLE			   		EEP_WAIT_BEFORE + 2									// 2 byte     uint16_t		delay in usec between  the acquisition of the two parts of the ramp;


#define     OLD_EEP_SERIAL_NUMBER           		0x0009        													// 4 byte 
#define     OLD_EEP_ID                      		0x0010           		     								// 4 byte     uint32_t    id;             
#define     OLD_EEP_LASER_ID                    OLD_EEP_ID +4        										// 4 byte     uint32_t    laser_id;             
#define     OLD_EEP_LASER_TEMPERATURE       		OLD_EEP_LASER_ID +4    									// 4 byte     float		    laser temperature;		 
#define     OLD_EEP_LASER_CURRENT_HS						OLD_EEP_LASER_TEMPERATURE +4      			// 4 byte     float		    laser currenthigh sensitivity;		 
#define     OLD_EEP_LASER_RAMP_AMPLITUDE_HS   	OLD_EEP_LASER_CURRENT_HS +4     				// 4 byte     float		    laser ramp amplitude high sensitivity;		 
#define     OLD_EEP_LASER_RAMP_FREQUENCY    		OLD_EEP_LASER_RAMP_AMPLITUDE_HS +4 			// 4 byte     float		    laser rampa frequency;		 
#define     OLD_EEP_LASER_CURRENT_LS					 	OLD_EEP_LASER_RAMP_FREQUENCY +4     		// 4 byte     float		    laser current low sensitivity;		 
#define     OLD_EEP_LASER_RAMP_AMPLITUDE_LS   	OLD_EEP_LASER_CURRENT_LS +4     		  	// 4 byte     float		    laser ramp amplitude low sensitivity;		 
#define     OLD_EEP_CALIB_HIGH						   		OLD_EEP_LASER_RAMP_AMPLITUDE_LS+4		  	// 4 byte     float				calibration factor high sensitivity;		
#define     OLD_EEP_CALIB_LOW							   		OLD_EEP_CALIB_HIGH+4		  							// 4 byte     float				calibration factor low sensitivity;
#define     OLD_EEP_NUMBER_OF_ACQUISITION   		OLD_EEP_CALIB_LOW+4		  								// 2 byte     uint16_t		number of acquisition for the average;
#define     OLD_EEP_ACQUISITION_DELAY			   		OLD_EEP_NUMBER_OF_ACQUISITION+2					// 2 byte     uint16_t		delay in usec before the acquisition of the ramp;
#define     OLD_EEP_TEST_CELL_CONCENTRATION			OLD_EEP_ACQUISITION_DELAY+2							// 4 byte     float				Test cell concentration in ppm;
#define     OLD_EEP_CAL_HOUR                		OLD_EEP_TEST_CELL_CONCENTRATION + 2    	// 2 byte       uint16_t	hour;				
#define     OLD_EEP_CAL_MINUTE              		OLD_EEP_CAL_HOUR + 2                		// 2 byte 	uint16_t	minute;			
#define     OLD_EEP_CAL_DAY                 		OLD_EEP_CAL_MINUTE + 2             			// 2 byte 	uint16_t	day;				
#define     OLD_EEP_CAL_MONTH               		OLD_EEP_CAL_DAY + 2                 		// 2 byte 	uint16_t	month;				
#define     OLD_EEP_CAL_YEAR                		OLD_EEP_CAL_MONTH + 2               		// 2 byte 	uint16_t	year;				

#define			EEP_CALIBRATION_GAS1_RANGE1			EEP_WAIT_INTHEMIDDLE+2							// 4 byte			float		fisrt calibration gas for range 1
#define			EEP_CALIBRATION_GAS2_RANGE1			EEP_CALIBRATION_GAS1_RANGE1+4				// 4 byte			float		second calibration gas for range 1
#define			EEP_CALIBRATION_GAS3_RANGE1			EEP_CALIBRATION_GAS2_RANGE1+4				// 4 byte			float		third calibration gas for range 1
#define			EEP_CALIBRATION_GAS4_RANGE1			EEP_CALIBRATION_GAS3_RANGE1+4				// 4 byte			float		fourth calibration gas for range 1
#define			EEP_CALIBRATION_GAS5_RANGE1			EEP_CALIBRATION_GAS4_RANGE1+4				// 4 byte			float		fifth calibration gas for range 1

#define			EEP_RAW_VALUE1_RANGE1						EEP_CALIBRATION_GAS5_RANGE1+4				// 4 byte			float		first raw value memasured for range 1
#define			EEP_RAW_VALUE2_RANGE1						EEP_RAW_VALUE1_RANGE1+4							// 4 byte			float		second raw value memasured for range 1
#define			EEP_RAW_VALUE3_RANGE1						EEP_RAW_VALUE2_RANGE1+4							// 4 byte			float		third raw value memasured for range 1
#define			EEP_RAW_VALUE4_RANGE1						EEP_RAW_VALUE3_RANGE1+4							// 4 byte			float		fourth raw value memasured for range 1
#define			EEP_RAW_VALUE5_RANGE1						EEP_RAW_VALUE4_RANGE1+4							// 4 byte			float		fifth raw value memasured for range 1

#define			EEP_CALIBRATION_GAS1_RANGE2			EEP_RAW_VALUE5_RANGE1+4							// 4 byte			float		fisrt calibration gas for range 2
#define			EEP_CALIBRATION_GAS2_RANGE2			EEP_CALIBRATION_GAS1_RANGE2+4				// 4 byte			float		second calibration gas for range 2
#define			EEP_CALIBRATION_GAS3_RANGE2			EEP_CALIBRATION_GAS2_RANGE2+4				// 4 byte			float		third calibration gas for range 2
#define			EEP_CALIBRATION_GAS4_RANGE2			EEP_CALIBRATION_GAS3_RANGE2+4				// 4 byte			float		fourth calibration gas for range 2
#define			EEP_CALIBRATION_GAS5_RANGE2			EEP_CALIBRATION_GAS4_RANGE2+4				// 4 byte			float		fifth calibration gas for range 2

#define			EEP_RAW_VALUE1_RANGE2						EEP_CALIBRATION_GAS5_RANGE2+4				// 4 byte			float		first raw value memasured for range 2
#define			EEP_RAW_VALUE2_RANGE2						EEP_RAW_VALUE1_RANGE2+4							// 4 byte			float		second raw value memasured for range 2
#define			EEP_RAW_VALUE3_RANGE2						EEP_RAW_VALUE2_RANGE2+4							// 4 byte			float		third raw value memasured for range 2
#define			EEP_RAW_VALUE4_RANGE2						EEP_RAW_VALUE3_RANGE2+4							// 4 byte			float		fourth raw value memasured for range 2
#define			EEP_RAW_VALUE5_RANGE2						EEP_RAW_VALUE4_RANGE2+4							// 4 byte			float		fifth raw value memasured for range 2

#define			EEP_CALIBRATION_GAS1_RANGE3			EEP_RAW_VALUE5_RANGE1+4							// 4 byte			float		fisrt calibration gas for range 2
#define			EEP_CALIBRATION_GAS2_RANGE3			EEP_CALIBRATION_GAS1_RANGE2+4				// 4 byte			float		second calibration gas for range 2
#define			EEP_CALIBRATION_GAS3_RANGE3			EEP_CALIBRATION_GAS2_RANGE2+4				// 4 byte			float		third calibration gas for range 2
#define			EEP_CALIBRATION_GAS4_RANGE3			EEP_CALIBRATION_GAS3_RANGE2+4				// 4 byte			float		fourth calibration gas for range 2
#define			EEP_CALIBRATION_GAS5_RANGE3			EEP_CALIBRATION_GAS4_RANGE2+4				// 4 byte			float		fifth calibration gas for range 2

#define			EEP_RAW_VALUE1_RANGE3						EEP_CALIBRATION_GAS5_RANGE3+4				// 4 byte			float		first raw value memasured for range 3
#define			EEP_RAW_VALUE2_RANGE3						EEP_RAW_VALUE1_RANGE3+4							// 4 byte			float		second raw value memasured for range 3
#define			EEP_RAW_VALUE3_RANGE3						EEP_RAW_VALUE2_RANGE3+4							// 4 byte			float		third raw value memasured for range 3
#define			EEP_RAW_VALUE4_RANGE3						EEP_RAW_VALUE3_RANGE3+4							// 4 byte			float		fourth raw value memasured for range 3
#define			EEP_RAW_VALUE5_RANGE3						EEP_RAW_VALUE4_RANGE3+4							// 4 byte			float		fifth raw value memasured for range 3


#define			EEP_CHECK_INIT1									2048
#define			EEP_CHECK_INIT2									EEP_CHECK_INIT1+4	


#endif
