#ifndef REGISTERLINK_H_
#define REGISTERLINK_H_

#include <stdint.h>

uint16_t  linkRegister_Read_03_and_4 ( uint16_t function, uint16_t registerAddress , int16_t * esito );
void  linkRegister_Write_06_16 ( uint16_t function, uint16_t registerAddress , uint16_t registerValue , int16_t * esito );

#endif /* REGISTERLINK_H_ */
