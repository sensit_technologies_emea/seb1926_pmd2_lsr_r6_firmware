
#ifndef PROTO_H
#define PROTO_H


typedef struct {

    uint16_t ST_ST_CHAR_REC:1;
    uint16_t STARTED:1;
    uint16_t ST_STD_CMD:1;
    uint16_t ST_SENDING:1;
    uint16_t ST_CS_ADDED:1;
    uint16_t ST_ST_SENT:1;

} typeProto;


typedef struct {
    unsigned char    ubSts;               /* status byte of response */
    uint16_t    ubLen;               /* length of the whole response */
} PCP_sResponse;


typedef struct {
    unsigned char    ubCmd;
    unsigned char    ubOp;
    uint16_t     ubSize;              /* size of data block in bytes */
    char         *pAddr;              /* address of data */
} PCP_sReadMem;

typedef struct {
    unsigned char    ubCmd;
    unsigned char    ubOp;
    uint16_t       ubSize;              /* size of data block in bytes */
    uint32_t       pAddr;              /* address of data */
} PCP_sReadEMem;


/* write memory */

typedef struct {
    unsigned char   ubCmd;
    unsigned char   ubOp;
    uint16_t   ubSize;              /* size of data block in bytes */
    char   *pAddr;              /* address of data */
    unsigned char   ubBuff[1];           /* data */
} PCP_sWriteMem;

typedef struct {
    unsigned char   ubCmd;
    unsigned char   ubOp;
    uint16_t    ubSize;              /* size of data block in bytes */
    uint32_t   pAddr;              /* address of data */
    unsigned char   ubBuff[1];           /* data */
} PCP_sWriteEMem;



typedef struct {
    char    sbReserved; /* status code */
    uint32_t serialNumber;
    char    ubData[1];  /* data to PC */
} PCP_sReturnData;


/*-------------------------------------*/
/* response message - status byte      */
/*-------------------------------------*/
/* confirmation codes */
#define PROTO_STC_OK            0x00   /* operation succesful */

/* error codes */
#define PROTO_STC_BUSY          0x80   /* busy */
#define PROTO_STC_INVCMD        0x81   /* invalid command */
#define PROTO_STC_CMDBUFFOVF    0x82   /* command too long */
#define PROTO_STC_ADDR          0x83   /* address error */
#define PROTO_STC_CMDSERR       0x84   /* checksum error */
#define PROTO_STC_DEVICEERR     0x85   /* checksum error */

#define PROTO_CMD_WRITEIMEM_NR  0xB3
#define PROTO_CMD_READIMEM      0xC3 /* read block of memory */
#define PROTO_CMD_WRITEIMEM     0xD3 /* write block of memory */
#define PROTO_CMD_READEMEM      0xC7 /* read block of memory */
#define PROTO_CMD_WRITEEMEM     0xD7 /* write block of memory */
#define PROTO_CMD_READDMEM      0xCB /* read block of memory */

#define START_CHAR             '*'
#define PROTO_START            1              /* constant for function prepareStringProto; signalizes start of message */
#define NO_PROTO_START         0              /* constant for function prepareStringProto; signalizes no message start */

#define PROTO_BUFFER_SIZE       1024            /* PC Master input/output buffer size */

#define     MA_OK        0
#define     MA_READY     0
#define     MA_EMPTY    -1
#define     MA_FULL     -2

#endif

