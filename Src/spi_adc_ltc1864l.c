/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "spi_adc_ltc1864l.h"
#include "vee.h"
#include "usart_pc.h"
#include "modbus.h"
#include "sensit.h"
#include "out.h"
#include "taskManager.h"
#include "i2c_digitalpot_AD5243.h"
#include "modbus_extension_register.h"

/* Private variables ---------------------------------------------------------*/
extern SPI_HandleTypeDef hspi2;
extern SPI_HandleTypeDef hspi3;
extern DAC_HandleTypeDef hdac;
extern TIM_HandleTypeDef htim6;
extern 	uint16_t waveform[1024];
void enableINT(void);
void disableINT(void);

/**
  * @brief  The application entry point.
  *
  * @retval None
  */

static void spi_usleep ( unsigned int delusecs );

#ifdef __KEIL__
#pragma  push
#pragma  O0
#endif

HAL_StatusTypeDef ltc1864l_get_value(uint16_t *adc_value)
{
  //uint16_t dummy_buffer_tx;
  HAL_StatusTypeDef retVal;

  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);
  //SPI2_NSS_GPIO_Port->BSRR=SPI2_NSS_Pin;
  spi_usleep(1);
  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_RESET);
  //SPI2_NSS_GPIO_Port->BSRR=(uint32_t)(SPI2_NSS_Pin) <<16U;
  //retVal=HAL_SPI_Receive(&hspi2, (uint8_t *)  adc_value,1 , HAL_MAX_DELAY);
	retVal=HAL_SPI_Receive(&hspi2, (uint8_t *)  adc_value,1 , HAL_MAX_DELAY);
  
  return retVal;
//  uint16_t dummy_buffer_tx;
//  HAL_StatusTypeDef retVal;
//  
//  SPI2_NSS_GPIO_Port->BSRR=(uint32_t)(SPI2_NSS_Pin) <<16U;
//  
//  retVal=HAL_SPI_TransmitReceive(&hspi2, (uint8_t *) &dummy_buffer_tx, (uint8_t *)  adc_value,1 , HAL_MAX_DELAY);
//  
//  SPI2_NSS_GPIO_Port->BSRR=SPI2_NSS_Pin;
//  
//  return retVal;
  
}
static void spi_usleep ( unsigned int delusecs )
{
    int i;
    for ( i = 0; i < 20 * delusecs; i++ );
}



#ifdef __KEIL__  
  if(interrupt_mask)
    __enable_irq();
#endif
      




#ifdef __KEIL__
#pragma  push
#pragma  O0
#endif
void read_rampa_DC(uint32_t *pvalue, uint16_t n_samples, uint16_t n_mean){

  uint32_t temp[512];
  uint8_t up, dw;
	uint16_t index_mean=0; 
	uint8_t count_out;
	uint16_t jumplevel;
	jumplevel=100;
	uint32_t background;
	uint16_t xmin, xmax;
	uint32_t ymin, ymax;


  for (uint16_t i=0; i<n_samples; i++){
    *(pvalue+i)=0;
		temp[i]=0;
  }
	//int interrupt_mask = __disable_irq();
	disableINT();
	count_out=0;
  while (index_mean<n_mean){
		
    //wait_for_trigger_counter();
    wait_for_trigger_micro();
		up=0; dw=0;

		fastLtc1864l_get_value(temp, n_samples);

		//test_PIN_ON();
		//for (uint16_t i=0; i<n_samples; i++)
		//	temp[i]-=PMD2_SendToRemote.field.measurementReading.PHDZero;
		background=PMD2_SendToRemote.field.measurementSettings.PHDGain*(50.0/256.0)*PMD2_SendToRemote.field.measurementSettings.PHDBackground;
		
		minmaxV(temp, PMD2_SendToRemote.field.measurementSettings.nAcqPoint/2, &xmin, &ymin, &xmax, &ymax);		//40 us
				
		if (ymin<1000)
				dw=1;
		if (ymax>64536)
				up=1;
		if (BOARD_VERSION==4){
			if (up==0 && (ymax-ymin)<32000 && (ymax-ymin)>100 && PMD2_SendToRemote.field.flag.autoBackground==1 && PMD2_SendToRemote.field.measurementSettings.PHDGain<255){
					PMD2_SendToRemote.field.measurementSettings.PHDGain=PMD2_SendToRemote.field.measurementSettings.PHDGain*1.5;
					if (PMD2_SendToRemote.field.measurementSettings.PHDGain>255)
						PMD2_SendToRemote.field.measurementSettings.PHDGain=255;
					AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_A, PMD2_SendToRemote.field.measurementSettings.PHDGain, 0);
					AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_B, PMD2_SendToRemote.field.measurementSettings.PHDGain, 0);
			}
			if ((ymax-ymin)>63000 && PMD2_SendToRemote.field.flag.autoBackground==1 && PMD2_SendToRemote.field.measurementSettings.PHDGain>2){
					PMD2_SendToRemote.field.measurementSettings.PHDGain=PMD2_SendToRemote.field.measurementSettings.PHDGain/1.5;
					if (PMD2_SendToRemote.field.measurementSettings.PHDGain<2)
						PMD2_SendToRemote.field.measurementSettings.PHDGain=2;
					AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_A, PMD2_SendToRemote.field.measurementSettings.PHDGain, 0);
					AD5243_set_potentiometer_value(&hi2c3, AD5243_ADDR, AD5243_WIPER_B, PMD2_SendToRemote.field.measurementSettings.PHDGain, 0);
			}
		}
  	if ((dw==0 && up==0)|| count_out>30 || PMD2_SendToRemote.field.flag.autoBackground==0){
				for (uint16_t i=0; i<n_samples; i++)
					*(pvalue+i) +=temp[i]+background;
			index_mean++;
			PMD2_SendToRemote.field.measurementReading.rampStatus=0;
		}else if (dw==1 && up==0 ){
      if (PMD2_SendToRemote.field.measurementSettings.PHDBackground<600){
				for (uint16_t i=0; i<n_samples; i++){
					*(pvalue+i) +=temp[i]+background;			
				}
				set_ad5693_DC_level(500);
				index_mean++;
			}else{
        PMD2_SendToRemote.field.measurementSettings.PHDBackground=PMD2_SendToRemote.field.measurementSettings.PHDBackground-jumplevel;
				jumplevel*=1.2;
				if (jumplevel>3000)
					jumplevel=3000;
        set_ad5693_DC_level(PMD2_SendToRemote.field.measurementSettings.PHDBackground);
				count_out++;
			}
			PMD2_SendToRemote.field.measurementReading.rampStatus=1;
			PMD2_PreviousReceiveFromRemote.field.measurementReading.rampStatus=1;
    }else if (dw==0 && up==1){
      if (PMD2_SendToRemote.field.measurementSettings.PHDBackground>64900){
				for (uint16_t i=0; i<n_samples; i++){
					*(pvalue+i) +=temp[i]+background;
				}
				set_ad5693_DC_level(65000);
				index_mean++;
			}else{
        PMD2_SendToRemote.field.measurementSettings.PHDBackground=PMD2_SendToRemote.field.measurementSettings.PHDBackground+jumplevel;
				jumplevel*=1.2;
				if (jumplevel>3000)
					jumplevel=3000;
        set_ad5693_DC_level(PMD2_SendToRemote.field.measurementSettings.PHDBackground);
				count_out++;
			}
			PMD2_SendToRemote.field.measurementReading.rampStatus=2;
    }else if(dw==1 && up==1){
			PMD2_SendToRemote.field.measurementReading.rampStatus=3;
			for (uint16_t i=0; i<n_samples; i++){
					*(pvalue+i) +=temp[i]+background;
      }
			index_mean++;
		}
		//PMD2_ReceiveFromRemote.field.measurementSettings.PHDGain=PMD2_SendToRemote.field.measurementSettings.PHDGain;
		//PMD2_PreviousReceiveFromRemote.field.measurementSettings.PHDGain=PMD2_ReceiveFromRemote.field.measurementSettings.PHDGain;
		PMD2_ReceiveFromRemote.field.measurementReading.rampStatus=PMD2_SendToRemote.field.measurementReading.rampStatus;
		PMD2_PreviousReceiveFromRemote.field.measurementReading.rampStatus=PMD2_ReceiveFromRemote.field.measurementReading.rampStatus;
		//PMD2_ReceiveFromRemote.field.measurementSettings.PHDBackground=PMD2_SendToRemote.field.measurementSettings.PHDBackground;
		//PMD2_PreviousReceiveFromRemote.field.measurementSettings.PHDBackground=PMD2_ReceiveFromRemote.field.measurementSettings.PHDBackground;	
		//test_PIN_OFF();

	}
	enableINT();

}


void read_rampa_SD(uint32_t *pvalue, uint16_t n_samples, uint16_t n_mean){

  uint32_t temp[512];
  uint8_t up, dw;
	uint16_t index_mean=0; 
	uint16_t xmin, xmax;
	uint32_t ymin, ymax;


  for (uint16_t i=0; i<n_samples; i++){
    *(pvalue+i)=0;
		temp[i]=0;
  }
	//int interrupt_mask = __disable_irq();
	disableINT();
  while (index_mean<n_mean){
		
    //wait_for_trigger_counter();
    wait_for_trigger_micro();
		up=0; dw=0;


		fastLtc1864l_get_valueSD(temp, n_samples);
		
		minmaxV(temp, PMD2_SendToRemote.field.measurementSettings.nAcqPoint/2, &xmin, &ymin, &xmax, &ymax);		//40 us

		for (uint16_t i=0; i<n_samples; i++)
			*(pvalue+i) +=temp[i];
		index_mean++;
	}
	enableINT();

}

#ifdef __KEIL__
#pragma pop
#endif

//void wait_for_trigger_micro(void){

//	while(global_triggerRampa)
//		__asm("nop");
//	while(!global_triggerRampa)
//		__asm("nop");
//	global_triggerRampa=0;
////	spi_usleep(470+PMD2_SendToRemote.field.measurementSettings.acqDelay);
//	spi_usleep(400+PMD2_SendToRemote.field.measurementSettings.acqDelay);
////	HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t*)waveform, 1024, DAC_ALIGN_12B_R); 
//}
void wait_for_trigger_micro(void){
	uint32_t waitTriggerCounter=0;
	Modbus_Task_CH2();
	if (service.modbusCH1)
		Modbus_Task_CH1();
	global_triggerRampa=0;
	while(!global_triggerRampa && waitTriggerCounter<100000)// controllare facendolo fermare
		waitTriggerCounter++;
		//__asm("nop");
	global_triggerRampa=0;
//	test_PIN_ON();
//tt	spi_usleep(400+PMD2_SendToRemote.field.measurementSettings.acqDelay);
//			spi_usleep((300+PMD2_SendToRemote.field.measurementSettings.acqDelay)/2.0);//tt

//	HAL_DAC_Start_DMA(&hdac, DAC_CHANNEL_1, (uint32_t*)waveform, 1024, DAC_ALIGN_12B_R); 
}

void disableINT(void)
{
	//HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
	HAL_NVIC_DisableIRQ(SysTick_IRQn);
	HAL_NVIC_DisableIRQ(TIM1_BRK_TIM15_IRQn);
	HAL_NVIC_DisableIRQ(TIM1_UP_TIM16_IRQn);	
  HAL_NVIC_DisableIRQ(DMA1_Channel1_IRQn);
  HAL_NVIC_DisableIRQ(DMA1_Channel2_IRQn);
  HAL_NVIC_DisableIRQ(DMA1_Channel5_IRQn);
  HAL_NVIC_DisableIRQ(DMA1_Channel6_IRQn);
  HAL_NVIC_DisableIRQ(DMA1_Channel7_IRQn);
	HAL_NVIC_DisableIRQ(USART1_IRQn);
	HAL_NVIC_DisableIRQ(USART2_IRQn);
  HAL_NVIC_DisableIRQ(DMA2_Channel6_IRQn);
  HAL_NVIC_DisableIRQ(DMA2_Channel7_IRQn);
}

void enableINT(void)
{
	//HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(SysTick_IRQn);
	HAL_NVIC_EnableIRQ(TIM1_BRK_TIM15_IRQn);
	HAL_NVIC_EnableIRQ(TIM1_UP_TIM16_IRQn);	
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
  HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);
  HAL_NVIC_EnableIRQ(DMA1_Channel6_IRQn);
  HAL_NVIC_EnableIRQ(DMA1_Channel7_IRQn);
	if (BOARD_VERSION==4){
		HAL_NVIC_EnableIRQ(USART1_IRQn);
//		HAL_NVIC_EnableIRQ(USART2_IRQn);
	}
	HAL_NVIC_EnableIRQ(DMA2_Channel6_IRQn);
	HAL_NVIC_EnableIRQ(DMA2_Channel7_IRQn);
}


#ifdef __KEIL__
#pragma  push
#pragma  O0
#endif
void  LTC1864L_SPI_Receive(uint8_t *pTxData, uint8_t *pRxData, uint32_t Timeout);
void  LTC1864L_SPI_ReceiveSD(uint8_t *pTxData, uint8_t *pRxData, uint32_t Timeout);

void fastLtc1864l_get_value( uint32_t *temp, uint16_t n_samples)
{
	uint16_t dummyTX=0;


	SPI2_NSS_GPIO_Port->BSRR=SPI2_NSS_Pin; // line = 1	
//	disableINT();
	SPI2_NSS_GPIO_Port->BSRR=(uint32_t)(SPI2_NSS_Pin) <<16U; // line = 0
//	for (uint16_t i=0; i<PMD2_SendToRemote.field.measurementSettings.waitBefore; i++){
//		temp[i]=0.0;
//		LTC1864L_SPI_Receive((uint8_t *)  &dummyTX, (uint8_t *)  &temp[i], HAL_MAX_DELAY);
//	}
	
	//start_clock_TIM5();
	wait_us_TIM2(PMD2_SendToRemote.field.measurementSettings.waitBefore);
	//PMD2_SendToRemote.field.measurementReading.lastTimeAcq=read_stop_clock_TIM5();
	for (uint16_t i=0; i<n_samples/2; i++){
		//SPI2_NSS_GPIO_Port->BSRR=(uint32_t)(SPI2_NSS_Pin) <<16U; // line = 0		
		//WDI_GPIO_Port->ODR ^= WDI_Pin; //  for debug only

		//spi_usleep(1);
		//SPI2_NSS_GPIO_Port->BSRR=(uint32_t)(SPI2_NSS_Pin) <<16U; // line = 0
		temp[i]=0.0;
		LTC1864L_SPI_Receive((uint8_t *)  &dummyTX, (uint8_t *)  &temp[i], HAL_MAX_DELAY);
	}
//	spi_usleep(200/2.0);//tt
//	for (uint16_t i=n_samples/2; i<n_samples/2+PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle; i++){
//		temp[i]=0.0;
//		LTC1864L_SPI_Receive((uint8_t *)  &dummyTX, (uint8_t *)  &temp[i], HAL_MAX_DELAY);
//	}
		wait_us_TIM2(PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle);

		for (uint16_t i=n_samples/2; i<n_samples; i++){
		//SPI2_NSS_GPIO_Port->BSRR=(uint32_t)(SPI2_NSS_Pin) <<16U; // line = 0		
		//WDI_GPIO_Port->ODR ^= WDI_Pin; //  for debug only

		//spi_usleep(1);
		//SPI2_NSS_GPIO_Port->BSRR=(uint32_t)(SPI2_NSS_Pin) <<16U; // line = 0
		temp[i]=0.0;
		LTC1864L_SPI_Receive((uint8_t *)  &dummyTX, (uint8_t *)  &temp[i], HAL_MAX_DELAY);
	}
//enableINT();
/**********************************/		
}

void fastLtc1864l_get_valueSD( uint32_t *temp, uint16_t n_samples)
{
	uint16_t dummyTX=0;


	SPI3_NSS_GPIO_Port->BSRR=SPI3_NSS_Pin; // line = 1	

	SPI3_NSS_GPIO_Port->BSRR=(uint32_t)(SPI3_NSS_Pin) <<16U; // line = 0
	wait_us_TIM2(PMD2_SendToRemote.field.measurementSettings.waitBefore);
	for (uint16_t i=0; i<n_samples/2; i++){
		temp[i]=0.0;
		LTC1864L_SPI_ReceiveSD((uint8_t *)  &dummyTX, (uint8_t *)  &temp[i], HAL_MAX_DELAY);
	}
		wait_us_TIM2(PMD2_SendToRemote.field.measurementSettings.waitInTheMiddle);

		for (uint16_t i=n_samples/2; i<n_samples; i++){
		temp[i]=0.0;
		LTC1864L_SPI_ReceiveSD((uint8_t *)  &dummyTX, (uint8_t *)  &temp[i], HAL_MAX_DELAY);
	}	
}

void postProcessingLtc1864l_get_value( void)
{
}
#ifdef __KEIL__
#pragma  pop
#pragma  O0
#endif

extern HAL_StatusTypeDef SPI_WaitFlagStateUntilTimeout(SPI_HandleTypeDef *hspi, uint32_t Flag, uint32_t State, uint32_t Timeout, uint32_t Tickstart);
extern HAL_StatusTypeDef SPI_WaitFifoStateUntilTimeout(SPI_HandleTypeDef *hspi, uint32_t Fifo, uint32_t State, uint32_t Timeout, uint32_t Tickstart);
extern HAL_StatusTypeDef SPI_EndRxTxTransaction(SPI_HandleTypeDef *hspi, uint32_t Timeout, uint32_t Tickstart);

inline void LTC1864L_SPI_Receive(uint8_t *pTxData, uint8_t *pRxData, uint32_t Timeout)
{
  uint32_t tickstart = 0U;

  /* Process Locked */
  //__HAL_LOCK(&hspi2); WARNING! LH 
	hspi2.Lock = HAL_LOCKED; // Locked forced see __HAL_LOCK macro
	
extern __IO uint32_t uwTick;
  /* Init tickstart for timeout management*/
  tickstart = uwTick; //HAL_GetTick();

  /* Don't overwrite in case of HAL_SPI_STATE_BUSY_RX */
  if (hspi2.State != HAL_SPI_STATE_BUSY_RX)
  {
    hspi2.State = HAL_SPI_STATE_BUSY_TX_RX;
  }

  /* Set the transaction information */
  hspi2.ErrorCode   = HAL_SPI_ERROR_NONE;
  //hspi2.pRxBuffPtr  = (uint8_t *)pRxData;
  //hspi2.RxXferCount = 1;
  //hspi2.RxXferSize  = 1;
  //hspi2.pTxBuffPtr  = (uint8_t *)pTxData;
  //hspi2.TxXferCount = 1;
  //hspi2.TxXferSize  = 1;

  /*Init field not used in handle to zero */
  //hspi2.RxISR       = NULL;
  //hspi2.TxISR       = NULL;

  /* Set the Rx Fifo threshold */
    /* Set fiforxthreshold according the reception data length: 16bit */
    CLEAR_BIT(hspi2.Instance->CR2, SPI_RXFIFO_THRESHOLD);

  /* Check if the SPI is already enabled */
  if ((hspi2.Instance->CR1 & SPI_CR1_SPE) != SPI_CR1_SPE)
  {
    /* Enable SPI peripheral */
    __HAL_SPI_ENABLE(&hspi2);
  }
	
	SPI2_NSS_GPIO_Port->BSRR=(uint32_t)(SPI2_NSS_Pin) <<16U; // line = 0
	
  /* Transmit and Receive data in 16 Bit mode */

	hspi2.Instance->DR = 0;//*((uint16_t *)pTxData);
	/* Check RXNE flag */
	while ( ! __HAL_SPI_GET_FLAG(&hspi2, SPI_FLAG_RXNE)){};
	
  *((uint16_t *)pRxData) = hspi2.Instance->DR;
	
	
	SPI2_NSS_GPIO_Port->BSRR=SPI2_NSS_Pin; // line = 1
	
  /* Control if the TX fifo is empty */
  if (SPI_WaitFifoStateUntilTimeout(&hspi2, SPI_FLAG_FTLVL, SPI_FTLVL_EMPTY, Timeout, tickstart) != HAL_OK)
  {
    SET_BIT(hspi2.ErrorCode, HAL_SPI_ERROR_FLAG);
    //return HAL_TIMEOUT;
  }
	

  /* Control the BSY flag */
  if (SPI_WaitFlagStateUntilTimeout(&hspi2, SPI_FLAG_BSY, RESET, Timeout, tickstart) != HAL_OK)
  {
    SET_BIT(hspi2.ErrorCode, HAL_SPI_ERROR_FLAG);
    //return HAL_TIMEOUT;
  }

	// ERO QUI 23oct19 - SPI2_NSS_GPIO_Port->BSRR=SPI2_NSS_Pin; // line = 1
	
	for ( volatile uint16_t i = 0; i < 14 ; i++ )UNUSED(i);


  /* Control if the RX fifo is empty */
  if (SPI_WaitFifoStateUntilTimeout(&hspi2, SPI_FLAG_FRLVL, SPI_FRLVL_EMPTY, Timeout, tickstart) != HAL_OK)
  {
    SET_BIT(hspi2.ErrorCode, HAL_SPI_ERROR_FLAG);
    //return HAL_TIMEOUT;
  }
	//if (__HAL_SPI_GET_FLAG(&hspi2, SPI_FLAG_RXNE))
	//{
	//	*((uint16_t *)pRxData) = hspi2.Instance->DR;
	//}	
  /* Check the end of the transaction */
/*	  if (SPI_EndRxTxTransaction(&hspi2, Timeout, tickstart) != HAL_OK)
  {
    hspi2.ErrorCode = HAL_SPI_ERROR_FLAG;
  }
*/	

	
error :
  hspi2.State = HAL_SPI_STATE_READY;
  __HAL_UNLOCK(&hspi2);

}

inline void LTC1864L_SPI_ReceiveSD(uint8_t *pTxData, uint8_t *pRxData, uint32_t Timeout)
{
  uint32_t tickstart = 0U;

  /* Process Locked */
  //__HAL_LOCK(&hspi2); WARNING! LH 
	hspi3.Lock = HAL_LOCKED; // Locked forced see __HAL_LOCK macro
	
extern __IO uint32_t uwTick;
  /* Init tickstart for timeout management*/
  tickstart = uwTick; //HAL_GetTick();

  /* Don't overwrite in case of HAL_SPI_STATE_BUSY_RX */
  if (hspi3.State != HAL_SPI_STATE_BUSY_RX)
  {
    hspi3.State = HAL_SPI_STATE_BUSY_TX_RX;
  }

  /* Set the transaction information */
  hspi3.ErrorCode   = HAL_SPI_ERROR_NONE;
  //hspi2.pRxBuffPtr  = (uint8_t *)pRxData;
  //hspi2.RxXferCount = 1;
  //hspi2.RxXferSize  = 1;
  //hspi2.pTxBuffPtr  = (uint8_t *)pTxData;
  //hspi2.TxXferCount = 1;
  //hspi2.TxXferSize  = 1;

  /*Init field not used in handle to zero */
  //hspi2.RxISR       = NULL;
  //hspi2.TxISR       = NULL;

  /* Set the Rx Fifo threshold */
    /* Set fiforxthreshold according the reception data length: 16bit */
    CLEAR_BIT(hspi3.Instance->CR2, SPI_RXFIFO_THRESHOLD);

  /* Check if the SPI is already enabled */
  if ((hspi3.Instance->CR1 & SPI_CR1_SPE) != SPI_CR1_SPE)
  {
    /* Enable SPI peripheral */
    __HAL_SPI_ENABLE(&hspi3);
  }
	
	SPI3_NSS_GPIO_Port->BSRR=(uint32_t)(SPI3_NSS_Pin) <<16U; // line = 0
	
  /* Transmit and Receive data in 16 Bit mode */

	hspi3.Instance->DR = 0;//*((uint16_t *)pTxData);
	/* Check RXNE flag */
	while ( ! __HAL_SPI_GET_FLAG(&hspi3, SPI_FLAG_RXNE)){};
	
  *((uint16_t *)pRxData) = hspi3.Instance->DR;
	
	
	SPI3_NSS_GPIO_Port->BSRR=SPI3_NSS_Pin; // line = 1
	
  /* Control if the TX fifo is empty */
  if (SPI_WaitFifoStateUntilTimeout(&hspi3, SPI_FLAG_FTLVL, SPI_FTLVL_EMPTY, Timeout, tickstart) != HAL_OK)
  {
    SET_BIT(hspi3.ErrorCode, HAL_SPI_ERROR_FLAG);
    //return HAL_TIMEOUT;
  }
	

  /* Control the BSY flag */
  if (SPI_WaitFlagStateUntilTimeout(&hspi3, SPI_FLAG_BSY, RESET, Timeout, tickstart) != HAL_OK)
  {
    SET_BIT(hspi3.ErrorCode, HAL_SPI_ERROR_FLAG);
    //return HAL_TIMEOUT;
  }

	// ERO QUI 23oct19 - SPI2_NSS_GPIO_Port->BSRR=SPI2_NSS_Pin; // line = 1
	
	for ( volatile uint16_t i = 0; i < 14 ; i++ )UNUSED(i);


  /* Control if the RX fifo is empty */
  if (SPI_WaitFifoStateUntilTimeout(&hspi3, SPI_FLAG_FRLVL, SPI_FRLVL_EMPTY, Timeout, tickstart) != HAL_OK)
  {
    SET_BIT(hspi3.ErrorCode, HAL_SPI_ERROR_FLAG);
    //return HAL_TIMEOUT;
  }
	//if (__HAL_SPI_GET_FLAG(&hspi2, SPI_FLAG_RXNE))
	//{
	//	*((uint16_t *)pRxData) = hspi2.Instance->DR;
	//}	
  /* Check the end of the transaction */
/*	  if (SPI_EndRxTxTransaction(&hspi2, Timeout, tickstart) != HAL_OK)
  {
    hspi2.ErrorCode = HAL_SPI_ERROR_FLAG;
  }
*/	

	
error :
  hspi3.State = HAL_SPI_STATE_READY;
  __HAL_UNLOCK(&hspi3);

}

