#ifndef TASKMANAGER_H
#define TASKMANAGER_H
#include "stm32l4xx_hal.h"
extern void ( *ptrTaskManager ) ( void );

void    powerUp_state( void );
void 		initStructure_state( void );
void    writeParameters_state( void );
void    startTemperatureControl_state(void);
void    waitControlTemperatureOne_state(void);
void    controlTemperatureOne_state(void);
void    controlTemperatureTwo_state(void);
void    startRamp_state(void);
void    dummy_state(void);
void    test_state(void);
void    print_n_values_first_protocol(uint32_t *pvalues, uint16_t n_values);
void    print_n_values_second_protocol(uint32_t *pvalues, uint16_t n_values);
void		print_n_values_third_protocol(uint32_t *pvalues, uint16_t n_values);
void 		print_conc(void);
void    choose_DC_level(void);
void    warming_housekeeping_state(void);
void    measurement_housekeeping_state(void);
void    measurement_state(void);
void    communication_state(void);
void 		MODBUS_state(void);
void    set_parameters_state(void);
void    calculate_concentration(void);
void 		calculate_concentration_diff(void);
void    change_settings_state(void);
void 		setting_state(void);
void    print_UART1(void);
void    print_UART2(void);

void    linearFit(void);
void    linearFit2(void);
void    frange(void);
void    line_center(void);
void    error_state(void);
void    taskSPI_communication(void);
void		update_state(void);
void 		load_struct_from_eeprom(void);
void 		load_struct_from_eeprom_OLD(void);
void		check_eeprom_value(void);
void 		write_struct_2_eeprom(void);
void 		change_sensitivity_level(void);
void 		set_sensitivity(uint8_t range);
void 		set_active_state(uint16_t number);
void 		activate_print(void);
void 		deactivate_print(void);
void 		reset_switchRange(void);
void    smooth(uint8_t nsm, uint32_t *x, uint16_t dim);
void 		meanv(uint32_t *v,	float *y_mean, uint16_t dim);
void 		minV(uint32_t *v,uint16_t dim, uint16_t *xmin, uint32_t *ymin);
void 		minmaxV(uint32_t *v,uint16_t dim, uint16_t *xmin, uint32_t *ymin, uint16_t *xmax, uint32_t *ymax);
void 		minmaxmeanV(uint32_t *v,uint16_t dim,float *y_mean, uint16_t *xmin, uint32_t *ymin, uint16_t *xmax, uint32_t *ymax);
void 		cpuCommand(void);
void 		init_BLE_UART(void);
void 		print_BLE_UART(void);
void		init_ramp(uint16_t timerRampa, float amp);
void 		start_ramp(void);

#define	 Tc_modbus													 1000L
#define	 Tc_setting_state										 100L
#define  Tc_dummy_state                      100L 
#define  Tc_test_state                       50L 
#define  Tc_powerUp_state                    10000L
#define  Tc_initStructure_state              1000L  
#define  Tc_housekeeping_state               3000L  
#define  Tc_measurement_state                1000L
#define  Tc_communication_state              1000L  
#define  Tc_writeParameters_state            1000L  
#define  Tc_startTemperatureControl_state    1000L  
#define  Tc_waitControlTemperatureOne_state  1000L  
#define  Tc_controlTemperatureOne_state      1000L  
#define  Tc_controlTemperatureTwo_state      1000L 
#define  Tc_startRamp_state                  1000L 
#define  Tc_choose_DC_level                  100L 
#define  Tc_measurement                      100L 
#define  Tc_warming_state                    1000L

#endif
