#ifndef __I2C_DAC_AD5693_H
#define __I2C_DAC_AD5693_H

#define TRIAL_COUNT     (3)
#define COMM_TIMEOUT    (1000)
// 7-bit address left shift is required
#define AD5693_ADDR_VGAGAIN			(0x4E << 1)
#define AD5693_ADDR_TEMPLAS     (0x4C << 1)
#define AD5693_ADDR_ILAS     		(0x4E << 1)
#define AD5693_ADDR_DCLEVEL     (0x4C << 1)


// Define DB7:DB4 of Command byte
//
typedef enum {
	AD5693_NO_OPERATION                    = 0x00,
	AD5693_WRITE_INPUT_REGISTER            = 0x10,
	AD5693_UPDT_DAC_REG                    = 0x20,
	AD5693_WRITE_DAC_AND_INP_REG           = 0x30,
	AD5693_WRITE_CTRL_REG           	= 0x40
} AD5693_Command_Bits;


//HAL_StatusTypeDef ad5693_is_ready(void);
//HAL_StatusTypeDef ad5693_set_channel(uint16_t value);
//HAL_StatusTypeDef ad5693_read_back(uint8_t *buf_read_back);
HAL_StatusTypeDef ad5693_is_ready(I2C_HandleTypeDef *phi2c, uint16_t DevAddress);
HAL_StatusTypeDef ad5693_set_channel(I2C_HandleTypeDef *phi2c, uint16_t DevAddress, uint16_t value);
HAL_StatusTypeDef ad5693_read_back(I2C_HandleTypeDef *phi2c, uint16_t DevAddress, uint8_t *buf_read_back);
#endif

