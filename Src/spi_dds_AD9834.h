#ifndef SPI_DDS_AD9834_H
#define SPI_DDS_AD9834_H
#include "main.h"
#define SPI1_NSS                GPIO_PIN_5
#define SPI1_MOSI               GPIO_PIN_4
#define SPI1_MISO               GPIO_PIN_3
#define SPI1_SCK                GPIO_PIN_2
#define SPI1_GPIO_Port     			GPIOG

void initSpi_Dds_AD9834 ( void );
static void spi_usleep ( unsigned int delusecs );

void Enable_DDS_FSYNC_2f ( void );
void Disable_DDS_FSYNC_2f ( void );
void Enable_DDS_FSYNC_f ( void );
void Disable_DDS_FSYNC_f ( void );
void RF_in_phase_same_freq_init(void);
void reset_DDS_rf(void);
void set_DDS_rf(void);
void spi_Dds_AD9834_Write_Word ( uint16_t dat );
void start_saw_tooth();
void RF_in_phase_same_freq_init_B();
void set_phase_f(unsigned char phase_index);
void set_scan_frequency_Hz(uint16_t scan_freq_Hz);
#endif