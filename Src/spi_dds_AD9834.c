#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "stm32l4xx_hal.h"

#include "spi_dds_AD9834.h"

// Output - HOLDn_IO3_Pin, WPn_IO2_Pin, RSTn_RFU_Pin
//              HAL_GPIO_WritePin(GPIOD, RSTn_RFU_Pin|WPn_IO2_Pin|HOLDn_IO3_Pin, GPIO_PIN_SET);
//
// Output - CS2n_Com_Pin
//              HAL_GPIO_WritePin(GPIOD, CS2n_Com_Pin, GPIO_PIN_RESET);
//
// Da programmare:
//
// Output - SPI2_NSS, SPI2_SCK, SPI2_MOSI
//              GPIO_InitTypeDef GPIO_InitStruct;
//              HAL_GPIO_WritePin(GPIOB, SPI2_NSS|SPI2_SCK|SPI2_MOSI, GPIO_PIN_RESET);
//
//              GPIO_InitStruct.Pin = SPI2_NSS|SPI2_SCK|SPI2_MOSI;
//              GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
//              GPIO_InitStruct.Pull = GPIO_NOPULL;
//              GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
//              HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
//
// Input - SPI2_MISO
//              GPIO_InitTypeDef GPIO_InitStruct;
//              GPIO_InitStruct.Pin = SPI2_MISO;
//              GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
//              GPIO_InitStruct.Pull = GPIO_NOPULL;
//              HAL_GPIO_Init(SPI2_MISO_GPIO_Port, &GPIO_InitStruct);



void initSpi_Dds_AD9834 ( void )
{
    GPIO_InitTypeDef GPIO_InitStruct;

    GPIO_InitStruct.Pin = SPI1_MISO;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init ( SPI1_GPIO_Port, &GPIO_InitStruct );
    
    GPIO_InitStruct.Pin = SPI1_SCK|SPI1_MOSI;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init ( SPI1_GPIO_Port, &GPIO_InitStruct );
   
    HAL_GPIO_WritePin ( FSYNC_2rf_GPIO_Port, FSYNC_2rf_Pin,GPIO_PIN_SET );
    HAL_GPIO_WritePin ( FSYNC_rf_GPIO_Port, FSYNC_rf_Pin,GPIO_PIN_SET );
    HAL_GPIO_WritePin ( RST_rf_GPIO_Port, RST_rf_Pin,GPIO_PIN_RESET ); 
    HAL_GPIO_WritePin ( RST_rf_GPIO_Port, RST_rf_Pin,GPIO_PIN_RESET );
    HAL_GPIO_WritePin ( RST_rf_GPIO_Port, RST_rf_Pin,GPIO_PIN_SET );
    HAL_GPIO_WritePin ( RST_rf_GPIO_Port, RST_rf_Pin,GPIO_PIN_RESET );
    
    HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_RESET );
    HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_MOSI,GPIO_PIN_SET );
}

#pragma optimize=none
static void spi_usleep ( unsigned int delusecs )
{
    int i;
    for ( i = 0; i < 64 * delusecs; i++ );
}

//======================================================================
//Enable_DDS_FSYNC_2rf(void)
//======================================================================
void Enable_DDS_FSYNC_2rf ( void )
{
    uint8_t i = 5;

    HAL_GPIO_WritePin ( FSYNC_2rf_GPIO_Port, FSYNC_2rf_Pin,GPIO_PIN_RESET );
    while ( i -- );
}

//======================================================================
//Disable_DDS_FSYNC_2rf(void)
//======================================================================
void Disable_DDS_FSYNC_2rf ( void )
{
    uint8_t i = 5;

    HAL_GPIO_WritePin ( FSYNC_2rf_GPIO_Port, FSYNC_2rf_Pin,GPIO_PIN_SET );
    while ( i -- );
}

//======================================================================
//Enable_DDS_FSYNC_rf(void)
//======================================================================
void Enable_DDS_FSYNC_rf ( void )
{
    uint8_t i = 5;

    HAL_GPIO_WritePin ( FSYNC_rf_GPIO_Port, FSYNC_rf_Pin,GPIO_PIN_RESET );
    while ( i -- );
}

//======================================================================
//Disable_DDS_FSYNC_rf(void)
//======================================================================
void Disable_DDS_FSYNC_rf ( void )
{
    uint8_t i = 5;

    HAL_GPIO_WritePin ( FSYNC_rf_GPIO_Port, FSYNC_rf_Pin,GPIO_PIN_SET );
    while ( i -- );
}


//======================================================================
//spi_Dds_AD9834_Write_Word(uint16_t dat)
//======================================================================
void spi_Dds_AD9834_Write_Word ( uint16_t dat )
{
    uint8_t i = 0;
    uint16_t sdat = 0;
    uint16_t bitmask= 	0x8000; 

    // Serial Clock Input. Data is clocked into the AD9834 on each falling SCLK edge.
    //When FSYNC is taken low, the internal logic is informed that a new word is being loaded into the device.
    sdat = dat;
    for ( i = 0; i < 16; i ++ ) {
        if ( sdat & bitmask ) {
            HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_MOSI,GPIO_PIN_SET );
        } else {
          
            HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_MOSI,GPIO_PIN_RESET );
        }
        HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET ); // io lo sposterei prima
        sdat <<= 1;
        //bitmask = bitmask >> 1;		// shift verso DX prendo i bit dal pi� significativo al meno significativo
        //spi_usleep(10);
        HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_RESET );  // deve stare gi� almeno 10 ns
    }
   
}



void reset_DDS_rf(void)				// disattivo i DDS radiofrequenza
{
   HAL_GPIO_WritePin ( RST_rf_GPIO_Port, RST_rf_Pin,GPIO_PIN_SET );
}

void set_DDS_rf(void)					// riattivo i DDS radiofrequenza
{
   HAL_GPIO_WritePin ( RST_rf_GPIO_Port, RST_rf_Pin,GPIO_PIN_RESET );

}

void RF_in_phase_same_freq_init(void){

	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );								// devo iniziare con sck 1
	
  
	//*****  DDS 1
	
  Disable_DDS_FSYNC_2rf();
  Enable_DDS_FSYNC_2rf();							 
	spi_Dds_AD9834_Write_Word(8744); //0b10 0010 0010 1000		// parola di controllo, reset HARDWARE 10001000101000
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_2rf();
	spi_usleep(10); 
  //31,485,242   0100 0111 1000 0001    0110 1101 0011 1010      
  Disable_DDS_FSYNC_2rf();
  Enable_DDS_FSYNC_2rf();
//	spi_Dds_AD9834_Write_Word(16384); //0b0100000000000000		// freq LSB
	spi_Dds_AD9834_Write_Word(27962); //0b 0110 1101 0011 1010		// freq LSB
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_2rf();
	spi_usleep(10);
        
  Disable_DDS_FSYNC_2rf();
  Enable_DDS_FSYNC_2rf();
//	spi_Dds_AD9834_Write_Word(18432); //0b0100 1000 0000 0000		// freq MSB
	spi_Dds_AD9834_Write_Word(18305); //0b 0100 0111 1000 0001		// freq MSB
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_2rf();
	spi_usleep(10);
        
       
  Disable_DDS_FSYNC_2rf();
  Enable_DDS_FSYNC_2rf();
	spi_Dds_AD9834_Write_Word(50368); //0b1100010011000000		// phase
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_2rf();
	spi_usleep(10);


	//*****  FINE DDS 1
	
	
		HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );								// devo iniziare con sck 1
	
  
	//*****  DDS 1
	
  Disable_DDS_FSYNC_rf();
  Enable_DDS_FSYNC_rf();
	spi_Dds_AD9834_Write_Word(8704); //0b0010 0010 0010 1000		// parola di controllo, reset HARDWARE
//	spi_Dds_AD9834_Write_Word(8704); //0b0010001000000000		// parola di controllo, reset HARDWARE 10 0010 0000 0000
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_rf();
	spi_usleep(10);
        
  Disable_DDS_FSYNC_rf();
  Enable_DDS_FSYNC_rf();
	//spi_Dds_AD9834_Write_Word(16384); //0b 0100 0000 0000 0000		// freq LSB
	spi_Dds_AD9834_Write_Word(30365); //0b 0111 0110 1001 1101		// freq LSB
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_rf();
	spi_usleep(10);
  //0100 0000 0000       00 0000 0000 0000
	//15742621    0011 1100 0000       11 0110 1001 1101
	Disable_DDS_FSYNC_rf();
  Enable_DDS_FSYNC_rf();
	//spi_Dds_AD9834_Write_Word(17408); //0b 0100 0100 0000 0000		// freq MSB
	spi_Dds_AD9834_Write_Word(17344); //0b 0100 0011 1100 0000		// freq MSB
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_rf();
	spi_usleep(10);
        
        
  Disable_DDS_FSYNC_rf();
  Enable_DDS_FSYNC_rf();
	spi_Dds_AD9834_Write_Word(50368); //0b1100010011000000		// phase
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_rf();
	spi_usleep(10);
	
	
	
//	//*****  DDS 2
//	
//  Disable_DDS_FSYNC_rf();
//  Enable_DDS_FSYNC_rf();
//	spi_Dds_AD9834_Write_Word(8704); //0b0010001000000000		// parola di controllo, reset HARDWARE 10 0010 0000 0000
//	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
//	Disable_DDS_FSYNC_rf();
//	spi_usleep(10);
//        
//	Disable_DDS_FSYNC_rf();
//	Enable_DDS_FSYNC_rf();
//	spi_Dds_AD9834_Write_Word(16384); //0b0100000000000000		// freq LSB
//	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
//	Disable_DDS_FSYNC_rf();
//	spi_usleep(10);
//        
//	Disable_DDS_FSYNC_rf();
//	Enable_DDS_FSYNC_rf();
//	spi_Dds_AD9834_Write_Word(17408); //0b0100010000000000		// freq MSB
//	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
//	Disable_DDS_FSYNC_rf();
//	spi_usleep(10);
//        
//        
//	Disable_DDS_FSYNC_rf();
//	Enable_DDS_FSYNC_rf();
//	spi_Dds_AD9834_Write_Word(49152); //0b1100000000000000		// phase
////	spi_Dds_AD9834_Write_Word(49152+64*20); //0b1100000000000000		// phase
//	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
//	Disable_DDS_FSYNC_rf();
//	spi_usleep(10);

//	//*****  FINE DDS 2	
}


void RF_in_phase_same_freq_init_B(void){

	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );								// devo iniziare con sck 1
	
  Disable_DDS_FSYNC_rf();        Disable_DDS_FSYNC_2rf(); 
  Enable_DDS_FSYNC_rf();        Enable_DDS_FSYNC_2rf();
	spi_Dds_AD9834_Write_Word(8450); //0b0010000100001000		// parola di controllo, reset software
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_rf();	Disable_DDS_FSYNC_2rf();
	spi_usleep(10);
  
	//*****  DDS 1

        
  Disable_DDS_FSYNC_rf();
  Enable_DDS_FSYNC_rf();
	spi_Dds_AD9834_Write_Word(16384); //0b0100000000000000		// freq LSB
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_rf();
	spi_usleep(10);
        
  Disable_DDS_FSYNC_rf();
  Enable_DDS_FSYNC_rf();
	spi_Dds_AD9834_Write_Word(18432); //0b0100100000000000		// freq MSB  0b1100010011000000
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_rf();
	spi_usleep(10);
        
        
  Disable_DDS_FSYNC_rf();
  Enable_DDS_FSYNC_rf();
	spi_Dds_AD9834_Write_Word(50368); //0b1100010011000000		// phase
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_rf();
	spi_usleep(10);


	//*****  FINE DDS 1
	
	
	//*****  DDS 2
	

        
	Disable_DDS_FSYNC_2rf();
	Enable_DDS_FSYNC_2rf();
	spi_Dds_AD9834_Write_Word(16384); //0b0100000000000000		// freq LSB
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_2rf();
	spi_usleep(10);
        
	Disable_DDS_FSYNC_2rf();
	Enable_DDS_FSYNC_2rf();
	spi_Dds_AD9834_Write_Word(17408); //0b0100010000000000		// freq MSB
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_2rf();
	spi_usleep(10);
        
        
	Disable_DDS_FSYNC_2rf();
	Enable_DDS_FSYNC_2rf();
	spi_Dds_AD9834_Write_Word(49152+64*0); //0b1100000000000000		// phase
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_2rf();
	spi_usleep(10);

	//*****  FINE DDS 2	

        
	Disable_DDS_FSYNC_rf();        Disable_DDS_FSYNC_2rf(); 
	Enable_DDS_FSYNC_rf();        Enable_DDS_FSYNC_2rf();
	spi_Dds_AD9834_Write_Word(8194); //0b0010000100000000		//  parola di controllo, esco reset
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_rf();
	Disable_DDS_FSYNC_2rf();
	spi_usleep(10);

}


void set_phase_f(unsigned char phase_index)
{
	// 64 punti, risoluzione di 5,6 � 
	unsigned int phaseWord = 0;
	// uso solo i 6 bit msb
	// 360/64		5.625 � ris
	// 0b1100000000000000 --> xxxx000001000000	* phase_index
	// + 
	// 0b1100xxxxxxxxxxxx

	if (phase_index >= 64)		phase_index = 0;	
	
	phaseWord = 64;
	phaseWord = phaseWord * phase_index;
	phaseWord = phaseWord | 49152;
	
	
	Disable_DDS_FSYNC_rf();
	Enable_DDS_FSYNC_rf();
	spi_Dds_AD9834_Write_Word(phaseWord); //0b1100000000000000		// phase
	HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
	Disable_DDS_FSYNC_rf();
	spi_usleep(10);

}




//void set_scan_frequency_Hz(uint16_t scan_freq_Hz){
//    uint16_t freqLSB, freqMSB;
//    
//    uint32_t freq_28=(uint32_t)(5.5924053*scan_freq_Hz);
//    freqMSB=(freq_28/2^14);
////    freqLSB=(freq_28-freqMSB*2^14);
////    freqMSB=freq_28>>14;
////    freqLSB=(freq_28<<18)>>14;
//    freqLSB=(uint16_t)(5.5924053*scan_freq_Hz);
//    freqMSB=0;
//    HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );								// devo iniziare con sck 1
//   
//    Disable_DDS_FSYNC_ST();
//    Enable_DDS_FSYNC_ST();
//    spi_Dds_AD9834_Write_Word(8450); //0b0010000100000010		// parola di controllo, reset software
//    HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
//    Disable_DDS_FSYNC_ST();
//    spi_usleep(10);
//   
//    Disable_DDS_FSYNC_ST();
//    Enable_DDS_FSYNC_ST();
//    spi_Dds_AD9834_Write_Word(16384+freqLSB);//0b0100010100100000	// freq LSB
//    HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
//    Disable_DDS_FSYNC_ST();
//    spi_usleep(10);
//    
//    Disable_DDS_FSYNC_ST();
//    Enable_DDS_FSYNC_ST();
//    spi_Dds_AD9834_Write_Word(16384+freqMSB); // 0b0100000000000000	// freq MSB
//    HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
//    Disable_DDS_FSYNC_ST();
//    spi_usleep(10);
//    
//    Disable_DDS_FSYNC_ST();
//    Enable_DDS_FSYNC_ST();
//    spi_Dds_AD9834_Write_Word(49152);//0b1100000000000000 // phase 
//    HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
//    Disable_DDS_FSYNC_ST();
//    spi_usleep(10);
//    
//    Disable_DDS_FSYNC_ST();
//    Enable_DDS_FSYNC_ST();
//    spi_Dds_AD9834_Write_Word(8194); //0b0010000000000010	// parola di controllo, esco reset
//    HAL_GPIO_WritePin ( SPI1_GPIO_Port, SPI1_SCK,GPIO_PIN_SET );
//    Disable_DDS_FSYNC_ST();
//    spi_usleep(10);	
//};
