/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "i2c_quad_dac_AD5696.h"

/* Private variables ---------------------------------------------------------*/
extern I2C_HandleTypeDef hi2c3;


HAL_StatusTypeDef ad5696_is_ready(void)
{
    return HAL_I2C_IsDeviceReady(&hi2c3, AD5696_ADDR, TRIAL_COUNT, COMM_TIMEOUT);
}

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
HAL_StatusTypeDef ad5696_set_channel(uint16_t channel, uint16_t value)
{
  uint8_t buffer[3];
  HAL_StatusTypeDef tmpRetVal;

  if ( (tmpRetVal=ad5696_is_ready()) == HAL_OK ) {

    buffer[0]=channel & 0xff;
    
    buffer[1]=(value>>8) & 0xFF;        //MSB byte  
    buffer[2]= value & 0xFF;            //LSB byte  
    tmpRetVal=HAL_I2C_Master_Transmit(&hi2c3,AD5696_ADDR,buffer,3,100);  

 }
  
  return tmpRetVal;
}


HAL_StatusTypeDef ad5696_read_back(uint8_t *buf_read_back)
{
  uint8_t buffer;
  HAL_StatusTypeDef tmpRetVal;

  if ( (tmpRetVal=ad5696_is_ready()) == HAL_OK ) {

    // ReadBack
    buffer=0x31;
    
    HAL_I2C_Master_Transmit(&hi2c3, AD5696_ADDR, &buffer, 1, 100);
    HAL_I2C_Master_Receive(&hi2c3, AD5696_ADDR, buf_read_back, 8, 100);

 }
  
  return tmpRetVal;
}


