#ifndef AD_H
#define AD_H

#include <stdint.h>

#define         VREF            33          //3.3 volt
#define         MAX_ADC_VALUE   4096

typedef struct adStruct {
  uint16_t ch;  
  uint16_t *destBuf;
  uint16_t *srcBuf;
  uint16_t dimBuf;
  uint16_t idxBuf;  
  uint16_t avg;
} type_adStruct;

extern uint16_t ADC1ConvertedValues[16];

extern type_adStruct  ad_V_Temp;                        
extern type_adStruct  ad_LASER_ANODE_BUF;      
extern type_adStruct  ad_LASER_CATHODE_BUF;  
extern type_adStruct  ad_difoutBUF;                  
extern type_adStruct  ad_IPELT;                            
extern type_adStruct  ad_TEMP_LASER;    
  

extern uint16_t  avg_ad_V_Temp;                        
extern uint16_t  avg_ad_LASER_ANODE_BUF;      
extern uint16_t  avg_ad_LASER_CATHODE_BUF;  
extern uint16_t  avg_ad_difoutBUF;                  
extern uint16_t  avg_ad_IPELT;                            
extern uint16_t  avg_ad_TEMP_LASER;   


void updateAdStruct(void);
void AD_circularBuffer(void);
void initAnalogInputs ( void );
void getAdAverageValue(void);

#endif
