#ifndef EPROM_ADDRESS_H
#define EPROM_ADDRESS_H

#define     EEP_ADDR_EEP_TEST           0x0004        // 4 byte 

#define     EEP_LANGUAGE                0x0008        // 1 byte 
#define     EEP_SERIAL_NUMBER           0x0009        // 4 byte 
#define     EEP_IDMODBUS           			0x000D        // 1 byte 

//////////////////////////////////////////////////////////////////////////
//        TYPE LASER EEP_LASER_TEMPERATURE
//////////////////////////////////////////////////////////////////////////

#define     EEP_ID                      		0x0010           										// 4 byte     uint32_t    id;             
#define     EEP_LASER_ID                    EEP_ID +4        										// 4 byte     uint32_t    laser_id;             
#define     EEP_LASER_TEMPERATURE       		EEP_LASER_ID +4    									// 4 byte     float		    laser temperature;		 
#define     EEP_LASER_CURRENT_HS						EEP_LASER_TEMPERATURE +4      			// 4 byte     float		    laser currenthigh sensitivity;		 
#define     EEP_LASER_RAMP_AMPLITUDE_HS   	EEP_LASER_CURRENT_HS +4     				// 4 byte     float		    laser ramp amplitude high sensitivity;		 
#define     EEP_LASER_RAMP_FREQUENCY    		EEP_LASER_RAMP_AMPLITUDE_HS +4 			// 4 byte     float		    laser rampa frequency;		 
#define     EEP_LASER_CURRENT_LS					 	EEP_LASER_RAMP_FREQUENCY +4     		// 4 byte     float		    laser current low sensitivity;		 
#define     EEP_LASER_RAMP_AMPLITUDE_LS   	EEP_LASER_CURRENT_LS +4     		  	// 4 byte     float		    laser ramp amplitude low sensitivity;		 
#define     EEP_CALIB_HIGH						   		EEP_LASER_RAMP_AMPLITUDE_LS+4		  	// 4 byte     float				calibration factor high sensitivity;		
#define     EEP_CALIB_LOW							   		EEP_CALIB_HIGH+4		  							// 4 byte     float				calibration factor low sensitivity;
#define     EEP_NUMBER_OF_ACQUISITION   		EEP_CALIB_LOW+4		  								// 2 byte     uint16_t		number of acquisition for the average;
#define     EEP_ACQUISITION_DELAY			   		EEP_NUMBER_OF_ACQUISITION+2					// 2 byte     uint16_t		delay in usec before the acquisition of the ramp;
#define     EEP_TEST_CELL_CONCENTRATION			EEP_ACQUISITION_DELAY+2							// 4 byte     float				Test cell concentration in ppm;

#endif
