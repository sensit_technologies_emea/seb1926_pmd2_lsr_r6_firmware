/*""FILE COMMENT""*******************************************************
*
*System Name : ??
*File Name   : main.c
*Version     : see 'version.c' file
*Contents    : Main routine
*Customer    : FPOM
*Model       :
*Order       :
*CPU         : M32C M30833FJFP
*Compiler    : M32C/80,M16C/80 Series NC308 COMPILER V.5.20 Release 1
*OS          : Not used
*Programmer  : 
*Note        : None
*
************************************************************************
*Copyright,2004 FPOM srl
************************************************************************
*""FILE COMMENT END""***************************************************/
#include <stdio.h>
#include <string.h>

#include "main.h"
#include "stm32l4xx_hal.h"
#include "sensit.h"
#include "version.h"
#include "timer.h"
#include "ad.h"
#include "eeprom_address.h"
#include "modbus_extension_register.h"
#include "spi_address.h"
#include "taskManager.h"
#include "modbus.h"
#include "usart_pc.h"

unsigned long OFFSET;


void resetWatchDog(){

}

void main_RoadRunner(void);

void main_RoadRunner(void){
  
//-------------------------------------------------------------------
//------ state machine-----
  timCiclo1ms = Tc_powerUp_state;
  ptrTaskManager = &powerUp_state;
  
	while (1){
		( *ptrTaskManager ) (); 	
		Modbus_Task_CH2();
//		service.modbusCH1=check_controller_RTS();
//		if (service.modbusCH1){
//			enable_controller_Modbus();
//			Modbus_Task_CH1();		
//		}else{
//			disable_controller_Modbus();
//		}
		if (PMD2_SendToRemote.field.flag.firmwareUpdate)
			ptrTaskManager= &update_state;
	}
}


