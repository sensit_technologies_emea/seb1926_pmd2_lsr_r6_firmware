#ifndef MODBUS_REGISTER_ESTENSION_H_
#define MODBUS_REGISTER_ESTENSION_H_

#include <stdint.h>
 
#define PW_ADMIN_CMD                    	7689 
#define ADMIN_CMD_DISABLE_PRIMARY_MODBUS	1001
#define ADMIN_CMD_CHANGE_MODBUS_ID     		1002 //altre 2 word canale 1 word indirizzo
#define ADMIN_CMD_CHANGE_MODBUS_BAUDRATE	1003
#define ADMIN_CMD_CHANGE_SERIAL_NUMBER  	1004

#define MODBUS_CHANNEL_1                    1
#define MODBUS_CHANNEL_2                    2
#define MODBUS_CHANNEL_3                    3
#define MODBUS_CHANNEL_4                    4



/*
#define PSG_REGISTER_BASE_FUNCTION_3					30000
#define PSG_REGISTER_BASE_FUNCTION_4					40000

#define PSG_REGISTER_BASE_START_1							00000
#define PSG_REGISTER_BASE_START_2							01000
#define PSG_REGISTER_BASE_START_3							02000
#define PSG_REGISTER_BASE_START_4							03000
#define PSG_REGISTER_BASE_START_5							04000
#define PSG_REGISTER_BASE_START_6							05000
#define PSG_REGISTER_BASE_START_7							06000
#define PSG_REGISTER_BASE_END_1								00009
#define PSG_REGISTER_BASE_END_2								01019
#define PSG_REGISTER_BASE_END_3								02010
#define PSG_REGISTER_BASE_END_4								03011
#define PSG_REGISTER_BASE_END_5								04018
#define PSG_REGISTER_BASE_END_6								05020
#define PSG_REGISTER_BASE_END_7								07442
*/
#define KT_1MS_ACCEPT_ONLY_SECONDARY_CHANNEL	((uint32_t) (30*60*1000))	// 30 minuti 

//#define DIM_UINT16_REGISTER_EXTERNSION  	256   //VERIFY DIMENSION
#define DIM_UINT16_REGISTER_EXTERNSION  	2200   //VERIFY DIMENSION
#define DIM_COD_PLACE                   	40
#define DIM_COD_SAMPLING                	40
//
// Size of packed structure : 172 byte 
//



#pragma pack(push,2)
typedef struct {
	struct{
		uint16_t	tecStatus:1;				//0		bit	ThermoElectricControl on laser diode 0/1 (OFF/ON)
		uint16_t	laserStatus:1;			//1		bit Laser Current 0/1 (OFF/ON)
		uint16_t	centerFlag:1;				//2		bit Line Centering 0/1 (OFF/ON)
		uint16_t	switchRange:1;			//3		bit Change sensitivity 0/1 (OFF/ON)
		uint16_t	autoBackground:1;		//4		bit	Automatic background subtraction and gain 0/1 (OFF/ON)
		uint16_t	autoRange:1;				//5		bit	Automatic select the right sensitivity 0/1 (OFF/ON)
		uint16_t	print:1;						//6		bit	Print the debug graph 0/1 (OFF/ON)
		uint16_t	sensorMode:1;				//7		bit Sensor Mode 0/1 (SET/MEASUREMENT)
		uint16_t 	e2proomSave:1;			//8		bit	1 save data on eeprom 
		uint16_t 	e2proomLoad:1;			//9		bit	1 load data from eeprom
		uint16_t	errorFlag:1;				//10	bit	1 activate error control
		uint16_t 	modbusLock:1;				//11	bit	MODBUS access 0/1 (ON/OFF)
		uint16_t 	firmwareUpdate:1;						//12	bit	spare coil
		uint16_t 	uartBridge:1;				//13	bit	spare coil
		uint16_t 	flagBit14:1;				//14	bit	spare coil
		uint16_t 	flagBit15:1;				//15	bit	spare coil
	} flag; //1 word

  struct {	// MODBUS address = 40000 Real Address 1
		uint32_t 	Serial_Nr;		     	//0-2	Serial Number	 
		uint16_t 	version;						//2-1	Version
		uint16_t 	release;						//3-1	Release
		uint16_t 	calYear;            //4-1	Year of last Calibration 
		uint16_t 	calMonth;        		//5-1	Month of last Calibration 
		uint16_t 	calDay;             //6-1	Day of last Calibration 
		uint16_t	modbusID;						//7-1 ID for modbus communication 
    } system; // 8 word

    
	struct{ // MODBUS address = 41000 Real address 9
		uint16_t	laserID;							//0+1	ID of laser diode
		float			laserTemperature;			//1+2	working temperature for laser diode
		float			laserCurrentHS;				//3+2	working current of laser diode for High Sensitivity
		float			laserRampAmplitudeHS;	//5+2	amplitude of current ramp for High Sensitivity
		float			laserRampFrequency;		//7+2	frequency ramp 
		float			laserCurrentLS;				//9+2	working current of laser diode for Low Sensitivity
		float			laserRampAmplitudeLS;	//11+2	amplitude of current ramp for Low Sensitivity
		float 		testCellConcentration;//13+2	test cell concentration
		float			calibHS;							//15+2	calibration coefficient for High Sensitivity
		float			calibLS;							//17+2	calibration coefficient for Low Sensitivity
	}workingSettings; // 19 word
	
	struct{ // MODBUS address 42000 Real address 28 
		float			laserTemperature;			//0+2	laser diode temperature read by internal ADC
		float			laserCurrent;					//2+2	laser diode current read by internal ADC
		float			laserVoltage;					//4+2	laser diode voltage read by internal ADC
		float			laserTECCurrent;			//6+2	laser diode TEC current read by internal ADC
		float			cellTemperature;			//8+2	temperature of the cell read by internal ADC
	} houseKeeping; //10 word

	struct{ // MODBUS address 43000 Real address 38
		uint16_t	nAvg;								//0+1		number of ramps acquired to give a single concentration value
		uint16_t	nAcqPoint;					//1+1		number of points acquired for a single ramp
		uint16_t	PHDBackground;			//2+1		value set on DAC to subtract a voltage value form the PHD signal
		uint16_t	PHDGain;						//3+1		value set on a digital potentiometer to set the amplification gain for the PHD signal
		uint16_t	nSmoothingPoint;		//4+1		number of acquired points considered for a moving average on the acquired signal
		uint16_t	deltaX;							//5+1		number of acquired points 
		uint16_t	startPoint;					//6+1		the first acquired point that is considered to calculate the concentration value 
		uint16_t	endPoint;						//7+1		the last acquired point that is considered to calculate the concentration value
		uint16_t	waitBefore;					//8+1		delay in us between the start of a ramp and the acquisition
		uint16_t	waitInTheMiddle;		//9+1		delay in us between the acquisition of the descending and ascending part of the ramp
	}measurementSettings;//10 word

	struct{ // MODBUS address 44000 Real Address 48
		uint16_t 	rampStatus;					//0+1		this value indicated if the acquired ramp s centered in the ADC acquisition windows
		uint16_t	PHDZero;						//1+1		PHD signal acquired qhen the laser diode is off
		float			concentrationRaw;		//2+2		
		float			concentration;			//4+2		concentration value calculated from the acquired signal
		float			concentrationB;			//6+2		concentration value calculated from the ascending part of the acquired signal
		float			PHDPower;						//8+2		the mean value of the PHD acquired signal during the last ramp
		uint16_t	bit;								//10+1	number of bits that are used to acuired the significant part of the PHD signal
		uint16_t	lastTimeAcq;				//11+1  time for last acquisition
		uint16_t	counterM;						//12+1  progressivive number from start
		uint16_t 	sensitivityLevel;		//13+1  Sensitivity Level 0/1/2 (MIN/LOW/HIGH)
		uint16_t	errorID;						//14+1		
		uint16_t	activeState;				//15+1		indicates which function the firmware is currently executing 
		uint16_t	quality;						//16+1
	}measurementReading; // 17 word
	
	struct{// MODBUS address 45000 Real Address 65
		float			calibrationGasRange1[5];	//0+10		calibration gas 1 for range 1 usually 0
		float			rawValueRange1[5];				//10+10
		float			calibrationGasRange2[5];	//20+10		calibration gas 1 for range 1 usually 0
		float			rawValueRange2[5];				//30+10
		float			calibrationGasRange3[5];	//40+10		calibration gas 1 for range 1 usually 0
		float			rawValueRange3[5];				//50+10
	}calibration;// 60 word 
	
	struct { // MODBUS addres 46000 Real Address 125
		uint16_t	acquiredLength; 					//0+1
		uint16_t	elaboratedLength;					//1+1
		float acquiredVector[450];					//2+900
		float elaboratedVector[270];				//902+540
	} data;//1442 word
	
} structModbusRegisterInContiguosMemory; //1087 word
#pragma pack(pop)

typedef union unionModbusStatus {
    uint16_t		                        	global[DIM_UINT16_REGISTER_EXTERNSION];
    structModbusRegisterInContiguosMemory   	field;
}typeRegisterExtensions;

extern typeRegisterExtensions PMD2_SendToRemote;
extern typeRegisterExtensions PMD2_ReceiveFromRemote;
extern typeRegisterExtensions PMD2_PreviousReceiveFromRemote;

void clearRegisterExtensionReceiveStructure(void);
void clearRegisterExtensionPreviousReceiveStructure(void);
void clearRegisterExtensionPreviousSendToRemote(void);
void initAllRegisterExtensionStructure(void);
void copySrcToTwiceDest(typeRegisterExtensions *srcStruct, typeRegisterExtensions *dest1_struct, typeRegisterExtensions *dest2_struct);
void checkAndReactToRemoteChanges(void);
//void updateTHTValue(uint16_t thtValue);
#endif /* MODBUS_REGISTER_ESTENSION_H_ */
