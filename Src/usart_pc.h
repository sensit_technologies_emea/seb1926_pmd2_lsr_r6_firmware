#ifndef PROTO_PC_H
#define PROTO_PC_H

#include "stm32l4xx_hal.h"
#include "usart.h"
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart2;

void sendUsart1 ( uint8_t *ptr );
void sendUsart2 ( uint8_t *ptr );
uint8_t check_controller_RTS(void);
void enable_controller_Modbus(void);
void disable_controller_Modbus(void);
void test_PIN_ON(void);
void test_PIN_OFF(void);
#endif

