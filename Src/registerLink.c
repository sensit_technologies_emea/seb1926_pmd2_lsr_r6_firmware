/****************************************
* File Name          : RegisterLink.c
* Author             : Luca Herzog
* Version            : V1.0
* Date               : 10/08/2008
* Description        : Modbus 485
********************************************************************************
********************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include "Modbus.h"
#include "RegisterLink.h"
#include "modbus_extension_register.h"
#include "sensit.h"

#define FORCE_FW_BOOTLOAD 0xA571F432
extern uint32_t FWupgradeCommand;



//uint16_t  linkRegister_Read_03_and_4 ( uint16_t function, uint16_t registerAddress, int16_t *esito )
//{
//    *esito = MODBUS_OK; // 0 = OK code
//	
//		if(function==0x03) {
//			 if ( registerAddress >= PSG_REGISTER_BASE_1 )  {
//				 registerAddress -= PSG_REGISTER_BASE_1;
//			 }
//		}
//		if(function==0x04) {
//			 if ( registerAddress >= PSG_REGISTER_BASE_1 )  {
//				 registerAddress -= PSG_REGISTER_BASE_1;
//			 }
//		}	
//		
//		if ( ( registerAddress >= PSG_REGISTER_BASE ) && ( registerAddress < PSG_REGISTER_BASE+DIM_UINT16_REGISTER_EXTERNSION ) ) {
//        return ( PMD2_SendToRemote.global[registerAddress-PSG_REGISTER_BASE] );
//    } else {
//        *esito = MODBUS_ILLEGAL_DATA_ADDRESS; // 2 = Illegal data address
//    }

//    return ( 0 ); 	
// }




uint16_t  linkRegister_Read_03_and_4 ( uint16_t function, uint16_t registerAddress, int16_t *esito )
{
    *esito = MODBUS_OK; // 0 = OK code
		
		if ( ( registerAddress >= PSG_REGISTER_BASE_START_1 ) && ( registerAddress < PSG_REGISTER_BASE_END_1 ) )
			registerAddress += -PSG_REGISTER_BASE_START_1 +PSG_REGISTER_REAL_BASE_1;
		else if ( ( registerAddress >= PSG_REGISTER_BASE_START_2 ) && ( registerAddress < PSG_REGISTER_BASE_END_2 ) )
			registerAddress += -PSG_REGISTER_BASE_START_2 +PSG_REGISTER_REAL_BASE_2;
		else if ( ( registerAddress >= PSG_REGISTER_BASE_START_3 ) && ( registerAddress < PSG_REGISTER_BASE_END_3 ) )
			registerAddress += -PSG_REGISTER_BASE_START_3 +PSG_REGISTER_REAL_BASE_3;
		else if ( ( registerAddress >= PSG_REGISTER_BASE_START_4 ) && ( registerAddress < PSG_REGISTER_BASE_END_4 ) )
			registerAddress += -PSG_REGISTER_BASE_START_4 +PSG_REGISTER_REAL_BASE_4;
		else if ( ( registerAddress >= PSG_REGISTER_BASE_START_5 ) && ( registerAddress < PSG_REGISTER_BASE_END_5 ) )
			registerAddress += -PSG_REGISTER_BASE_START_5 +PSG_REGISTER_REAL_BASE_5;
		else if ( ( registerAddress >= PSG_REGISTER_BASE_START_6 ) && ( registerAddress < PSG_REGISTER_BASE_END_6 ) )
			registerAddress += -PSG_REGISTER_BASE_START_6 +PSG_REGISTER_REAL_BASE_6;
		else if ( ( registerAddress >= PSG_REGISTER_BASE_START_7 )&& ( registerAddress < PSG_REGISTER_BASE_END_7 ))
			registerAddress += -PSG_REGISTER_BASE_START_7 +PSG_REGISTER_REAL_BASE_7;
    else {
        *esito = MODBUS_ILLEGAL_DATA_ADDRESS; // 2 = Illegal data address
				return 0;
    }
			
		if ( ( registerAddress >= PSG_REGISTER_BASE ) && ( registerAddress < PSG_REGISTER_BASE+DIM_UINT16_REGISTER_EXTERNSION ) ) {
        return ( PMD2_SendToRemote.global[registerAddress-PSG_REGISTER_BASE] );
    } else {
        *esito = MODBUS_ILLEGAL_DATA_ADDRESS; // 2 = Illegal data address
    }

    return ( 0 ); 	
 }


//void  linkRegister_Write_06_16 ( uint16_t function, uint16_t registerAddress, uint16_t registerValue, int16_t *esito)
//{
//    uint16_t test=0;
//    ( void ) test;

//		if(function==0x06) {
//			 if ( registerAddress >= PSG_REGISTER_BASE_1 )  {
//				 registerAddress -= PSG_REGISTER_BASE_1;
//			 }
//		}
//		if(function==0x10) {
//			 if ( registerAddress >= PSG_REGISTER_BASE_1 )  {
//				 registerAddress -= PSG_REGISTER_BASE_1;
//			 }
//		}		

//    *esito = MODBUS_OK; // 0 = OK code
//		
//		if ( ( registerAddress >= PSG_REGISTER_BASE ) && ( registerAddress < PSG_REGISTER_BASE+DIM_UINT16_REGISTER_EXTERNSION ) ) {
//        PMD2_ReceiveFromRemote.global[registerAddress-PSG_REGISTER_BASE]=registerValue;
//    }
//		else {
//        *esito = MODBUS_ILLEGAL_DATA_ADDRESS; // 2 = Illegal data address
//		} 
//}

void  linkRegister_Write_06_16 ( uint16_t function, uint16_t registerAddress, uint16_t registerValue, int16_t *esito)
{
		*esito = MODBUS_OK;
		if ( ( registerAddress >= PSG_REGISTER_BASE_START_1 ) && ( registerAddress < PSG_REGISTER_BASE_END_1 ) )
			registerAddress += -PSG_REGISTER_BASE_START_1 +PSG_REGISTER_REAL_BASE_1;
		else if ( ( registerAddress >= PSG_REGISTER_BASE_START_2 ) && ( registerAddress < PSG_REGISTER_BASE_END_2 ) )
			registerAddress += -PSG_REGISTER_BASE_START_2 +PSG_REGISTER_REAL_BASE_2;
		else if ( ( registerAddress >= PSG_REGISTER_BASE_START_3 ) && ( registerAddress < PSG_REGISTER_BASE_END_3 ) )
			registerAddress += -PSG_REGISTER_BASE_START_3 +PSG_REGISTER_REAL_BASE_3;
		else if ( ( registerAddress >= PSG_REGISTER_BASE_START_4 ) && ( registerAddress < PSG_REGISTER_BASE_END_4 ) )
			registerAddress += -PSG_REGISTER_BASE_START_4 +PSG_REGISTER_REAL_BASE_4;
		else if ( ( registerAddress >= PSG_REGISTER_BASE_START_5 ) && ( registerAddress < PSG_REGISTER_BASE_END_5 ) )
			registerAddress += -PSG_REGISTER_BASE_START_5 +PSG_REGISTER_REAL_BASE_5;
		else if ( ( registerAddress >= PSG_REGISTER_BASE_START_6 ) && ( registerAddress < PSG_REGISTER_BASE_END_6 ) )
			registerAddress += -PSG_REGISTER_BASE_START_6 +PSG_REGISTER_REAL_BASE_6;
		else if ( ( registerAddress >= PSG_REGISTER_BASE_START_7 )&& ( registerAddress < PSG_REGISTER_BASE_END_7 ))
			registerAddress += -PSG_REGISTER_BASE_START_7 +PSG_REGISTER_REAL_BASE_7;
    else {
        *esito = MODBUS_ILLEGAL_DATA_ADDRESS; // 2 = Illegal data address
				registerAddress=0xFFFF;
    }
			
		if ( ( registerAddress >= PSG_REGISTER_BASE ) && ( registerAddress < PSG_REGISTER_BASE+DIM_UINT16_REGISTER_EXTERNSION ) ) {
        PMD2_ReceiveFromRemote.global[registerAddress-PSG_REGISTER_BASE]=registerValue;
    } else {
        *esito = MODBUS_ILLEGAL_DATA_ADDRESS; // 2 = Illegal data address
    } 	
}
