#include <ctype.h>
#include <stdio.h>
#include <string.h>

#include "stm32l4xx_hal.h"
#include "usart_pc.h"
#include "usart.h"
#include "main.h"

void sendUsart1 ( uint8_t *ptr )
{
  HAL_UART_Transmit ( &huart1, ( uint8_t * ) ptr,strlen ( (char *)ptr ),1000 );
}
void sendUsart2 ( uint8_t *ptr )
{
  HAL_UART_Transmit ( &huart2, ( uint8_t * ) ptr,strlen ( (char *)ptr ),1000 );
}

uint8_t check_controller_RTS(void){
	return HAL_GPIO_ReadPin(USART1_CTS_GPIO_Port, USART1_CTS_Pin);
}

void enable_controller_Modbus(void){
	HAL_GPIO_WritePin(USART1_RTS_GPIO_Port, USART1_RTS_Pin, GPIO_PIN_SET);
}

void disable_controller_Modbus(void){
	HAL_GPIO_WritePin(USART1_RTS_GPIO_Port, USART1_RTS_Pin, GPIO_PIN_RESET);
}


void test_PIN_ON(void){
	HAL_GPIO_WritePin(USART1_RTS_GPIO_Port, USART1_RTS_Pin, GPIO_PIN_SET);
}

void test_PIN_OFF(void){
	HAL_GPIO_WritePin(USART1_RTS_GPIO_Port, USART1_RTS_Pin, GPIO_PIN_RESET);
}