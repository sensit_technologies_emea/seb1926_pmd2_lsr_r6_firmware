#ifndef FUNCTIONS_H
#define FUNCTIONS_H

void Init_FPOM_struct(void);

void doMeasureCalculation(unsigned char OU, unsigned char OD);
void SetPompaFlow(char stato);
void Riscaldamento(void);
void stato_heat_on_off(void);

void Rapporto_Gain (void);
void LoadCalibration(void);
void SaveCalibrazioneEEPROM(char cal_num);
void SaveCalibration(unsigned char calibrazione);
void SaveMeasure(void);
void LoadMeasure(unsigned int numero);
void change_gain(void);

#endif
