#include "stm32l4xx_hal.h"
#include "dateEpoch.h"

// Count epoch value from 01.01.2015 (this value represents days JDN of 01.01.2015)
// To user Unix epoch time define '2440588' here
// JDN       0 = Monday, B.C. 4713 Jan 1	12:00:00.0
// JDN 2440588 = Thursday, A.D. 1970 Jan 1	12:00:00.0	
// JDN 2457023 = Wednesday, A.D. 2014 Dec 31	12:00:00.0	
#define JULIAN_DATE_BASE 2457023

RTC_DateTypeDef epoch_sdate;
RTC_TimeTypeDef epoch_stime;
uint32_t epochTimeStamp;

// Convert Date/Time structures to epoch time
// input:
//   time - pointer to the RTC time structure
//   date - pointer to the RTC date structure
// return: 32-bit epoch value (seconds)
uint32_t RTC_ToEpoch(RTC_TimeTypeDef *time, RTC_DateTypeDef *date) {
	uint8_t  a;
	uint16_t y;
	uint8_t  m;
	uint32_t JDN;

	// Calculate some coefficients
	a = (14 - date->Month) / 12;
	y = date->Year + 6800 - a; // years since 1 March, 4801 BC
	m = date->Month + (12 * a) - 3;

	// Compute Julian day number (from Gregorian calendar date)
	JDN  = date->Date;
	JDN += ((153 * m) + 2) / 5; // Number of days since 1 march
	JDN += 365 * y;
	JDN += y / 4;
	JDN -= y / 100;
	JDN += y / 400;
	JDN -= 32045;

	// Subtract number of days passed before base date from Julian day number
	JDN -= JULIAN_DATE_BASE;

	// Convert days to seconds
	JDN *= 86400;

	// Add to epoch specified time in seconds
	JDN += time->Hours * 3600;
	JDN += time->Minutes * 60;
	JDN += time->Seconds;

	// Number of seconds passed since the base date
	return JDN;
}

// Convert epoch time to RTC date/time
// input:
//   epoch - 32-bit epoch value (seconds)
//   time - pointer to the RTC time structure
//   date - pointer to the RTC date structure
void RTC_FromEpoch(uint32_t epoch, RTC_TimeTypeDef *time, RTC_DateTypeDef *date) {
	uint32_t a,b,c,d;

	// Calculate JDN (Julian day number) from a specified epoch value
	a = (epoch / 86400) + JULIAN_DATE_BASE;

	// Day of week
	date->WeekDay = (a % 7) + 1;

	// Calculate intermediate values
	a += 32044;
	b  = ((4 * a) + 3) / 146097;
	a -= (146097 * b) / 4;
	c  = ((4 * a) + 3) / 1461;
	a -= (1461 * c) / 4;
	d  = ((5 * a) + 2) / 153;

	// Date
	date->Date  = a - (((153 * d) + 2) / 5) + 1;
	date->Month = d + 3 - (12 * (d / 10));
	date->Year  = (100 * b) + c - 6800 + (d / 10);

	// Time
	time->Hours   = (epoch / 3600) % 24;
	time->Minutes = (epoch / 60) % 60;
	time->Seconds =  epoch % 60;
}

// Adjust time with time zone offset
// input:
//   time - pointer to RTC_Time structure with time to adjust
//   date - pointer to RTC_Date structure with date to adjust
//   offset - hours offset to add or subtract from date/time (hours)
void RTC_AdjustTimeZone(RTC_TimeTypeDef *time, RTC_DateTypeDef *date, int8_t offset) {
	uint32_t epoch;

	// Convert date/time to epoch
	epoch  = RTC_ToEpoch(time,date);
	// Add or subtract offset in seconds
	epoch += offset * 3600;
	// Convert updated epoch back to date/time
	RTC_FromEpoch(epoch,time,date);
}

// Day Of Week calculation from specified date
// input:
//   date - pointer to RTC_Date structure with date
// return: RTC_WeekDay field of date structure will be modified
// note: works for dates after 1583 A.D.
void RTC_CalcDOW(RTC_DateTypeDef *date) {
	int16_t adjustment,mm,yy;

	// Calculate intermediate values
	adjustment = (14 - date->Month) / 12;
	mm = date->Month + (12 * adjustment) - 2;
	yy = date->Year - adjustment;

	// Calculate day of week (0 = Sunday ... 6 = Saturday)
	date->WeekDay = (date->Date + ((13 * mm - 1) / 5) + yy + (yy / 4) - (yy / 100) + (yy / 400)) % 7;

	// Sunday?
	if (!date->WeekDay) date->WeekDay = 7;
}

#if 0

/**
  * @brief Get current date and time
  * @param[out] date Pointer to a structure representing the date and time
  **/
 
void getCurrentDate(struct tm *date)
{
   //Retrieve current time
   time_t time = getCurrentUnixTime();

   //Convert Unix timestamp to date
   convertUnixTimeToDate(time, date);
}


/**
 * @brief Get current time
 * @return Unix timestamp
 **/

time_t getCurrentUnixTime(void)
{

   //Retrieve current time
   return time(NULL);

}
#endif

/**
 * @brief Convert Unix timestamp to date
 * @param[in] t Unix timestamp
 * @param[out] date Pointer to a structure representing the date and time
 **/

void convertUnixTimeToDate(time_t t, struct tm *date)
{
   uint32_t a;
   uint32_t b;
   uint32_t c;
   uint32_t d;
   uint32_t e;
   uint32_t f;

   //Negative Unix time values are not supported
   if(t < 1)
      t = 0;

   //Clear milliseconds
   //date->milliseconds = 0;

   //Retrieve hours, minutes and seconds
   date->tm_sec = t % 60;
   t /= 60;
   date->tm_min = t % 60;
   t /= 60;
   date->tm_hour = t % 24;
   t /= 24;

   //Convert Unix time to date
   a = (uint32_t) ((4 * t + 102032) / 146097 + 15);
   b = (uint32_t) (t + 2442113 + a - (a / 4));
   c = (20 * b - 2442) / 7305;
   d = b - 365 * c - (c / 4);
   e = d * 1000 / 30601;
   f = d - e * 30 - e * 601 / 1000;

   //January and February are counted as months 13 and 14 of the previous year
   if(e <= 13)
   {
      c -= 4716;
      e -= 1;
   }
   else
   {
      c -= 4715;
      e -= 13;
   }

   //Retrieve year, month and day
   date->tm_year = c;
   date->tm_mon = e;
   date->tm_mday = f;

   //Calculate day of week
   date->tm_wday = computeDayOfWeek(c, e, f);
}


/**
 * @brief Convert date to Unix timestamp
 * @param[in] date Pointer to a structure representing the date and time
 * @return Unix timestamp
 **/

time_t convertDateToUnixTime(const struct tm *date)
{
   uint32_t y;
   uint32_t m;
   uint32_t d;
   uint32_t t;

   //Year
   y = date->tm_year;
   //Month of year
   m = date->tm_mon;
   //Day of month
   d = date->tm_mday;

   //January and February are counted as months 13 and 14 of the previous year
   if(m <= 2)
   {
      m += 12;
      y -= 1;
   }

   //Convert years to days
   t = (365 * y) + (y / 4) - (y / 100) + (y / 400);
   //Convert months to days
   t += (30 * m) + (3 * (m + 1) / 5) + d;
   //Unix time starts on January 1st, 1970
   t -= 719561;
   //Convert days to seconds
   t *= 86400;
   //Add hours, minutes and seconds
   t += (3600 * date->tm_hour) + (60 * date->tm_min) + date->tm_sec;

   //Return Unix time
   return t;
}


/**
 * @brief Calculate day of week
 * @param[in] y Year
 * @param[in] m Month of year (in range 1 to 12)
 * @param[in] d Day of month (in range 1 to 31)
 * @return Day of week (in range 1 to 7)
 **/

uint8_t computeDayOfWeek(uint16_t y, uint8_t m, uint8_t d)
{
   uint32_t h;
   uint32_t j;
   uint32_t k;

   //January and February are counted as months 13 and 14 of the previous year
   if(m <= 2)
   {
      m += 12;
      y -= 1;
   }

   //J is the century
   j = y / 100;
   //K the year of the century
   k = y % 100;

   //Compute H using Zeller's congruence
   h = d + (26 * (m + 1) / 10) + k + (k / 4) + (5 * j) + (j / 4);

   //Return the day of the week
   return ((h + 5) % 7) + 1;
}

uint32_t epochTimeStamp_now(RTC_HandleTypeDef *hrtcEpoch)
{
  /* Get the RTC current Time */
  HAL_RTC_GetTime(hrtcEpoch, &epoch_stime, FORMAT_BIN);
  /* Get the RTC current Date */
  HAL_RTC_GetDate(hrtcEpoch, &epoch_sdate, FORMAT_BIN);   
  epochTimeStamp = RTC_ToEpoch(&epoch_stime, &epoch_sdate);
  return epochTimeStamp;
}
