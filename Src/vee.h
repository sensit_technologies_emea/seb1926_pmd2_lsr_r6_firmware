#ifndef VEE_H
#define VEE_H
#include "stm32l4xx_hal.h"
#include "i2c_quad_dac_AD5696.h"
#include "i2c_dac_AD5693.h"
#include "sensit.h"

extern I2C_HandleTypeDef hi2c1;
extern I2C_HandleTypeDef hi2c2;
extern I2C_HandleTypeDef hi2c3;
uint8_t set_laser_scan_amplitude(float amp);
//uint8_t set_laser_bias_current(float iLas);
//uint8_t set_laser_temperature(float tLas);
//uint8_t set_trigger_level(uint16_t lTrigg);
uint8_t read_laser_temperature(void);
uint8_t read_laser_voltage(void);
uint8_t read_laser_current(void);
uint8_t read_peltier_current(void);
uint8_t read_cell_temperature(void);
//uint8_t set_ad5693_DC_level(uint16_t vDC);
uint16_t convertLaserTemperature(float tLas);
uint8_t set_ad5693_DC_level(uint16_t vDC);
uint8_t set_ad5693_laser_temperature(float tLas);
uint8_t set_ad5693_laser_bias_current(float iLas);
uint8_t set_ad5693_VGA_GAIN(uint16_t gain);
#endif
