#ifndef SENSIT_H
#define SENSIT_H

#include "stdint.h"

#define     FALSE       0
#define     TRUE        1

#define     OFF      0
#define     ON       1

#define     SET      1
#define     MEAS     0

#define     BUTTON_ON      0
#define     BUTTON_OFF     1

#define			MIN				2
#define     LOW      	1
#define     HIGH			0


typedef enum {TEC_ERROR, PUMP_ERROR, EEP_ERROR} error_type;

#define BITBAND_SRAM(address, bit) 	(( uint32_t *)(SRAM1_BB_BASE + (((uint32_t)address) - SRAM1_BASE) * 32 + (bit) * 4))
//#define GET_ADDR_XY(X,Y)			(( uint8_t *)(((uint32_t)&pDataSRAM1[0]) + ((Y*LCD_WIDTH)+X)/8 ))
#define GET_BIT_IN_ADDR_XY(X,Y)	( ((Y*LCD_WIDTH)+X)%8 ) 
 

typedef union {
    uint8_t buf[2];
    uint16_t  uiVar;
} unionTwo;

typedef union {
    uint8_t buf[4];
    uint16_t  uiVar;
    uint32_t 	ulVar;
    int32_t   lVar;
    float     fVar;
    struct {
        uint16_t  wordVar_1;
        uint16_t  wordVar_2;
    } word;
} unionFour;

extern unionFour        unsized_1;
extern unionFour        unsized_2;
typedef union {
    uint8_t buf[8];
    double        dVar;
} unionEight;


//---------------------------------------------------------------------
// Struttura per la definizione della versione del firmware
typedef struct {
    uint16_t  release;        // 2 byte /**< Release */
    uint16_t  versione;       // 2 byte /**< Version */
} typeVersion;

//---------------------------------------------------------------------
// Struttura per la definizione della versione della data di calibrazione
typedef struct {
    uint8_t sysDay;         // 1 byte /**< Day of the month - [1,31] */
    uint8_t sysMonth;       // 1 byte /**< Months since January - [0,11] */
    uint16_t sysYear;         // 2 byte /**< Years */
} typeCalibrationDate;

//---------------------------------------------------------------------
// Struttura per la definizione dell'orologio interno
typedef struct {
    uint8_t sysDay;         // 1 byte /**< Day of the month - [1,31] */
    uint8_t sysMonth;       // 1 byte /**< Months since January - [0,11] */
    uint8_t sysYear;        // 1 byte /**< Years since 1900 */
    uint8_t sysHour;        // 1 byte /**< Hours since midnight - [0,23] */
    uint8_t sysMin;         // 1 byte /**< Minutes after the hour - [0,59] */
    uint8_t sysSec;         // 1 byte /**< Seconds after the minute - [0,59] */
} typeOrologio;

typedef struct{
	uint8_t			ready;
	uint8_t			nV;
  uint16_t  	nStab;
	uint8_t			modbusCH1;
  uint32_t 		adcValueOriginal[512];
  uint32_t 		adcValueElaborated[512];
  uint32_t 		adcValueFinal[512];
	uint16_t 		waveform[1024];
	uint8_t			tmpBuf[256];
}service_struct;


extern service_struct service;
extern volatile int global_triggerRampa;
extern const uint16_t triangleWavePointTable[1024];
extern const uint16_t sawtoothWavePointTable[1024];
/*
typedef struct {
    uint8_t 						state;		// 72 - 1 byte con un flag gestisco tutti i possibili stati , 255 possibili segnalazioni differenti} typeInternalStructure;
    uint32_t 						serialNumber;		// 73 - 4 
    typeVersion  				version;		// 77 - 4 byte
		typeCalibrationDate calDate;
    char 								tmpBuf[ 256 ];			// 85 - 256  /buffer per debug strumento	
    char 								errorFlag;
} RoadRunner_Struct;


typedef struct{
  uint16_t			ID;								//0 - 2 laser ID
	float         temperatureW;			//2 - 4 working laser temperature. The default value is stored in EEPROM but could change during the operation
  float	        currentHSW;				//6 - 4 working laser current. The default value is stored in EEPROM but could change during the operation
  float	        rampAmplitudeHSW;	//10 - 4 amplitude of the scan. The default value is stored in EEPROM but could change during the operation
  float 				rampFrequencyW;		//16 - 4 ramp frequency currently not used
  float	        currentLSW;				//20 - 4 working laser current after jump. The default value is stored in EEPROM but could change during the operation
  float	        rampAmplitudeLSW;	//24 - 4 amplitude of the scan after jump. The default value is stored in EEPROM but could change during the operation
  float	        temperatureR;			//30 - 4 realtime temperature on the NTC, the value is read by the microcontroller ADC
  float	        currentLaserR;		//34 - 4 realtime current on the laser diode, the value is read by the microcontroller ADC
  float	        voltageR;					//38 - 4 realtime voltage on the laser diode, the value is read by the microcontroller ADC
  float	        currentTecR;			//42 - 4 realtime current on the peltier cell, the value is read by the microcontroller ADC
  float	        temperatureCellR;	//46 - 4 realtime temperature on the Herriot Cell, the value is read by the microcontroller ADC
  uint8_t				tecStatus;				//50 - 1 flag for temperature stabilization on/off (1/0), 
  uint8_t				laserStatus;			//51 - 1 flag for laser current on/off(1/0)
}laser_struct;


typedef struct{
  uint16_t  		nAvgW;								//0 - 2 Number of ramp acquired for a single measurements 
  uint8_t  			centerFlag;						//2 - 1	Flag to center the absorption line by changing the laser current
  uint16_t 			nAcquisitionPointsW;	//3 - 2 Number of point acquired for each ramp
  uint16_t 			dCLevelW;							//5 - 2 The value is used to set DAC output to subtract a fixed voltage from the photodiode signal
	uint16_t			amplResistorValue;		//7 - 2 The value is used to set gain for photodiode signal
  float 				acquisitionDelay;			//9 - 4 delay in usec from the start of the ramp to the  start of a new acquisition
  uint8_t  			ready;						
	uint8_t  			rampStatus;
	uint8_t 			switchRange;
	uint8_t 			autoBackground;
	uint8_t 			autoRange;
	uint16_t			zerophd;
}measurement_struct;

typedef struct{
  uint32_t 	timeStamp;					//0 - 4 timeStamp
  float			concValue;					//4 - 4 absolute concentration 
  float    	concValueRel; 			//8 - 4 relative concentration
	float			concMean;
  uint8_t		relAbsFlag;					//12 - 1 relative or absolute visualisation
  uint16_t 	laserState;					//13 - 2 state of the laser board
  uint8_t		lineState;					//15 - 1 in which line is emitting the laser
  uint8_t		sensitivityLevel;   //16 - 1 high or low sensitivity
  float     limite;							//17 - 4 ??????
  uint8_t		release;						//21 - 1 release 
  uint8_t		version;						//22 - 1 version
  uint8_t   errorType;          //23 - 1 version
  float     zero;								//24 - 4 version
	float 		power;
	float 		bit;
	float 		testCellConcentration;
}realTime_struct;


typedef struct{
  uint16_t 	middle_point;
  uint16_t 	start_point;
  uint16_t 	end_point;
  uint8_t  	tipo;
  uint8_t  	nsm;
	uint8_t		sep;
	float 		calibHigh;
	float 		calibLow;
} analysis_struct;
//extern laser_struct laser;
//extern measurement_struct measurement;
//extern realTime_struct realTime;
//extern RoadRunner_Struct RoadRunner;
//extern analysis_struct analysis;

*/
#endif

