#ifndef MODBUS_H_
#define MODBUS_H_

#include <stdint.h>

#define     K_MODBUS_ID                   0x31

#define PSG_REGISTER_BASE                     0
#define PSG_REGISTER_BASE_COIL                0
#define PSG_REGISTER_BASE_DISCRETE_INPUT      10000

#define PSG_REGISTER_BASE_START_1							40000
#define PSG_REGISTER_BASE_START_2							41000
#define PSG_REGISTER_BASE_START_3							42000
#define PSG_REGISTER_BASE_START_4							43000
#define PSG_REGISTER_BASE_START_5							44000
#define PSG_REGISTER_BASE_START_6							45000
#define PSG_REGISTER_BASE_START_7							46000
#define PSG_REGISTER_BASE_END_1								40008+2
#define PSG_REGISTER_BASE_END_2								41019+2
#define PSG_REGISTER_BASE_END_3								42010+2
#define PSG_REGISTER_BASE_END_4								43010+2
#define PSG_REGISTER_BASE_END_5								44017+2
#define PSG_REGISTER_BASE_END_6								45060+2
#define PSG_REGISTER_BASE_END_7								47442+2+34

#define PSG_REGISTER_REAL_BASE_1							1
#define PSG_REGISTER_REAL_BASE_2							9
#define PSG_REGISTER_REAL_BASE_3							28
#define PSG_REGISTER_REAL_BASE_4							38
#define PSG_REGISTER_REAL_BASE_5							48
#define PSG_REGISTER_REAL_BASE_6							65
#define PSG_REGISTER_REAL_BASE_7							125

#define         TIM_BUS_RELEASE_BUS_BEFORE_SEND   1
#define         SIZE_UartModbus_send_buff   250
#define         SIZE_UartModbus_rev_buff    100 
 
#ifndef FALSE
#define     FALSE       0
#endif

#ifndef TRUE
#define     TRUE        1
#endif

// Exception Codes
#define         MODBUS_OK                       0x00
#define         MODBUS_ILLEGAL_FUNCTION         0x01
#define         MODBUS_ILLEGAL_DATA_ADDRESS     0x02
#define         MODBUS_ILLEGAL_DATA_VALUE       0x03
#define         MODBUS_SLAVE_DEVICE_FAILURE     0x04
#define         MODBUS_ACKNOWLEDGE              0x05
#define         MODBUS_SLAVE_DEVICE_BUSY        0x06
#define         MODBUS_NEGATIVE_ACKNOWLEDGE     0x07
#define         MODBUS_MEMORY_PARITY_ERROR      0x08

#define         MODBUS_TIMEOUT_LAST_WRITE       500

typedef union {
    struct {
        uint16_t Modbus_ON:1;
        uint16_t Rx_end:1;
    } BIT;
    uint16_t ALL;
} typeModbusStatus;



extern void ( *Modbus_Task_CH1_Ptr ) ( void );
extern void ( *Modbus_Task_CH1_Ptr ) ( void );

void changeModbus_ID_CH1(uint16_t newID);
void Modbus_Init_CH1 ( void );
void Modbus_Function_1_and_2_CH1 ( void );
void Modbus_Function_3_and_4_CH1 ( void );
void Modbus_Function_5_CH1 ( void );
void Modbus_Function_6_CH1 ( void );
void Modbus_Function_7_CH2 ( void );
void Modbus_Function_16_CH1 ( void );
void Modbus_ExceptionResponses_CH1 ( uint16_t functionCode, uint16_t exceptionCode );
//void Modbus_LED_STATUS_ON ( uint16_t timLed );
//void Modbus_LED_ERROR_ON ( uint16_t timLed );
void Delay ( volatile uint32_t nCount );
int16_t Modbus_receive_CH1 ( uint8_t ReceivedChar );
void Modbus_Timer_EndRX_CH1 ( void );
void Modbus_Timer_update_CH1 ( void );
void Modbus_TimerInit_CH1 ( uint16_t value );

void Modbus_Task_CH1 ( void );
void Modbus_Task_Listen_CH1 ( void );
void Modbus_Task_Send_CH1 ( void );
void Modbus_Task_ReleaseBUS_CH1 ( void );
void Modbus_Task_StartSendSession_CH1 ( void );

void changeModbus_ID_CH2(uint16_t newID);
void Modbus_Init_CH2 ( void );
void Modbus_Function_1_and_2_CH2 ( void );
void Modbus_Function_3_and_4_CH2 ( void );
void Modbus_Function_5_CH2 ( void );
void Modbus_Function_6_CH2 ( void );
void Modbus_Function_16_CH2 ( void );
void Modbus_ExceptionResponses_CH2 ( uint16_t functionCode, uint16_t exceptionCode );
//void Modbus_LED_STATUS_ON ( uint16_t timLed );
//void Modbus_LED_ERROR_ON ( uint16_t timLed );
void Delay ( volatile uint32_t nCount );
int16_t Modbus_receive_CH2 ( uint8_t ReceivedChar );
void Modbus_Timer_EndRX_CH2 ( void );
void Modbus_Timer_update_CH2 ( void );
void Modbus_TimerInit_CH2 ( uint16_t value );

void Modbus_Task_CH2 ( void );
void Modbus_Task_Listen_CH2 ( void );
void Modbus_Task_Send_CH2 ( void );
void Modbus_Task_ReleaseBUS_CH2 ( void );
void Modbus_Task_StartSendSession_CH2 ( void );

#endif /* MODBUS_H_ */
