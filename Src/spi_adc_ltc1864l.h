#ifndef __SPI_ADC_LTC1864_H
#define __SPI_ADC_LTC1864_H

#include "stm32l4xx_hal.h"
#
// Define DB7:DB4 of Command byte
//

HAL_StatusTypeDef ltc1864l_get_value(uint16_t *value);
void read_rampa(uint32_t *pvalue, uint16_t n_samples, uint8_t n_mean);
void wait_for_trigger_counter(void);
void wait_for_trigger_micro(void);
void read_rampa_DC(uint32_t *pvalue, uint16_t n_samples, uint16_t n_mean);
void read_rampa_SD(uint32_t *pvalue, uint16_t n_samples, uint16_t n_mean);
void fastLtc1864l_get_value( uint32_t *temp, uint16_t n_samples);
void fastLtc1864l_get_valueSD( uint32_t *temp, uint16_t n_samples);
void postProcessingLtc1864l_get_value(void);

#endif

