#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include "stm32l4xx_hal.h"
#include "main.h"
#include "modbus.h"
#include "registerLink.h"
#include "crc16Modbus.h"
#include "sensit.h"
#include "modbus_extension_register.h"
#include "ringbufferdma.h"
#include "eeprom_address.h"
#include "functions.h"


//extern UART_HandleTypeDef huart4;
//extern UART_HandleTypeDef huart3;

extern UART_HandleTypeDef huart1;
void ( *Modbus_Task_CH1_Ptr ) ( void );  // State pointer B branch


#define     K_SETTINGS_MODBUSCH1_TIMOUT                         (5L*60L*1000L)      // 2000 x 150ms = 300 secondi = 5 minuti
#define			RING_BUFFER_SIZE		64
uint16_t UartModbus_rev_buff_CH1[SIZE_UartModbus_rev_buff];
uint16_t UartModbus_send_buff_CH1[SIZE_UartModbus_send_buff];

volatile uint16_t UartModbus_rev_count_CH1;
volatile uint16_t UartModbus_send_counter_CH1 = 0;

volatile uint16_t Nchr_Modbus_pckt_CH1 = 0;
volatile uint16_t *UartModbus_send_pointer_CH1 = UartModbus_send_buff_CH1;
uint16_t Modbus_Task_Send_idxTrsmtChars_CH1=0;

uint32_t   cntModbusFun01CH1=0;
uint32_t   cntModbusFun03CH1=0;
uint32_t   cntModbusFun05CH1=0;
uint32_t   cntModbusFun06CH1=0;
uint32_t   cntModbusFun15CH1=0;
uint32_t   cntModbusFun16CH1=0;
uint32_t   cntModbusFunUnknownCH1=0;
uint32_t   cntModbusOKCH1=0;
uint32_t   cntNotForMeCH1=0;
uint32_t   cntModbusERRCH1=0 ;
uint16_t   timModbusCH1=0;
//uint16_t   timModbusLED_STATUS=0;
//uint16_t   timModbusLED_ERROR=0;
uint32_t   timLastModbusOKCH1=0;
uint32_t   timLastModbusOKforMeCH1=0;
uint32_t   timModbusTimeOutCH1=0L; // 30000L;
uint32_t   timModbusWriteOccouredCH1=0L;
int16_t    flgModbusWriteOccouredCH1=FALSE;

typeModbusStatus    ModbusStatusCH1;
uint8_t 						modbusRxBufferCH1[RING_BUFFER_SIZE];
RingBufferDmaU8 		ringModbusCH1; 
int16_t    					flgModbusActivityCH1=FALSE;




void changeModbus_ID_CH1(uint16_t newID)
{
	if( (newID>0) && (newID<=247) )
		PMD2_SendToRemote.field.system.modbusID= newID;
}
void Modbus_Init_CH1 ( void )
{
    int16_t i;
    UartModbus_rev_count_CH1 = 0;
    UartModbus_send_counter_CH1 = 0;
    ModbusStatusCH1.BIT.Modbus_ON = 0;
    ModbusStatusCH1.BIT.Rx_end = 0;
    Modbus_Task_CH1_Ptr = &Modbus_Task_Listen_CH1;

//    PMD2_SendToRemote.field.system.modbusID= K_MODBUS_ID;
		timModbusTimeOutCH1 = K_SETTINGS_MODBUSCH1_TIMOUT;
    Modbus_TimerInit_CH1 ( 0 );

    for ( i=0; i<SIZE_UartModbus_rev_buff; ++i )
        UartModbus_rev_buff_CH1[i]=0;
    for ( i=0; i<SIZE_UartModbus_send_buff; ++i )
        UartModbus_send_buff_CH1[i]=0;

    initAllRegisterExtensionStructure();
	 
    RingBufferDmaU8_initUSARTRx ( &ringModbusCH1, &huart1, modbusRxBufferCH1, RING_BUFFER_SIZE );
}



// Function 1 - Read Coil Status 
// Request packet:
// byte 0 - Slave Address 
// byte 1 - Function Code 1 (read Coil Status)
// byte 2 - HIGH BYTE Data Address of the first coil to read
// byte 3 - LOW BYTE Data Address of the first coil to read
// byte 4 - HIGH BYTE total number of coils to read
// byte 5 - LOW BYTE total number of coils to read
// byte 6 - HIGH BYTE CRC
// byte 7 - LOW BYTE CRC
//
void Modbus_Function_1_and_2_CH1 ( void )
{
    uint16_t    numCoils;
    uint16_t    idxSendBuf;
    uint16_t    crcresult;
	  uint16_t    cntCoils;
	  uint16_t    numBitInByte;
    uint16_t    addressFirstCoil;
		uint16_t		coilFunctionNumber;
	
    ++cntModbusFun01CH1;

		//volatile uint8_t * ptrODR_SendToRemote = (uint8_t *) &ODR_SendToRemote.field.FLAG;
		volatile uint32_t * bitBand_ptrCoil;
	
    addressFirstCoil = ( uint16_t ) ( UartModbus_rev_buff_CH1[2] << 8 ) + ( uint16_t ) UartModbus_rev_buff_CH1[3];
    numCoils = ( uint16_t ) ( UartModbus_rev_buff_CH1[4] << 8 ) + ( uint16_t ) UartModbus_rev_buff_CH1[5];			
    bitBand_ptrCoil = BITBAND_SRAM((uint32_t *) (&PMD2_SendToRemote.field.flag)  , 0);

	bitBand_ptrCoil += (uint32_t )((uint32_t )addressFirstCoil-(uint32_t )PSG_REGISTER_BASE_COIL);

	if ( (addressFirstCoil<PSG_REGISTER_BASE_COIL) || ((addressFirstCoil+numCoils)>(PSG_REGISTER_BASE_COIL+2000)) ) {
        Modbus_ExceptionResponses_CH1 ( ( uint16_t ) UartModbus_rev_buff_CH1[1], MODBUS_ILLEGAL_DATA_ADDRESS );
        return;
			}

		
    UartModbus_send_buff_CH1[0] = PMD2_SendToRemote.field.system.modbusID;
    UartModbus_send_buff_CH1[1] = ( uint16_t ) UartModbus_rev_buff_CH1[1]; //0x01 or 0x02;
    UartModbus_send_buff_CH1[2] = numCoils/8+1; //Number of byte (coils / 8)
    UartModbus_send_counter_CH1 = 3 + UartModbus_send_buff_CH1[2]; // Total answer length
    idxSendBuf = 3; // Indice nel send buffer primo dato richiesto
		numBitInByte=0;
		for(cntCoils=0; cntCoils<numCoils; ++cntCoils) {
			if(numBitInByte==0)
				  UartModbus_send_buff_CH1[idxSendBuf]=0;
			UartModbus_send_buff_CH1[idxSendBuf] |= ((*bitBand_ptrCoil!=0)?(1<<numBitInByte):0);  // Coil value
			bitBand_ptrCoil++;
			numBitInByte++;
			if(numBitInByte==8) {
				numBitInByte=0;
				idxSendBuf++;
			}
		}
    crcresult = GetCRC16 ( UartModbus_send_buff_CH1,UartModbus_send_counter_CH1 );
    UartModbus_send_buff_CH1[UartModbus_send_counter_CH1]   = ( uint16_t ) ( crcresult & 0xff );
    UartModbus_send_buff_CH1[UartModbus_send_counter_CH1+1] = ( uint16_t ) ( ( crcresult >> 8 ) & 0xff );
    UartModbus_send_counter_CH1 = ( uint16_t ) ( UartModbus_send_counter_CH1+2 );
    UartModbus_send_pointer_CH1 = UartModbus_send_buff_CH1;
}


/*******************************************************************************/
void Modbus_Function_3_and_4_CH1 ( void )
{
    uint16_t    variabile;
    uint16_t    tempdress;
    uint16_t    numRegisters;
    uint16_t    idxSendBuf;
    uint16_t    crcresult;
    int16_t             registerExist;

    ++cntModbusFun03CH1;

    tempdress = ( uint16_t ) ( UartModbus_rev_buff_CH1[2] << 8 ) + ( uint16_t ) UartModbus_rev_buff_CH1[3];
    numRegisters = ( uint16_t ) ( UartModbus_rev_buff_CH1[4] << 8 ) + ( uint16_t ) UartModbus_rev_buff_CH1[5];

    if ( ( numRegisters*2 ) > ( SIZE_UartModbus_send_buff - 5 ) ) {
        Modbus_ExceptionResponses_CH1 ( ( uint16_t ) ( UartModbus_rev_buff_CH1[1] ), MODBUS_SLAVE_DEVICE_FAILURE );
        return; // Errore - dimensione buffer richiesto per esaudire richiesta superiore a dim max
    }

    UartModbus_send_buff_CH1[0] = PMD2_SendToRemote.field.system.modbusID;
    UartModbus_send_buff_CH1[1] = ( uint16_t ) UartModbus_rev_buff_CH1[1];  // Function number, can be 3 or 4
    UartModbus_send_buff_CH1[2] = ( uint16_t ) ( 2 * UartModbus_rev_buff_CH1[5] );
    UartModbus_send_counter_CH1 = ( uint16_t ) ( 2 * UartModbus_rev_buff_CH1[5] + 3 );

    idxSendBuf = 3; // Indice nel send buffer primo dato richiesto
    
		for ( ; numRegisters>0; --numRegisters ) {

				variabile = linkRegister_Read_03_and_4 ( UartModbus_rev_buff_CH1[1], tempdress, &registerExist );
				if ( registerExist != 0 ) {
						Modbus_ExceptionResponses_CH1 ( ( uint16_t ) UartModbus_rev_buff_CH1[1], registerExist );
						return;
				}

				UartModbus_send_buff_CH1[idxSendBuf] = ( uint16_t ) ( ( variabile >> 8 ) & 0xff );
				idxSendBuf++;
				UartModbus_send_buff_CH1[idxSendBuf] = ( uint16_t ) ( variabile & 0xff );
				idxSendBuf++;
				++tempdress;  //LH 4sep18
		} 
 

    crcresult = GetCRC16 ( UartModbus_send_buff_CH1,UartModbus_send_counter_CH1 );

    UartModbus_send_buff_CH1[UartModbus_send_counter_CH1]   = ( uint16_t ) ( crcresult & 0xff );
    UartModbus_send_buff_CH1[UartModbus_send_counter_CH1+1] = ( uint16_t ) ( ( crcresult >> 8 ) & 0xff );

    UartModbus_send_counter_CH1 = ( uint16_t ) ( UartModbus_send_counter_CH1+2 );
    UartModbus_send_pointer_CH1 = UartModbus_send_buff_CH1;
}

/*******************************************************************************/
// Function 5 - Force Single Coil
void Modbus_Function_5_CH1 ( void )
{
    uint16_t    coilValue  = 0;
    uint16_t    coilAddress = 0;
    uint16_t    crcresult;

		volatile uint32_t * bitBand_ptrCoil;

    ++cntModbusFun05CH1;

    // Register address
    coilAddress = ( uint16_t ) ( UartModbus_rev_buff_CH1[2]<<8 ) + ( uint16_t ) UartModbus_rev_buff_CH1[3];
		bitBand_ptrCoil = BITBAND_SRAM((uint32_t *) (&PMD2_ReceiveFromRemote.field.flag)  , 0);
    bitBand_ptrCoil += (uint32_t )((uint32_t )coilAddress-(uint32_t )PSG_REGISTER_BASE_COIL);

    // Register value
    // if the master wants reset coils then variabile = 0x0000
    // otherwise, if wants set coils then variable = 0xFF00

    coilValue = ( uint16_t ) ( UartModbus_rev_buff_CH1[4]<<8 ) + ( uint16_t ) UartModbus_rev_buff_CH1[5];
    if ( ( coilValue != 0x0000 ) && ( coilValue != 0xFF00 ) ) {
        Modbus_ExceptionResponses_CH1 ( ( uint16_t ) UartModbus_rev_buff_CH1[1], MODBUS_ILLEGAL_DATA_VALUE );
        return;
    }

    // Coil address validation
    if ( (coilAddress>=PSG_REGISTER_BASE_COIL) && (coilAddress<=(PSG_REGISTER_BASE_COIL+2000/8)) ) {
				*bitBand_ptrCoil = (coilValue==0?0:1);
    } else {
        Modbus_ExceptionResponses_CH1 ( ( uint16_t ) UartModbus_rev_buff_CH1[1], MODBUS_ILLEGAL_DATA_ADDRESS );
        return;
    }

    UartModbus_send_buff_CH1[0] = PMD2_SendToRemote.field.system.modbusID;
    UartModbus_send_buff_CH1[1] = 0x05;
    UartModbus_send_buff_CH1[2] = UartModbus_rev_buff_CH1[2];
    UartModbus_send_buff_CH1[3] = UartModbus_rev_buff_CH1[3];
    UartModbus_send_buff_CH1[4] = UartModbus_rev_buff_CH1[4];
    UartModbus_send_buff_CH1[5] = UartModbus_rev_buff_CH1[5];
    UartModbus_send_counter_CH1 = 6;

    crcresult = GetCRC16 ( UartModbus_send_buff_CH1,UartModbus_send_counter_CH1 );

    UartModbus_send_buff_CH1[UartModbus_send_counter_CH1] = ( uint16_t ) ( crcresult & 0xff );
    UartModbus_send_buff_CH1[UartModbus_send_counter_CH1+1] = ( uint16_t ) ( ( crcresult >> 8 ) & 0xff );

    UartModbus_send_counter_CH1 = ( uint16_t ) ( UartModbus_send_counter_CH1+2 );
    UartModbus_send_pointer_CH1 = UartModbus_send_buff_CH1;

    flgModbusWriteOccouredCH1=TRUE;
}


/******************************************************************************/
void Modbus_Function_6_CH1 ( void )
{
    uint16_t    variabile = 0;
    uint16_t    tempdress = 0;
    uint16_t    crcresult;
    int16_t         registerExist;

    ++cntModbusFun06CH1;

    // Register address
    tempdress = ( uint16_t ) ( UartModbus_rev_buff_CH1[2]<<8 ) + ( uint16_t ) UartModbus_rev_buff_CH1[3];

    // Register value
    variabile = ( uint16_t ) ( UartModbus_rev_buff_CH1[4]<<8 ) + ( uint16_t ) UartModbus_rev_buff_CH1[5];

    linkRegister_Write_06_16 ( UartModbus_rev_buff_CH1[1], tempdress, variabile, &registerExist );
    if ( registerExist != MODBUS_OK ) {
        Modbus_ExceptionResponses_CH1 ( ( uint16_t ) UartModbus_rev_buff_CH1[1], registerExist );
        return;
    }else{
				flgModbusWriteOccouredCH1=TRUE;
				timModbusWriteOccouredCH1=MODBUS_TIMEOUT_LAST_WRITE;
			}
    // Se l'indirizzo della variabile non � incluso nel range ammesso cosa si fa?
    // Disattivo il driver TX RS485_RX_ENABLE;
    UartModbus_send_buff_CH1[0] = PMD2_SendToRemote.field.system.modbusID;
    UartModbus_send_buff_CH1[1] = 0x06;
    UartModbus_send_buff_CH1[2] = UartModbus_rev_buff_CH1[2];
    UartModbus_send_buff_CH1[3] = UartModbus_rev_buff_CH1[3];
    UartModbus_send_buff_CH1[4] = UartModbus_rev_buff_CH1[4];
    UartModbus_send_buff_CH1[5] = UartModbus_rev_buff_CH1[5];
    UartModbus_send_counter_CH1 = 6;

    crcresult = GetCRC16 ( UartModbus_send_buff_CH1,UartModbus_send_counter_CH1 );

    UartModbus_send_buff_CH1[UartModbus_send_counter_CH1] = ( uint16_t ) ( crcresult & 0xff );
    UartModbus_send_buff_CH1[UartModbus_send_counter_CH1+1] = ( uint16_t ) ( ( crcresult >> 8 ) & 0xff );

    UartModbus_send_counter_CH1 = ( uint16_t ) ( UartModbus_send_counter_CH1+2 );
    UartModbus_send_pointer_CH1 = UartModbus_send_buff_CH1;
}


// Function 15 - Force Multiple Coils

void Modbus_Function_15_CH1 ( void )
{
    uint16_t    addressFirstCoil;
    uint16_t    numCoils;
    uint16_t    idxGetBuf;
    uint16_t    crcresult;
	  uint16_t    cntCoils;
	  uint16_t    numBitInByte;
	
    ++cntModbusFun15CH1;

	//	volatile uint8_t * ptrODR_SendToRemote = (uint8_t *) &ODR_SendToRemote.field.FLAG;
		volatile uint32_t * bitBand_ptrCoil;
	
	
    addressFirstCoil = ( uint16_t ) ( UartModbus_rev_buff_CH1[2] << 8 ) + ( uint16_t ) UartModbus_rev_buff_CH1[3];
		bitBand_ptrCoil = BITBAND_SRAM((uint32_t *) (&PMD2_SendToRemote.field.flag + ((addressFirstCoil-PSG_REGISTER_BASE_COIL)/8)), (addressFirstCoil-PSG_REGISTER_BASE_COIL)%8);
	
    numCoils = ( uint16_t ) ( UartModbus_rev_buff_CH1[4] << 8 ) + ( uint16_t ) UartModbus_rev_buff_CH1[5];
    if ( numCoils > 2000 ) {
        Modbus_ExceptionResponses_CH1 ( ( uint16_t ) UartModbus_rev_buff_CH1[1], MODBUS_ILLEGAL_DATA_VALUE);
        return;
    }
		
    if ( (addressFirstCoil<PSG_REGISTER_BASE_COIL) || ((addressFirstCoil+numCoils)>(PSG_REGISTER_BASE_COIL+2000)) ) {
        Modbus_ExceptionResponses_CH1 ( ( uint16_t ) UartModbus_rev_buff_CH1[1], MODBUS_ILLEGAL_DATA_ADDRESS );
        return;
    }
		
    idxGetBuf = 7; // Indice nel send buffer primo dato richiesto
		numBitInByte=0;
		for(cntCoils=0; cntCoils<numCoils; ++cntCoils) {
			if(UartModbus_rev_buff_CH1[idxGetBuf] & (1<<numBitInByte)) {
				*bitBand_ptrCoil = 1;
			} else {
				*bitBand_ptrCoil = 0;
			}
			++bitBand_ptrCoil;
			++numBitInByte;
			if(numBitInByte==8) {
				numBitInByte=0;
				idxGetBuf++;
			}
		}
		
    UartModbus_send_buff_CH1[0] = PMD2_SendToRemote.field.system.modbusID;
    UartModbus_send_buff_CH1[1] = 0x01;
    UartModbus_send_buff_CH1[2] =  UartModbus_rev_buff_CH1[2];
    UartModbus_send_buff_CH1[3] =  UartModbus_rev_buff_CH1[3];
    UartModbus_send_buff_CH1[4] =  UartModbus_rev_buff_CH1[4];
    UartModbus_send_buff_CH1[5] =  UartModbus_rev_buff_CH1[5];
		
    UartModbus_send_counter_CH1 = 6; // Total answer length
				
    crcresult = GetCRC16 ( UartModbus_send_buff_CH1,UartModbus_send_counter_CH1 );
    UartModbus_send_buff_CH1[UartModbus_send_counter_CH1]   = ( uint16_t ) ( crcresult & 0xff );
    UartModbus_send_buff_CH1[UartModbus_send_counter_CH1+1] = ( uint16_t ) ( ( crcresult >> 8 ) & 0xff );
    UartModbus_send_counter_CH1 = ( uint16_t ) ( UartModbus_send_counter_CH1+2 );
    UartModbus_send_pointer_CH1 = UartModbus_send_buff_CH1;
} 
 

/******************************************************************************/
void Modbus_Function_16_CH1 ( void )
{
    uint16_t    startingAddress;
    uint16_t    byteCount=0;
    uint16_t    quantityOfRegisters;
    uint16_t    crcresult;
    uint16_t    value;
    uint16_t    idxBuf; // Indice nel send buffer primo dato richiesto

    int16_t     registerExist;

    ( void ) byteCount;

    ++cntModbusFun16CH1;

    // Starting address (2 word)
    startingAddress = ( uint16_t ) ( UartModbus_rev_buff_CH1[2]<<8 ) + ( uint16_t ) UartModbus_rev_buff_CH1[3];

    // Quantity of registers (2 word)
    quantityOfRegisters = ( uint16_t ) ( UartModbus_rev_buff_CH1[4]<<8 ) + ( uint16_t ) UartModbus_rev_buff_CH1[5];

    // Byte count (1 byte)
    byteCount = ( uint16_t ) UartModbus_rev_buff_CH1[6];

    if ( ( quantityOfRegisters*2 ) > ( SIZE_UartModbus_send_buff - 5 ) ) {
        Modbus_ExceptionResponses_CH1 ( ( uint16_t ) ( UartModbus_rev_buff_CH1[1] ), MODBUS_SLAVE_DEVICE_FAILURE );
        return; // Errore - dimensione buffer richiesto per esaudire richiesta superiore a dim max
    }


    // Se l'indirizzo della variabile non � incluso nel range ammesso cosa si fa?
    // Disattivo il driver TX RS485_RX_ENABLE;
    UartModbus_send_buff_CH1[0] = PMD2_SendToRemote.field.system.modbusID;
    UartModbus_send_buff_CH1[1] = 0x10;
    UartModbus_send_buff_CH1[2] = UartModbus_rev_buff_CH1[2];
    UartModbus_send_buff_CH1[3] = UartModbus_rev_buff_CH1[3];
    UartModbus_send_buff_CH1[4] = UartModbus_rev_buff_CH1[4];
    UartModbus_send_buff_CH1[5] = UartModbus_rev_buff_CH1[5];
    UartModbus_send_counter_CH1 = 6;

    // Prima posizione contenente valori per i registri
    idxBuf = 7;

    for ( ; quantityOfRegisters>0; --quantityOfRegisters ) {
			value = ( uint16_t ) ( UartModbus_rev_buff_CH1[idxBuf]<<8 ) + ( uint16_t ) UartModbus_rev_buff_CH1[idxBuf+1];
			idxBuf += 2;
			linkRegister_Write_06_16 ( UartModbus_rev_buff_CH1[1], startingAddress, value, &registerExist );
			if ( registerExist != MODBUS_OK ) {
				Modbus_ExceptionResponses_CH1 ( 0x10, registerExist );
				return;
			}else{
				flgModbusWriteOccouredCH1=TRUE;
				timModbusWriteOccouredCH1=MODBUS_TIMEOUT_LAST_WRITE;
			}
			++startingAddress;
    }

    crcresult = GetCRC16 ( UartModbus_send_buff_CH1,UartModbus_send_counter_CH1 );

    UartModbus_send_buff_CH1[UartModbus_send_counter_CH1] = ( uint16_t ) ( crcresult & 0xff );
    UartModbus_send_buff_CH1[UartModbus_send_counter_CH1+1] = ( uint16_t ) ( ( crcresult >> 8 ) & 0xff );

    UartModbus_send_counter_CH1 = ( uint16_t ) ( UartModbus_send_counter_CH1+2 );
    UartModbus_send_pointer_CH1 = UartModbus_send_buff_CH1;
}

/******************************************************************************/
void Modbus_ExceptionResponses_CH1 ( uint16_t functionCode, uint16_t exceptionCode )
{
    uint16_t    crcresult;

    UartModbus_send_buff_CH1[0] = PMD2_SendToRemote.field.system.modbusID;
    UartModbus_send_buff_CH1[1] = ( functionCode | 0x80 ) & 0xFF;
    UartModbus_send_buff_CH1[2] = exceptionCode;
    UartModbus_send_counter_CH1 = 3;

    crcresult = GetCRC16 ( UartModbus_send_buff_CH1,UartModbus_send_counter_CH1 );

    UartModbus_send_buff_CH1[UartModbus_send_counter_CH1] = ( uint16_t ) ( crcresult & 0xff );
    UartModbus_send_buff_CH1[UartModbus_send_counter_CH1+1] = ( uint16_t ) ( ( crcresult >> 8 ) & 0xff );

    UartModbus_send_counter_CH1 = ( uint16_t ) ( UartModbus_send_counter_CH1+2 );
    UartModbus_send_pointer_CH1 = UartModbus_send_buff_CH1;

//    Modbus_LED_ERROR_ON ( 2000 );
}



/*******************************************************************************
* Function Name  : Delay
* Description    : Inserts a delay time.
* Input          : nCount: specifies the delay time length.
* Output         : None
* Return         : None
*******************************************************************************/
void Delay_CH1 ( volatile uint32_t nCount )
{
    for ( ; nCount != 0; nCount-- );
}

int16_t Modbus_receive_CH1 ( uint8_t ReceivedChar )
{

    //while ( HAL_UART_Receive ( &huart1, &ReceivedChar, 1, 0 /*HAL_MAX_DELAY*/ ) == HAL_OK ) {
    Modbus_TimerInit_CH1 ( 4 ); // LH 17aug16

    if ( ModbusStatusCH1.BIT.Rx_end != 1 ) {
        if ( UartModbus_rev_count_CH1 < SIZE_UartModbus_rev_buff ) {

            UartModbus_rev_buff_CH1[UartModbus_rev_count_CH1++]= ReceivedChar;

            Modbus_TimerInit_CH1 ( 4 ); // ritardo di 2ms per fine ricezione pacchetto

        }
    }
    //}

    return ( 0 );
}

/*******************************************************************************
* Function Name  : ModbusTimer_IRQHandler
* Description    : This function handles TIMX global interrupt request.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void Modbus_Timer_EndRX_CH1( void )
{
    /* Disabilita timer modbus */

    //if ( ( timModbus == 0 ) && ( UartModbus_rev_count >= 3  ) ) {
	 // il valore era 3 ma e stats messo a 1 perche.. ognitanto non si azzerava
		if ( ( timModbusCH1 == 0 ) && ( UartModbus_rev_count_CH1 >= 1  ) ) {
        ModbusStatusCH1.BIT.Rx_end = 1;
        Nchr_Modbus_pckt_CH1 = UartModbus_rev_count_CH1;
        UartModbus_rev_count_CH1 = 0;
        timModbusCH1 = 0xFFFF;
    }
}

//void Modbus_LED_STATUS_ON ( uint16_t timLed )
//{
//    LED_485A_STATUS_ON;
//    timModbusLED_STATUS = timLed;
//}

//void Modbus_LED_ERROR_ON ( uint16_t timLed )
//{
//    LED_485A_ERROR_ON;
//    timModbusLED_ERROR = timLed;
//}

void Modbus_Timer_update_CH1 ( void )
{

    /*static uint32_t cnt=0;

    ++cnt;
    if(((cnt % 50)==0) || ((timModbus!=0xffff) && (timModbus!=0)) ){
        sprintf( S.unionRom.romStruct.buffer, " %du:%d ",timModbus, UartModbus_rev_count);
        //pnext1( S.unionRom.romStruct.buffer );

    }*/

    if ( flgModbusWriteOccouredCH1 == TRUE ) {
        if ( timModbusWriteOccouredCH1 > 0 ) {
            --timModbusWriteOccouredCH1;
        }
    }
		

    /* Disabilita timer modbus */
//    if ( timModbusLED_STATUS > 0 ) {
//        --timModbusLED_STATUS;
//    } else {
//        LED_485A_STATUS_OFF;
//    }
//    if ( timModbusLED_ERROR > 0 ) {
//        --timModbusLED_ERROR;
//    } else {
//        LED_485A_ERROR_OFF;
//    }

    if ( timLastModbusOKCH1 != 0xFFFFFFFF )
        ++timLastModbusOKCH1;

    if ( timModbusTimeOutCH1 != 0L ) {
        if ( timLastModbusOKforMeCH1 != 0xFFFFFFFF )
            ++timLastModbusOKforMeCH1;

        if ( timLastModbusOKforMeCH1 > timModbusTimeOutCH1 )
            ModbusStatusCH1.BIT.Modbus_ON = 0;
    }

    if ( timModbusCH1 == 0xFFFF )
        return;

    if ( timModbusCH1>0 )
        --timModbusCH1;
}

void Modbus_TimerInit_CH1 ( uint16_t value )
{
    timModbusCH1 = value;
}



//=================================================================================
//  B - TASKS (executed in every 750 usec)
//             LH measured 750 usec
//=================================================================================
void Modbus_Task_CH1 ( void )
{
    uint8_t theRing;

    uint8_t almostOneChar=FALSE;


    if ( RingBufferDmaU8_available ( &ringModbusCH1) && !almostOneChar ) {
        theRing = RingBufferDmaU8_read ( &ringModbusCH1 );
        almostOneChar=TRUE;
        flgModbusActivityCH1=TRUE;
    }		

		
    if ( almostOneChar ) {
        ( void ) Modbus_receive_CH1 ( theRing );
        almostOneChar=FALSE;
    }
    Modbus_Timer_update_CH1();

    Modbus_Timer_EndRX_CH1();

    //-----------------------------------------------------------
    ( *Modbus_Task_CH1_Ptr ) ();     // jump to a B Task (B1,B2,B3,...)
    //-----------------------------------------------------------
    //}

    if ( flgModbusWriteOccouredCH1==TRUE ) {
        if ( timModbusWriteOccouredCH1==0 ) {
            flgModbusWriteOccouredCH1=FALSE;
            checkAndReactToRemoteChanges();
        }
    }


}
//----------------------------------- USER ----------------------------------------

//----------------------------------------
void Modbus_Task_Listen_CH1 ( void )
//----------------------------------------
{
		uint16_t temp[2];
    uint16_t i;
    uint16_t    crcresult;
	
    if ( ModbusStatusCH1.BIT.Rx_end == 1 ) {

        if ( UartModbus_rev_buff_CH1[0] == PMD2_SendToRemote.field.system.modbusID || UartModbus_rev_buff_CH1[0] == K_MODBUS_ID) {

            crcresult = GetCRC16 ( UartModbus_rev_buff_CH1, ( uint16_t ) ( Nchr_Modbus_pckt_CH1-2 ) );
            temp[1] = ( uint16_t ) ( crcresult & 0xff );
            temp[0] = ( uint16_t ) ( ( crcresult >> 8 ) & 0xff );
            if ( ( UartModbus_rev_buff_CH1[Nchr_Modbus_pckt_CH1-1] == temp[0] ) && ( UartModbus_rev_buff_CH1[Nchr_Modbus_pckt_CH1-2] == temp[1] ) ) {
//                Modbus_LED_STATUS_ON ( 200 );

					
                ++cntModbusOKCH1;
                timLastModbusOKCH1=0;

                switch ( UartModbus_rev_buff_CH1[1] ) {
                case 0x01:
								case 0x02:
                    timLastModbusOKforMeCH1=0;
                    Modbus_Function_1_and_2_CH1();
                    Modbus_Task_StartSendSession_CH1();
                    break;
                case 0x03:
                case 0x04:
                    timLastModbusOKforMeCH1=0;
                    //ModbusStatusCH1.BIT.Modbus_ON = 1;
                    Modbus_Function_3_and_4_CH1();
                    Modbus_Task_StartSendSession_CH1(); // TODO: da fare in funzione di esito Modbus_Function_3_and_4
                    //ledOn(&Led485Error, 0);
                    break;
                case 0x05:
                    timLastModbusOKforMeCH1=0;
                    Modbus_Function_5_CH1();
                    Modbus_Task_StartSendSession_CH1();
                    break;
                case 0x06:
                    timLastModbusOKforMeCH1=0;
                    //ModbusStatusCH1.BIT.Modbus_ON = 1;
                    Modbus_Function_6_CH1();
                    Modbus_Task_StartSendSession_CH1();// TODO: da fare in funzione di esito Modbus_Function_6
                    //ledOn(&Led485Error, 0);
                    break;
                case 0x10:
                    timLastModbusOKforMeCH1=0;
                    //ModbusStatusCH1.BIT.Modbus_ON = 1;
                    Modbus_Function_16_CH1();
                    Modbus_Task_StartSendSession_CH1();// TODO: da fare in funzione di esito Modbus_Function_6
                    //ledOn(&Led485Error, 0);
                    break;
                default:
                    ++cntModbusFunUnknownCH1;
                    //ledOn(&Led485Error, 1);
                    Modbus_ExceptionResponses_CH1 ( UartModbus_rev_buff_CH1[1], MODBUS_ILLEGAL_FUNCTION );
                    Modbus_Task_StartSendSession_CH1();// TODO: da fare in funzione di esito Modbus_Function_6
//                    Modbus_LED_ERROR_ON ( 2000 );
                    break;
                }
            } else {
						
                ++cntModbusERRCH1;
                // ledOn(&Led485Error, 1);
//                Modbus_LED_ERROR_ON ( 2000 );
            }
        } else if ( UartModbus_rev_buff_CH1[0] != PMD2_SendToRemote.field.system.modbusID && UartModbus_rev_buff_CH1[0] != K_MODBUS_ID) ++cntNotForMeCH1;

        ModbusStatusCH1.BIT.Rx_end = 0;
        for ( i = 0; i<SIZE_UartModbus_rev_buff; ++i ) {
            UartModbus_rev_buff_CH1[i] = 0;
        }
    }

}

//----------------------------------------
void Modbus_Task_Send_CH1 ( void )
//----------------------------------------
{

    // Possibile test buffer TX libero
    // se non libero return della funzione

    if ( 1 ) {

        if ( Modbus_Task_Send_idxTrsmtChars_CH1 < UartModbus_send_counter_CH1 ) {
            //PutCharCh0_SCI( UartModbus_send_pointer[Modbus_Task_Send_idxTrsmtChars] );
            //04sep17  HAL_UART_Transmit ( &huart1, ( uint8_t * ) &UartModbus_send_pointer[Modbus_Task_Send_idxTrsmtChars],1,1000 );

            if ( flgModbusActivityCH1 ) {
                HAL_UART_Transmit ( &huart1, ( uint8_t * ) &UartModbus_send_pointer_CH1[Modbus_Task_Send_idxTrsmtChars_CH1],1,1000 );
            }
			

            ++Modbus_Task_Send_idxTrsmtChars_CH1;

        } else {
            Modbus_Task_CH1_Ptr = &Modbus_Task_ReleaseBUS_CH1;
            Modbus_Task_Send_idxTrsmtChars_CH1=0;
            timModbusCH1 = TIM_BUS_RELEASE_BUS_BEFORE_SEND;
        }
    }

}

//----------------------------------------
void Modbus_Task_ReleaseBUS_CH1 ( void )
//----------------------------------------
{

    if ( timModbusCH1 == 0 ) {
        Modbus_Task_CH1_Ptr = &Modbus_Task_Listen_CH1;
        flgModbusActivityCH1=FALSE;
    }
}

//----------------------------------------
void Modbus_Task_StartSendSession_CH1 ( void )
//----------------------------------------
{
    //sprintf( S.unionRom.romStruct.buffer, " %d ",UartModbus_send_counter_CH1);
    //pnext1( S.unionRom.romStruct.buffer );

    Modbus_Task_Send_idxTrsmtChars_CH1 = 0;
    Modbus_Task_CH1_Ptr = &Modbus_Task_Send_CH1;
}



