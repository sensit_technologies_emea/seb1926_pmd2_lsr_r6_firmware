/*
 * crc16_modbus.h
 *
 *  Created on: 12/gen/2013
 *      Author: develop
 */

#ifndef CRC16_MODBUS_H_
#define CRC16_MODBUS_H_

extern const uint16_t crc16_modbus_tab[];
extern uint16_t compute_modbus_crc ( uint16_t crc, uint16_t data );
extern uint16_t  GetCRC16 ( uint16_t  *puchMsg, uint16_t  DataLen );

#endif /* CRC16_MODBUS_H_ */
