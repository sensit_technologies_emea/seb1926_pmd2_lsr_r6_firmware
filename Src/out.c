#include "stm32l4xx_hal.h"
#include "sensit.h"
#include "out.h"
#include "main.h"
#include "modbus_extension_register.h"
/*
 *
 */

//void LED1_ON(){
//    HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_SET);
//}

//void LED1_OFF(){
//     HAL_GPIO_WritePin(LED1_GPIO_Port, LED1_Pin, GPIO_PIN_RESET);
//}


/*
 *
 */
  
void VLD_ON(){
	if (PMD2_SendToRemote.field.flag.tecStatus==ON){
		HAL_GPIO_WritePin(ON_VLD_GPIO_Port, ON_VLD_Pin, GPIO_PIN_SET);
		PMD2_SendToRemote.field.flag.laserStatus=ON;PMD2_ReceiveFromRemote.field.flag.laserStatus=ON;PMD2_PreviousReceiveFromRemote.field.flag.laserStatus=ON;
	}
}

void VLD_OFF(){
  HAL_GPIO_WritePin(ON_VLD_GPIO_Port, ON_VLD_Pin, GPIO_PIN_RESET);
	PMD2_SendToRemote.field.flag.laserStatus=OFF;PMD2_ReceiveFromRemote.field.flag.laserStatus=OFF;PMD2_PreviousReceiveFromRemote.field.flag.laserStatus=OFF;
}

void V5V_ON(){
	if (PMD2_SendToRemote.field.flag.tecStatus==ON){
		HAL_GPIO_WritePin(ON_5V_GPIO_Port, ON_5V_Pin, GPIO_PIN_SET);
//		PMD2_SendToRemote.field.flag.laserStatus=ON;PMD2_ReceiveFromRemote.field.flag.laserStatus=ON;PMD2_PreviousReceiveFromRemote.field.flag.laserStatus=ON;
	}
}

void V5V_OFF(){
  HAL_GPIO_WritePin(ON_5V_GPIO_Port, ON_5V_Pin, GPIO_PIN_RESET);
//	PMD2_SendToRemote.field.flag.laserStatus=OFF;PMD2_ReceiveFromRemote.field.flag.laserStatus=OFF;PMD2_PreviousReceiveFromRemote.field.flag.laserStatus=OFF;
}

void TC_ON(){
	HAL_GPIO_WritePin(ON_TC_GPIO_Port, ON_TC_Pin, GPIO_PIN_SET);
	PMD2_SendToRemote.field.flag.tecStatus=ON;PMD2_ReceiveFromRemote.field.flag.tecStatus=ON;PMD2_PreviousReceiveFromRemote.field.flag.tecStatus=ON;
}

void TC_OFF(){
	if (PMD2_SendToRemote.field.flag.laserStatus==ON)
		VLD_OFF();
	if (PMD2_SendToRemote.field.flag.laserStatus==OFF){
		HAL_GPIO_WritePin(ON_TC_GPIO_Port, ON_TC_Pin, GPIO_PIN_RESET);
		PMD2_SendToRemote.field.flag.tecStatus=OFF;PMD2_ReceiveFromRemote.field.flag.tecStatus=OFF;PMD2_PreviousReceiveFromRemote.field.flag.tecStatus=OFF;
	}
}

/**
  * @brief  Reset function external watch-dog MAX823 device with WDI line
  *         PAY ATTENTION: minimum Watchdog Timeout Period 1.12s
  *
  * @param  None
  * @retval None
  */
void WDI_TOGGLE() {
   HAL_GPIO_TogglePin(WDI_GPIO_Port, WDI_Pin);
}
