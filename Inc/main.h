/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define FSYNC_rf_Pin GPIO_PIN_2
#define FSYNC_rf_GPIO_Port GPIOE
#define ON_5V_Pin GPIO_PIN_3
#define ON_5V_GPIO_Port GPIOE
#define ON_TC_Pin GPIO_PIN_5
#define ON_TC_GPIO_Port GPIOE
#define WDI_Pin GPIO_PIN_13
#define WDI_GPIO_Port GPIOC
#define I2C2_SDA_Pin GPIO_PIN_0
#define I2C2_SDA_GPIO_Port GPIOF
#define I2C2_SCL_Pin GPIO_PIN_1
#define I2C2_SCL_GPIO_Port GPIOF
#define E2PROM_WP_Pin GPIO_PIN_2
#define E2PROM_WP_GPIO_Port GPIOF
#define TEMP1_Pin GPIO_PIN_0
#define TEMP1_GPIO_Port GPIOC
#define IPELT_Pin GPIO_PIN_1
#define IPELT_GPIO_Port GPIOC
#define TEMP_LASER_Pin GPIO_PIN_2
#define TEMP_LASER_GPIO_Port GPIOC
#define SPI3_NSS_Pin GPIO_PIN_4
#define SPI3_NSS_GPIO_Port GPIOA
#define LASER_CATHODE_BUF_Pin GPIO_PIN_4
#define LASER_CATHODE_BUF_GPIO_Port GPIOC
#define LASER_ANODE_BUF_Pin GPIO_PIN_5
#define LASER_ANODE_BUF_GPIO_Port GPIOC
#define AT45D_BANK_Pin GPIO_PIN_8
#define AT45D_BANK_GPIO_Port GPIOE
#define AT45D_RESET_Pin GPIO_PIN_9
#define AT45D_RESET_GPIO_Port GPIOE
#define ON_VLD_Pin GPIO_PIN_10
#define ON_VLD_GPIO_Port GPIOE
#define SPI2_NSS_Pin GPIO_PIN_12
#define SPI2_NSS_GPIO_Port GPIOB
#define USART1_CTS_Pin GPIO_PIN_11
#define USART1_CTS_GPIO_Port GPIOA
#define USART1_RTS_Pin GPIO_PIN_12
#define USART1_RTS_GPIO_Port GPIOA
#define USART2_RTS_Pin GPIO_PIN_1
#define USART2_RTS_GPIO_Port GPIOA
#define SWDIO_JTMS_Pin GPIO_PIN_13
#define SWDIO_JTMS_GPIO_Port GPIOA
#define JTCK_SWCLK_Pin GPIO_PIN_14
#define JTCK_SWCLK_GPIO_Port GPIOA
#define JTDI_Pin GPIO_PIN_15
#define JTDI_GPIO_Port GPIOA
#define JTDO_SWO_Pin GPIO_PIN_3
#define JTDO_SWO_GPIO_Port GPIOB
#define JTRST_Pin GPIO_PIN_4
#define JTRST_GPIO_Port GPIOB
#define RST_rf_Pin GPIO_PIN_9
#define RST_rf_GPIO_Port GPIOB
#define FSYNC_2rf_Pin GPIO_PIN_0
#define FSYNC_2rf_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */
#define BOARD_VERSION  4
void wait_us_TIM2(uint32_t usec);
void start_clock_TIM2(void);
uint32_t read_stop_clock_TIM2(void);
void wait_us_TIM5(uint32_t usec);
void start_clock_TIM5(void);
uint32_t read_stop_clock_TIM5(void);
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
